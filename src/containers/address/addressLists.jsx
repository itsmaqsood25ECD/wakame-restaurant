import React,{ useState, useEffect } from "react";
import {Drawer,Spin, Button } from 'antd'
import {useDispatch} from 'react-redux'
import {showAddressDrawer} from '../../redux/actions/toggleAction'
import { setAddress } from "../../redux/actions/adress";
import propTypes from "prop-types";
import {connect} from 'react-redux'
import {getData} from '../../services/api'
import {FiPlusCircle} from 'react-icons/fi'
import EditAddress from './EditAddressManagement'
import { useTranslation } from 'react-i18next';
import './address.css'

const AddressManagmentList = (props) => {
  const {REACT_APP_API_URL} = process.env
  const {t} = useTranslation()
  var default_add = localStorage.getItem("default_address")
  var parsed_default_add = default_add ? JSON.parse(default_add) : ''

    const [visible, setVisible] = useState()
    const [address, setAddress] = useState([])
    const [defaultAddress, setDefaultAddress] = useState(parsed_default_add)
    const [OpenEditAddress, setOpenEditAddress] = useState(false)
    const [EditAddressData, setEditAddressData] = useState(0)
    const [load, setLoad] = useState(true)

    const dispatch = useDispatch();
    
    useEffect(() => {
        setVisible(false);
        setLoad(false)
        fetchAllAddress()
     
     }, []);

      

     async function fetchAllAddress () {
     setLoad(true)
     const res = await getData(`${REACT_APP_API_URL}/api/v1/outlet/list_address`)
     try {
       
       console.log(res)
       if(res.data.status === 200){
        setAddress(res.data.data)
       }
       setLoad(false)
       
     } catch (error) {
       setLoad(false)
     }
     }

     const handleChangeDefaultAddress = add => {
       console.log(add)
       localStorage.setItem("default_address", JSON.stringify(add))
       dispatch(props.setAddress(add))
     }

     const handleAddAddress = () => {
          dispatch(showAddressDrawer(true))
     }

     const deleteAddress = async(id) => {
         setLoad(true)
       const res = await getData(`${REACT_APP_API_URL}/api/v1/outlet/delete_address?address_id=${id}`)
         try {
             console.log(res)
             setLoad(false)
             fetchAllAddress()
         } catch (error) {
             setLoad(false)
         }
     }

     const handleEditAddress = async(add) => {
        setOpenEditAddress(true)
        setEditAddressData(add)
        setVisible(true)
     }

     const toggleEditAddress = () => {
        setVisible(!visible)
     }


    return (
        <div>
         
        
        {load ? <Spin ></Spin>:  <div>

          <div className="address-list-container" defaultValue={defaultAddress.id} buttonStyle="outline">
          {address && address.length > 0 && address.map(add => {
            return <div onClick={() => handleChangeDefaultAddress(add)} className="address-item" value={add.id}>
                <p className="name-of-address">{add.name_of_address}</p>
                <p className="complete-address">{add.complete_address}</p>
                <div className="action-btn-container">
                <Button onClick={() => handleEditAddress(add)} className="bg-white text-primary address-action-btn">{t('edit')}</Button>
                <Button onClick={() => deleteAddress(add.id)} className="bg-white text-primary address-action-btn">{t('delete')}</Button>
                </div>
            </div>
          })}
          <div className="address-item add-new-address-item">
              <div onClick={handleAddAddress} className="centered-item">
               <FiPlusCircle />
              <p className="add-new-Address">{t('addAddress')}</p>
              </div>
          </div>
          </div>
          </div>}

          <Drawer
          title={<div className={props.locale == 'en' ? '': 'text-left'}>{`${t('myAddresses')}`}</div>}
          placement={props.locale === 'en' ? "left" : "right"}
          closable={true}
          onClose={toggleEditAddress}
          visible={visible}
          key={props.locale === 'en' ? "left" : "right"}
          className="responsive-drawer"
        >
        <EditAddress EditAddress={EditAddressData}></EditAddress>
        </Drawer>
        </div>
    );
}


AddressManagmentList.propTypes = {
  setAddress:propTypes.func.isRequired,
  DefaultAddress:propTypes.object.isRequired,
  locale: propTypes.string.isRequired,

};

const mapStateToProps = state => ({
  DefaultAddress:state.DefaultAddress.default_address,
  locale: state.i18n.locale
});


const mapDispatchToProps = { setAddress };


export default connect(mapStateToProps, mapDispatchToProps)(AddressManagmentList);



