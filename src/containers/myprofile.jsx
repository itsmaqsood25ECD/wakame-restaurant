import React,{useState, useEffect, Fragment} from "react";
import { Form, Button, Input, Spin } from 'antd';
import {postData } from "../services/api";
import {  toast } from 'react-toastify';
import propTypes from 'prop-types';
import {connect} from 'react-redux';
import { useTranslation } from "react-i18next";

const MyProfile = () => {
    const {REACT_APP_API_URL} = process.env;
    const {t} = useTranslation()
   const [userID, setUserID] = useState(localStorage.getItem('userID') ? localStorage.getItem('userID') : '')
   const [userDetails, setUserDetails] = useState({})
   const [load, setLoad] = useState(false)
   const [form] = Form.useForm()


   useEffect(() => {
   getUserDetails(userID)
  }, []);

    const getUserDetails = async(emp_id) => {
      setLoad(true)
      const formData = new FormData();
      formData.append('emp_id', emp_id)
      const res = await postData(`${REACT_APP_API_URL}/api/v1/outlet/userprofile`, formData)
      try {
             if(res.data.status === 200){
              setLoad(false)
               setUserDetails(res.data.data[0])
               form.setFieldsValue({
                first_name:res.data.data[0].first_name,
                last_name:res.data.data[0].last_name,
                phone:res.data.data[0].phone,
                email:res.data.data[0].email
               })
             }
      } catch (e) {
        setLoad(false)
      }
    }


    const onFinish = async(value) => {
        setLoad(true)
        const formData = new FormData();
        formData.append('emp_id', userID)
        formData.append('first_name', value.first_name)
        formData.append('last_name', value.last_name)
        formData.append('phone', value.phone)
        formData.append('email', value.email)
        const res = await postData(`${REACT_APP_API_URL}/api/v1/outlet/employeeupdate`, formData)
        try {
             setLoad(false)
             toast.success("updated successfully")
             console.log("profile update", res)
        } catch (error) {
            
        }

      }

    return (
        <Fragment>
        
        <Spin spinning={load} >
            <Form 
              name="my-profile-form"
              className="login-form" 
              form={form}
              size="large"
              onFinish={onFinish}
            >
                    <Form.Item
                        name="first_name"
                        rules={[{
                            required: true,
                            message: `${t('err-firstName')}`
                        }
                    ]}>
                        <Input
                           name="first_name"
                            type="text"
                            varient="bottom-outline"
                            placeholder={t('firstName')}/>
                    </Form.Item>
                    <Form.Item
                        name="last_name"
                        rules={[{
                            required: true,
                            message: `${t('err-lastName')}`
                        }
                    ]}>
                        <Input
                            type="text"
                            name="last_name"
                            placeholder={t('lastName')}
                            varient="bottom-outline"/>
                    </Form.Item>
                    <Form.Item
                        name="phone"
                        rules={[{
                            required: true,
                            message:  `${t('err-mobileNumber')}`
                        }
                    ]}>
                        <Input
                            maxlength='10'
                            name="phone"
                            type="tel"
                            disabled
                            varient="bottom-outline"
                            placeholder={t('mobileNumber')}/>
                    </Form.Item>
                    <Form.Item
                        name="email"

                        rules={[{
                            required: true,
                            message: `${t('err-emailId')}`
                        }
                    ]}>
                        <Input
                        name="email"
                            type="email"
                            placeholder={t('emailId')}
                            varient="bottom-outline"/>
                    </Form.Item>

                    <Form.Item>
                        <Button type="primary" htmlType="submit" className="login-form-button">
                        {t('updateProfile')}
                        </Button>
                    </Form.Item>
                </Form>
            </Spin>
               
        </Fragment>
    )
  }


MyProfile.propTypes = {
    locale: propTypes.string.isRequired,
  };
  
  const mapStateToProps = state => ({
      locale: state.i18n.locale
    });
    
    const mapDispatchToProps = { };
    
  export default connect(mapStateToProps, mapDispatchToProps)(MyProfile);