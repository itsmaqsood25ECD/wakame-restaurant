import React,{ useState, useEffect } from "react";
import {Drawer,Spin, Rate , Button, Progress, Select, DatePicker, TimePicker } from 'antd'
import {useDispatch, useSelector} from 'react-redux'
import {showBranchListViewModal} from '../../redux/actions/toggleAction'
import { Scrollbars } from 'react-custom-scrollbars';
import {getData, postData} from '../../services/api'
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Map, Marker } from 'google-maps-react'
import {setActiveBranch} from '../../redux/actions/ActiveBranch'
import Gallery from 'react-grid-gallery';
import {
    MobileOutlined, 
    WhatsAppOutlined, 
    PhoneOutlined, 
    ClockCircleOutlined,
    EnvironmentOutlined
} from '@ant-design/icons'
import {ButtonGroup } from 'reactstrap';
import { FaMinus, FaPlus } from 'react-icons/fa';
import moment from "moment";
import '../styles/branchesList.css'
import { toast } from "react-toastify";
import { useHistory } from "react-router-dom";
import { useTranslation } from "react-i18next";

const { Option } = Select;

const BranchManagmentListView = (props) => {
    let history = useHistory();
    const {t} = useTranslation()
    const {REACT_APP_API_URL} = process.env;
    const [visible, setVisible] = useState()
    const [Branches, setBranches] = useState([])
    const [load, setLoad] = useState(true)
    const [branchDetailsModal, setBranchDetailsModal] = useState(false)
    const [tableBookingBranch, setTableBookingBranch] = useState(false)
    const [BranchData, setBranchData] = useState({})
    const [branchImages, setBranchImages] = useState([])
    const [centerMap, setCenterMap] = useState({lat: 35.5496939, lng: -120.7060049})
    const [branchReview, setBranchReview] = useState([])
    const [tableCount, setTableCount] = useState(1)
    const [userID, setUserID] = useState(localStorage.getItem('userID') ? localStorage.getItem('userID') : '')
    const [BookingDate, setBookingDate] = useState('')
    const [BookingTime, setBookingTime] = useState('')
    const [ratingCountProgress, setRatingsCountProgress] = useState([])
    const dispatch = useDispatch();
    const state_visible = useSelector( state => state.toggleStack.showBranchListsView)
    useEffect(() => {
        setVisible(state_visible);
        getBranchOutlet()
      }, [state_visible]);

    useEffect(() => {
        const timeLeft =  moment().endOf('day').fromNow();          // in 3 hours
        const timeleftHour = moment().startOf('hour').fromNow();       
        console.log("timeNow", moment().format('LT'))
        console.log("timeLeft", timeLeft)
        console.log("timeleftHour", timeleftHour)
    }, [])

    async function getBranchOutlet () {
         const res = await getData(`${REACT_APP_API_URL}/api/v1/outlet/getoutlets`)
         try {
             console.log(res)
             setLoad(false)
             if(res.data.status === 200){
               setBranches(res.data.data)
             }
          } catch (error) {
         }
    }

     const setBranchGallary = (image) => {
         const gallary = []
         image.map(img => {
            gallary.push({
              "src":'https://wakameuat.upappfactory.app/wakame-admin/uploads/' + img,
              "thumbnail":'https://wakameuat.upappfactory.app/wakame-admin/uploads/' + img,
              thumbnailWidth:120,
              thumbnailHeight:120
            })
         })
         setBranchImages(gallary)
         console.log(gallary)
     }

     const getReview = async(id) => {
       const formdata = new FormData()
       formdata.append("outlet_id", id)
      const res = await postData('/api/v1/outlet/getreview', formdata)
      try {
          
          if(res.data.status === 200){
            setBranchReview(res.data.data)
            getProgessPercentage(res.data.data)
            console.log("review", res.data.data)
          }

      } catch (error) {
          
      }
     }

     const getIncreament = () => {
        setTableCount(tableCount + 1)
     }

     const getDecrement = () => {
        if(tableCount > 1){
            setTableCount(tableCount - 1)
        }
     }

     const reqestTableBooking = async(outletID) => {
        setLoad(true)
        if(outletID == '' || userID == '' || BookingTime == '' || BookingDate == '' ) {
            toast.warning("please fill the all details")
            setLoad(false)
        }else {
        
        const formData = new FormData();
        formData.append('emp_id', userID)
        formData.append('booking_status', 1)
        formData.append('persons', tableCount)
        formData.append('outlet_id', outletID)
        formData.append('time', moment(BookingTime).format('LTS'))
        formData.append('booking_date', moment(BookingDate).format('l'))
        const res = await postData(`${REACT_APP_API_URL}/api/v1/outlet/addbooking`, formData)
        try {
            
            setLoad(false)
            toast.success("Booked successfully")
            console.log("profile update", res)

        } catch (error) {
            
        }
        }
        
     }

     function disabledDate(current) {
        // Can not select days before today and today
        return current && current <= moment().startOf('day');
      }
      
     const goToDirection = (lat, lng) => {
        if /* if we're on iOS, open in Apple Maps */
        ((navigator.platform.indexOf("iPhone") !==-1) || 
         (navigator.platform.indexOf("iPod") !==-1) || 
         (navigator.platform.indexOf("iPad") !==-1))
        window.open(`maps://maps.google.com/maps?daddr=${lat},${lng}&amp;ll=`);
    
      else /* else use Google */
        window.open(`https://maps.google.com/maps?daddr=${lat},${lng}&amp;ll=`);
      }

    const getProgessPercentage = (reviews) => {
        const total = reviews.length;
        const counts = {};
        const ratArray = [];
        reviews.map(rev => {
            ratArray.push(rev.rating)
        })
        
            for (const num of ratArray) {
                   counts[num] = counts[num] ? counts[num] + 1 : 1;
                
              }
        
        const branchRatingsData = [
            {   "count": counts.hasOwnProperty("1") ? counts["1"] : 0,
                "progess": counts.hasOwnProperty("1") ? ((counts["1"] / total)*100) : 0,
            },
            {   "count": counts.hasOwnProperty("2") ? counts["2"] : 0,
                "progess": counts.hasOwnProperty("2") ? ((counts["2"] / total)*100) : 0,
            },
            {   "count": counts.hasOwnProperty("3") ? counts["3"] : 0,
                "progess": counts.hasOwnProperty("3") ? ((counts["3"] / total)*100) : 0,
            },
            {   "count": counts.hasOwnProperty("4") ? counts["4"] : 0,
                "progess": counts.hasOwnProperty("4") ? ((counts["4"] / total)*100) : 0,
            },
            {   
                "count": counts.hasOwnProperty("5") ? counts["5"] : 0,
                "progess": counts.hasOwnProperty("5") ? ((counts["5"] / total)*100) : 0,
            },
        ]
        

        
        setRatingsCountProgress(branchRatingsData.reverse())
        console.log("ratArray",ratArray);
        console.log("counts", counts);
        console.log("branchRatingsData", branchRatingsData.reverse())
    }
     

     const handleBranchView = (branch) => {
        setBranchDetailsModal(true)
        setBranchData(branch)
        setBranchLocation(branch && branch.latitude, branch && branch.longitude)
        setBranchGallary(branch.image)
        getReview(branch.outlet_id)
        console.log(branchReview)

     }

     const handleTableBook = (branch) => {

        if(branch.outlet_full === '1'){
            toast.error("Outlet is full! please try after sometimes. ")
        }else {
            setTableBookingBranch(true)
        }

     }

     const handleMenuView = (branch) => {
        localStorage.setItem("Active_Branch", JSON.stringify(branch))
        dispatch(setActiveBranch(JSON.stringify(branch)))
        setBranchDetailsModal(false)
        setTableBookingBranch(false)
        dispatch(showBranchListViewModal(false))
        history.push("/menu");
     }

     const setBranchLocation = (lat, lng) => {
      setCenterMap({
        lat:lat,
        lng:lng
      })
     }


     const onClose = () => {
      dispatch(showBranchListViewModal(false))
     };


    return (
        <div>
            <Drawer
          title={<div className={props.locale == 'en' ? '': "text-left"}>{t('active-branches')}</div>}
          placement={props.locale == 'en' ? 'left': 'right'}
          closable={true}
          onClose={onClose}
          visible={visible}
          key="left"
          className="responsive-drawer"
        >
          <div className="Drawer-header">
            
          </div>
        {load ? <Spin ></Spin>: 
              <Scrollbars >
             <div style={{height:"100vh"}}>
          {Branches && Branches.map((branch, index) => {
              return <>
              
              <div className={`branch-list-container ${branch.status === "0" ? 'inActive' : branch.status === "2" ? "Busy" : ''}`}>
              
              <div className="branch-img-container">
                  {branch.image && branch.image.length > 0 ? <img className="branch-image" src={'https://wakameuat.upappfactory.app/wakame-admin/uploads/'+ branch.image[0]} alt={branch.image[0]}></img> : <img className="branch-image" src={process.env.REACT_APP_PUBLIC_URL+ "/images/NO_IMG.jpg"} alt="no img"></img>}
                  
              </div>
              <div className="branch-details-container">
                  <p className="branch-name">{branch.outlet_name[props.locale]} {branch.outlet_busy === "1" && <span class="badge bg-busy badge badge-outline">{t('busy')}</span> } {branch.outlet_full === "0" ? <span class="badge bg-dinein badge badge-outline">{t('dineIn')}</span> : <span class="badge bg-dinein badge badge-outline">{t('full')}</span>}</p>
                  <p className="branch-address">{branch.address[props.locale]}</p>
                  <p className="branch-timing"><img src={process.env.REACT_APP_PUBLIC_URL + '/images/time.svg'} alt="time"></img> {branch.opentime} -  {branch.closetime}</p>
              </div>
              <div className="branch-action-container">
              <Rate disabled defaultValue={branch.rat.rating} />
              <p className="branch-review">{branch.rat.review} {t('review')}</p>
              <Button onClick={() => handleBranchView(branch)} type="primary">{t('view')}</Button>
              </div>
          </div>
             
              </>
          })}
          </div>
          </Scrollbars>

          }
          {/* branch View drawer */}

          <Drawer
          className="responsive-drawer"
            closable={false}
            
            placement={props.locale == 'en' ? 'left': 'right'}
            onClose={()=> setBranchDetailsModal(false)}
            visible={branchDetailsModal}
          >
             <Scrollbars >
             <div style={{height:"100vh"}}>
             <div>
                <p className="section-title">{BranchData && BranchData.outlet_name && BranchData.outlet_name[props.locale]} {BranchData.outlet_busy === "1" && <span class="badge bg-busy badge badge-outline">{t('busy')}</span> } {BranchData.outlet_full === "0" ? <span class="badge bg-dinein badge badge-outline">{t('dineIn')}</span> : <span class="badge bg-dinein badge badge-outline">{t('full')}</span>}</p>
                <p className="d-flex"> <span className="mr-2">5.0</span> <Rate className="branch-view-rating" disabled defaultValue={BranchData && BranchData.rat && BranchData.rat.rating} /> <p className="branch-review text-primary pl-2 pr-2"> {BranchData && BranchData.rat && BranchData.rat.review} {t('review')}</p> </p>

                <div className="d-flex branch-view-action-btn ">
                    <Button size="small" onClick={() => handleMenuView(BranchData)} className="btn-primary-outline"> {t('viewMenu')} </Button>
                    <Button size="small" onClick={() => handleTableBook(BranchData)} className="btn-primary-outline"> {t('bookTable')} </Button>
                    <Button onClick={() => goToDirection(BranchData && BranchData.latitude , BranchData && BranchData.longitude)} size="small" className="btn-primary-outline">  {t('direction')} </Button>
                </div>

                <div className="border-bottom w-100 mt-3 mb-4"></div>

                <div className="branch-contact-view">
                    <div className="d-flex mb-2">
                        <div className="branch-view-contact-icon">
                           <MobileOutlined />
                        </div>
                        <div>
                            <p className="branch-view-contact-details">{BranchData && BranchData.phone}</p>
                        </div>
                    </div>
                    <div className="d-flex mb-2">
                        <div className="branch-view-contact-icon">
                        <WhatsAppOutlined />
                        </div>
                        <div>
                            <p className="branch-view-contact-details">{BranchData && BranchData.whatsup}</p>
                        </div>
                    </div>
                    <div className="d-flex mb-2">
                        <div className="branch-view-contact-icon">
                        <PhoneOutlined />
                        </div>
                        <div>
                            <p className="branch-view-contact-details">{BranchData && BranchData.telphone}</p>
                        </div>
                    </div>
                    <div className="d-flex mb-2">
                        <div className="branch-view-contact-icon">
                        <ClockCircleOutlined />
                        </div>
                        <div>
                            <p className="branch-view-contact-details">{BranchData && BranchData.opentime} - {BranchData && BranchData.closetime}</p>
                        </div>
                    </div>
                    <div className="d-flex mb-2">
                        <div className="branch-view-contact-icon">
                        <EnvironmentOutlined />
                        </div>
                        <div>
                            <p className="branch-view-contact-details">{BranchData && BranchData.address && BranchData.address[props.locale]}</p>
                        </div>
                    </div>
                    </div>
                
                <div className="branch-view-map-container mb-4">
                <Map 
                    google={window.google} 
                    zoom={15}
                    initialCenter={centerMap}
                    center={centerMap}
                    containerStyle={{  
                    position: "sticky",
                    width: "100%",
                    maxWidth: "100%",
                    height: "180px"
                    }}
                    >
                    <Marker 
                        position={centerMap}
                        draggable={false}
                        name={'Current location'}
                        icon={{
                        url: `${process.env.REACT_APP_PUBLIC_URL}/images/map-pin.svg`,
                        anchor: new window.google.maps.Point(32.5,66),
                        scaledSize: new window.google.maps.Size(64,64)
                        
                        }}>
                    </Marker>
                    <Button size="small" onClick={() => goToDirection(centerMap.lat, centerMap.lng)} className="map-get-direction-btn" type="primary">{t('getDirection')}</Button>
                </Map>
                </div>
                <p className="section-subtitle">{t('restaurentPhotos')}</p>
                <div className="branch-view-gallary mb-4">
                <div style={{
                display: "block",
                minHeight: "1px",
                overflow: "auto",
                textAlign: "center"
            }}>
                <Gallery  enableImageSelection={false} images={branchImages}/>
                </div>
                </div>
                <p className="section-subtitle mt-4">{t('reviewSummary')}</p>
                
                <div className="review-rating-container d-flex mb-4">
                    <Rate disabled defaultValue={BranchData && BranchData.rat && BranchData.rat.rating} />
                    <div className="p-2 pl-2">
                    <p className="mb-0">{BranchData && BranchData.rat && BranchData.rat.rating} out of 5</p>
                    <p className="text-primary mb-0">{BranchData && BranchData.rat && BranchData.rat.review} {t('review')}</p>
                </div>
                </div>
                <div style={{width:"80%"}}>
                    {ratingCountProgress.map((review, index) => {
                        return <div className="row" >
                            <div className="col-2 font-bold">{5 - index} Star</div>
                            <div className="col-10">
                                <Progress 
                                    className="branch-rating-progress-bar" 
                                    strokeWidth={20} 
                                    strokeColor={{
                                        '0%': '#FFD700',
                                        '100%': '#FFD700',
                                    }} 
                                    percent={review.progess} showInfo={false} />
                        </div>
                        </div>
                    })}
                </div>
                <div className="review-outer-container">
                  {branchReview && branchReview.length > 0 ? branchReview && branchReview.map(review => {
                    return  <div className="review-inner-container mb-2 d-flex">
                    <div className="review-profile">
                        {review && review.hasOwnProperty("name") && review.name !== null && review.name.split(/\s/).reduce((response,word)=> response+=word.slice(0,1),'')}
                    </div>
                    <div className="pl-3">
                    <p className="mb-0">
                        <span>{review.name}</span>
                        <span className="float-right">{moment(review.date).format('LL')}</span>
                    </p>
                    <div>
                    <Rate className="branch-view-rating" disabled defaultValue={review.rating} />
                    </div>
                    <p className="text-sm">
                    Do you want your order to be ready as you reach Wakame? Find our delicious take away menu and order online with just few clicks!
                    </p>
                    </div>
                    
                 </div>
              
                  }) : <><p className="text-lg">{t('noReviewAdded')}</p></>}
                 
                </div>
                
             </div>
             </div>
             </Scrollbars>

             {/* TableBooking View drawer */} 

             <Drawer
             className="responsive-drawer"
            closable={false}
            
            placement={props.locale == 'en' ? 'left': 'right'}
            onClose={()=> setTableBookingBranch(false)}
            visible={tableBookingBranch}
          >
             <Scrollbars >
             <div style={{height:"100vh"}}>
             <div>
               <div className="d-flex">
                   <div className="w-25">
                   {BranchData.image && BranchData.image.length > 0 ? <img className="table-branch-image" src={'https://wakameuat.upappfactory.app/wakame-admin/uploads/'+ BranchData.image[0]} alt={BranchData.image[0]}></img> : <img className="branch-image" src={process.env.REACT_APP_PUBLIC_URL+ "/images/NO_IMG.jpg"} alt="no img"></img>}
                   </div>
                   <div className="w-75 pl-2">
                       {console.log("BranchData", BranchData)}
                   <p className="section-title">{BranchData && BranchData.outlet_name && BranchData.outlet_name[props.locale]} {BranchData.outlet_busy === "1" && <span class="badge bg-busy badge badge-outline">{t('busy')}</span> } {BranchData.outlet_full === "0" ? <span class="badge bg-dinein badge badge-outline">{t('dineIn')}</span> : <span class="badge bg-dinein badge badge-outline">{t('full')}</span>}</p>
                   <p className="text-sm mb-1">{BranchData && BranchData.address && BranchData.address[props.locale]}</p>
                   <div className="d-flex">
                        <div className="d-flex flex-fill flex-column">
                          <p className="mb-0 text-white"> <ClockCircleOutlined /> {BranchData && BranchData.opentime} - {BranchData && BranchData.closetime}</p>
                          <p className="text-sm text-white">{t('service_time')}</p>
                        </div>
                        <div className="text-right flex-fill d-flex flex-column">
                        <Rate className="branch-view-rating" disabled defaultValue={BranchData && BranchData.rat && BranchData.rat.rating} />
                        <p className="branch-review text-primary pl-2 pr-2"> {BranchData && BranchData.rat && BranchData.rat.review} {t('review')}</p> 
                        </div>
                    </div>
                  

                <div className="d-flex branch-view-action-btn ">
                    <Button onClick={() => goToDirection(BranchData && BranchData.latitude , BranchData && BranchData.longitude)} size="small" className="btn-primary-outline"> {t('direction')} </Button>
                </div>
                   </div>
               </div>

                <div className="border-bottom w-100 mt-3 mb-4"></div>

               
                <p className="section-subtitle">{t('selectDateTime')}</p>
                <div className="mb-2">
                <DatePicker
                 disabledDate={disabledDate}
                 className="table-booking-picker" onChange={value => setBookingDate(value)} />
                </div>
                <div className="mb-2">
                <TimePicker format="HH:mm" className="table-booking-picker" type="time" onChange={value => setBookingTime(value)} />
                </div>
                <div>
                <p className="section-subtitle mt-4">{t('selectNumberOfGuest')}</p>
                <ButtonGroup>
                <Button style={{width:"40px"}} onClick={() => getDecrement()} size="middle" type="primary" ><FaMinus /></Button>
                <Button style={{width:"40px"}} size="middle"  type="primary" >{tableCount}</Button>
                <Button style={{width:"40px"}} onClick={() => getIncreament()} size="middle" type="primary" > <FaPlus /></Button>
                </ButtonGroup>
                </div> 
                <div className="mt-4 pt-5">
                    <p class="text-white">{t('TermsCondition')}</p>
                    <p class="text-sm mb-0">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                </div>
                <Spin spinning={load} >
                <Button type="primary" onClick={() => reqestTableBooking(BranchData && BranchData.outlet_id)} className="mt-3 text-uppercase login-form-button">
                {t('requestBooking')}
                </Button>
                </Spin>
                <p>
                {t('callbackNote')}
                </p>
                
             </div>
             </div>
             </Scrollbars>
          </Drawer>
           {/* TableBooking View drawer */} 
          </Drawer>

            {/* End branch View drawer */} 

        </Drawer>
        </div>
    );
};

BranchManagmentListView.propTypes = {
  locale: PropTypes.string.isRequired,
};

const mapStateToProps = state => ({
  locale: state.i18n.locale
});

const mapDispatchToProps = { };


export default connect(mapStateToProps, mapDispatchToProps)(BranchManagmentListView);

