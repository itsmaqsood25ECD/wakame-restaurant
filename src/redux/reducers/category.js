import * as types from '../actions/types'

const initialState = {
    loading:true,
    error:null,
    category:{}
}

export const selectedCategory = (state = initialState, action) => {
    switch(action.type) {
        case types.SET_CATEGORY:
            return {
                ...state,
                category:action.payload,
                loading:false,
                error:null
            }
        default: 
        return state
    }
}