import * as types from '../actions/types'

const initialState = {
    loading:true,
    error:null,
    default_address:localStorage.getItem("default_address") ? JSON.parse(localStorage.getItem("default_address")) : {}

}

export const DefaultAddress = (state = initialState, action) => {
    switch(action.type) {
        case types.SET_DEFAULT_ADDRESS:
            return {
                ...state,
                default_address:action.payload,
                loading:false,
                error:null
            }
        default: 
        return state
    }
}