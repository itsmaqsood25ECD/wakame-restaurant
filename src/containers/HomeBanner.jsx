import React, { Component } from 'react';
import './styles/homeBanner.css'
import { Row, Col} from 'antd';
import {
    geocodeByAddress,
    getLatLng,
  } from 'react-places-autocomplete';
import HomeNavigator from './HomeNevigator'
import Deals from './deals/promotion'
import { connect } from "react-redux";
import { withTranslation } from 'react-i18next';
import {withRouter} from 'react-router-dom'

const {REACT_APP_PUBLIC_URL} = process.env

class HomeLayout extends Component {

    state = {
        data: [],
        value: undefined,
        address:'',
      };
 
      handleChange = address => {
        this.setState({ address });
      };

      handleSelect = address => {
        geocodeByAddress(address)
          .then(results => getLatLng(results[0]))
          .then(latLng => console.log('Success', latLng))
          .catch(error => console.error('Error', error));
      };


    render() {
      
      const { t } = this.props;
     
        return (
          <>
          <section className="homepage-banner-bg">
              <div className="outer-layout">
                  <h1 onClick={this.getRess}>{t('home-orderDeliveryTakeAway')}</h1>
                  <p>{t('home-findyourlocationandgetstarted')}</p>
                  <div className="d-flex address-locate-outer">
                  {/* <PlacesAutocomplete
                    value={this.state.address}
                    onChange={this.handleChange}
                    onSelect={this.handleSelect}
                  >
                    {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => (
                      <div>
                        <input
                          {...getInputProps({
                            placeholder: `${t('home-searchyourlocation')}`,
                            className: 'location-search-input ant-input',
                          })}
                        />
                        <div className="autocomplete-dropdown-container">
                          {loading && <Spin>{t('common-Loading')}</Spin>}
                          {suggestions.map(suggestion => {
                            const className = suggestion.active
                              ? 'suggestion-item--active'
                              : 'suggestion-item';
                            // inline style for demonstration purpose
                            const style = suggestion.active
                              ? { backgroundColor: '#0000', cursor: 'pointer' }
                              : { backgroundColor: '#0000', cursor: 'pointer' };
                            return (
                              <div
                                {...getSuggestionItemProps(suggestion, {
                                  className,
                                  style,
                                })}
                              >
                                <div>{suggestion.description}</div>
                              </div>
                            );
                          })}
                        </div>
                      </div>
                    )}
                  </PlacesAutocomplete> */}
                  {/* <Button type="primary"><MdMyLocation style={{margin:"0px 3px 0px 3px"}} />  LOCATE ME </Button> */}
                  </div>
              </div>
              <div className="container home-navigation-outer d-none d-md-block">
                <HomeNavigator />
              </div>
          </section>
          <section >
          <div className="container home-navigation-outer d-block d-md-none">
                <HomeNavigator />
              </div>
          </section>
          <section className="mt-2 mb-5">
          <Deals />
          </section>
          <div className='app-promo-bg-desktop'>
          <section  className='container'>
            <div className="app-promotion-section" style={{maxWidth:"1024px",margin:"0 auto"}}>
              <Row gutter={16}>
                <Col xs={8} lg={12}>
                  <img className="home-app-screen" src={REACT_APP_PUBLIC_URL+ '/images/homepage/app-screen.webp'} alt="app-screen" />
                </Col>
                <Col xs={16} lg={12}>
                  <div className="app-promo-container">
                    <h2>{t('theBestFoodDeliveryApp')}</h2>
                    <p>
                      {t('appdummytext')}
                    </p>
                    <div className="d-flex">
                    <a href="https://play.google.com/store/apps/details?id=com.connect.Wakame1" target="_blank"><img className="app-store-icon" src={REACT_APP_PUBLIC_URL+ '/images/homepage/google-play.png'} alt="google play" /></a>
                   <a href="https://apps.apple.com/us/app/wakame/id1071400999" target="_blank"><img className="app-store-icon" src={REACT_APP_PUBLIC_URL+ '/images/homepage/apple-store.png'} alt="apple-store" /></a> 
                    </div>
                  </div>
                </Col>
              </Row>
            </div>
          </section>
          </div>
        </>
        );
    }
}


HomeLayout.propTypes = {


};

const mapStateToProps = state => ({


})

const mapDispatchToProps = { }

export default connect(mapStateToProps, mapDispatchToProps)(withTranslation()(withRouter(HomeLayout)));

