import * as types from './types'


export const showAddressDrawer = (data) => async dispatch => {

    dispatch({
        type:types.TOGGLE_ADDRESS_DRAWER,
        payload:data
    })

}
export const showAddressListDrawer = (data) => async dispatch => {

    dispatch({
        type:types.TOGGLE_ADDRESS_LIST_DRAWER,
        payload:data
    })

}

export const showLoginModal = (data) => async dispatch => {

    dispatch({
        type:types.TOGGLE_LOGIN_MODAL,
        payload:data
    })

}

export const showSignupModal = (data) => async dispatch => {

    dispatch({
        type:types.TOGGLE_SIGNUP_MODAL,
        payload:data
    })

}


export const showForgetPassModal = (data) => async dispatch => {

    dispatch({
        type:types.TOGGLE_FORGET_PASSWORD_MODAL,
        payload:data
    })

}

export const showResetPassModal = (data) => async dispatch => {

    dispatch({
        type:types.TOGGLE_RESET_PASSWORD_MODAL,
        payload:data
    })

}

export const showBranchListModal = (data) => async dispatch => {

    dispatch({
        type:types.TOGGLE_BRANCH_LIST_MODAL,
        payload:data
    })

}

export const showBranchListViewModal = (data) => async dispatch => {

    dispatch({
        type:types.TOGGLE_BRANCH_LIST_VIEW_MODAL,
        payload:data
    })

}
