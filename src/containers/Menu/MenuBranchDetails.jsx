import React,{useState, useEffect} from 'react';
import {Button, Rate} from 'antd'
import {connect} from 'react-redux'
import propTypes from 'prop-types'
import '../styles/mainMenu.css'
import { useTranslation } from 'react-i18next';

const MenuBranchDetails = (props) => {
    const {t} = useTranslation()
    const{REACT_APP_API_URL} = process.env
    const storedBranch = localStorage.getItem("Active_Branch");
    const branches = storedBranch ? JSON.parse(storedBranch): {};
    const [branch, setBranch] = useState(branches)
    

    useEffect(() => {
     setBranch(branches)
    }, [props])

    const mapsSelector = (lat, lng) => {
        if /* if we're on iOS, open in Apple Maps */
        ((navigator.platform.indexOf("iPhone") !==-1) || 
         (navigator.platform.indexOf("iPod") !==-1) || 
         (navigator.platform.indexOf("iPad") !==-1))
        window.open(`maps://maps.google.com/maps?daddr=${lat},${lng}&amp;ll=`);
    
      else /* else use Google */
        window.open(`https://maps.google.com/maps?daddr=${lat},${lng}&amp;ll=`);
      }

    return (
        <section className="branch-header-bg">
            <div className="container">
            <div className="branch-header-details-container">
                  <div className="branch-img-container d-none d-md-flex">
                      {branch && branch.image && branch.image.length > 0 && branch.image[0] != ''  ? <img className="branch-image" src={'https://wakameuat.upappfactory.app/wakame-admin/uploads/'+ branch.image[0]} alt={branch && branch.image[0]}></img> : <img className="branch-image" src={process.env.REACT_APP_PUBLIC_URL+ "/images/NO_IMG.jpg"} alt="no img"></img>}                      
                  </div>
                  <div className={props.locale === 'en' ? "branch-details-container " : "branch-details-container text-right"}>
                      <p className="branch-name">{branch && branch.outlet_name[props.locale]}</p>
                      <p className="branch-address">{branch && branch.address[props.locale]}</p>
                     <div className="d-flex branch-timing-rating">
                         <div className="branch-details-container">
                         <p className="branch-timing"><img src={process.env.REACT_APP_PUBLIC_URL + '/images/time.svg'} alt="time"></img> {branch && branch.opentime} -  {branch && branch.closetime}</p>
                         <p style={{color:"#d7d3d3", fontSize:"11px", marginLeft:"20px"}}>{t('service_time')}</p>
                         </div>
                        <div className="branch-action-container">
                        <Rate disabled defaultValue={branch && branch.rat.rating} />
                  <p className="branch-review">{branch && branch.rat.review}{t('review')}</p>
                        </div>
                     </div>
                        <Button type="link" onClick={() => mapsSelector(branch && branch.latitude, branch && branch.longitude)} 
                        className="mt-4 btn-blue ant-btn ant-btn-dashed" target="_blank" type="dashed">{t('direction')}</Button>                  
                  </div>
                 
              </div>
           
            </div>
             
        </section>
    );
};

  const mapStateToProps = state => ({
    selectedBranch: state.branch.selectedBranch,
    locale: state.i18n.locale
  });
  export default connect(mapStateToProps, null)(MenuBranchDetails);

// export default MenuBranchDetails;

