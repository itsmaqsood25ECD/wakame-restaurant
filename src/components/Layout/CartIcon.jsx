import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import {Link} from 'react-router-dom'
import {useDispatch, useSelector} from 'react-redux'
import {Badge} from 'antd'

const {REACT_APP_PUBLIC_URL} = process.env

export const CartIcon = (props) => {
    const [cartCount, setcartCount] = useState(0)
    useEffect(() => {
       
     
      }, [])

    return (
        <>
            <Link to="/checkout">
                <Badge
                className="site-badge-count-109 text-white c-pointer"
                count={cartCount}
                style={{ backgroundColor: '#52c41a' }}>
                    <img style={{opacity:0.65, margin:"0px 5px"}} src={REACT_APP_PUBLIC_URL + '/images/header/shopping-cart-icon.svg'} alt="cart icon" />
                </Badge>
                </Link>
        </>
    )
}

CartIcon.propTypes = {
    prop: PropTypes
}

const mapStateToProps = (state) => ({
    
})

const mapDispatchToProps = {
    
}

export default connect(mapStateToProps, mapDispatchToProps)(CartIcon)
