import * as types from '../actions/types'

const initialState = {
    loading:true,
    error:null
}

export const toggleReducer = (state = initialState, action) => {
    switch(action.type) {
        case types.TOGGLE_ADDRESS_DRAWER:
            return {
                ...state,
                showAddressDrawer:action.payload,
                loading:false,
                error:null
            }
        case types.TOGGLE_ADDRESS_LIST_DRAWER:
            return {
                ...state,
                showAddressListDrawer:action.payload,
                loading:false,
                error:null
            }
        case types.TOGGLE_LOGIN_MODAL:
        return {
            ...state,
            showLoginModal:action.payload,
            loading:false,
            error:null
        }
        case types.TOGGLE_SIGNUP_MODAL:
        return {
            ...state,
            showSignupModal:action.payload,
            loading:false,
            error:null
        }
        case types.TOGGLE_FORGET_PASSWORD_MODAL:
        return {
            ...state,
            showForgetPassModal:action.payload,
            loading:false,
            error:null
        }
        case types.TOGGLE_RESET_PASSWORD_MODAL:
        return {
            ...state,
            showResetPassModal:action.payload,
            loading:false,
            error:null
        }
        case types.TOGGLE_BRANCH_LIST_MODAL:
            return {
                ...state,
                showBranchLists:action.payload,
                loading:false,
                error:null
            }
        case types.TOGGLE_BRANCH_LIST_VIEW_MODAL:
                return {
                    ...state,
                    showBranchListsView:action.payload,
                    loading:false,
                    error:null
                }
        
        default: 
        return state
    }
}