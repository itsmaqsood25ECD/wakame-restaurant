import React, {   useState,   useEffect,   forwardRef,   useRef,   useImperativeHandle, } from "react";
import propTypes from 'prop-types';
import { connect } from 'react-redux';
import {getData, postData} from '../../services/api'
import {addToCart, clearCart, getCartItems} from '../../redux/actions/cart'
import {allProducts, updateProductList} from '../../redux/actions/products'
import '../styles/cart.css';
import {Button,Drawer, Spin, Radio ,Row, Col, Card, Checkbox, Statistic, Input } from 'antd'
import {ButtonGroup } from 'reactstrap';
import { FaMinus, FaPlus, FaTrashAlt, FaPercent } from 'react-icons/fa';
import { InfoCircleOutlined } from '@ant-design/icons';
import moment from 'moment'
import {toast} from 'react-toastify'
import { useHistory } from "react-router-dom";
import { useTranslation } from "react-i18next";


const CartItems = forwardRef((props, ref) => {
  let history = useHistory();
  const {t} = useTranslation()
  const {REACT_APP_API_URL} = process.env
    let storedBranch = localStorage.getItem("Active_Branch");
    let branches = storedBranch ? JSON.parse(storedBranch): '';
    let cartItems;
    let vatPercentage = 0;
    let loyalityToOmrRate = 0.001;
    const [load, setLoad] = useState(false)
    const [loyaltyPoints, setLoyalityPoints] = 
    useState(sessionStorage.getItem("loyalityPoints") ? 
    JSON.parse(sessionStorage.getItem("loyalityPoints")) : 
    {loyality_available_points:0.0,
    loyality_point:0.0,
    loyality_used:0.0,
    loyality_used_this_month: 0.00})
    const [MyCartItems, SetMyCartItems] = useState([])
    const [availableCoupons, setAvailableCoupons] = useState([])
    const [DeliveryMethodType, setDeliveryMethodType] = useState( sessionStorage.getItem("DeliveryMethodType") ?  JSON.parse(sessionStorage.getItem("DeliveryMethodType")) : '' )
    const [subTotal, setSubTotal] = useState()
    const [totalPrice, setTotalPrice] = useState()
    const [priceLoad ,setPriceLoad] = useState(false)
    const [couponDetailsModal , setCouponDetailsModal] =  useState(false)
    const [DeliveryNote ,setDeliveryNote] = useState(sessionStorage.getItem("DeliveryNote") ? JSON.parse(sessionStorage.getItem("DeliveryNote")) : '')
    const [usedLoyality, setUsedLoyality] = useState( sessionStorage.getItem("usedLoyality") ? JSON.parse(sessionStorage.getItem("usedLoyality")) : false)
    const [DeliveryFee, setDeliveryFee] = useState(0)
    const [userID, setUserID] = useState(localStorage.getItem('userID') ? localStorage.getItem('userID') : '')
    const [cookingTime, setCookingTime] = useState(0)
    const [isDeliveryChargeApplied, setDeliveryChargeApplied] = useState(false)
    const [DeliveryInfo , setDeliveryInfo] = useState({
                                              flat: 0,
                                              free: 0
                                              })
    const { TextArea } = Input;
    
    useEffect(() => {
        // getDeliveryCharges()
        cartItems = localStorage.getItem("cart") ? JSON.parse(localStorage.getItem("cart")) : []
        SetMyCartItems(cartItems)
        getCartItems()
        CalculateTotal()
        getCookingTime(cartItems)
    
    }, [props.productList])

    useEffect(() => {
      getLoayaltyPoints()
      getCoupons()
    },[])

    const getCookingTime = (cartItems) => {
      if(cartItems != undefined || cartItems && cartItems.length > 0){
      const cookingTime = Math.max.apply(Math, cartItems.map(function(o) { return o.cooking_time; }))
      setCookingTime(cookingTime)
      }else {
        setCookingTime(0)
      }
    }

  
    const getCoupons = async() =>{
      const res = await getData(`${REACT_APP_API_URL}/api/v1/outlet/couponall?page=0`)
      try {
        if(res.data.msg == 'Operation Successful'){
          setAvailableCoupons(res.data.data)
        }

      } catch (error) {
        
      }
    }
    
    const CalculateTotal = () => {
      setPriceLoad(true)
      cartItems = localStorage.getItem("cart") ? JSON.parse(localStorage.getItem("cart")) : []
      let totalPrice = cartItems.reduce(function (accumulator, item) {
        return accumulator + (item.discount_price == "0.00"? (+item.price * item.quantity ): (+item.discount_price * item.quantity));
      }, 0); 
      setSubTotal(totalPrice)
      if(usedLoyality === true){
        console.log("if")
        const loyalityPointAmmount = loyaltyPoints.loyality_available_points * loyalityToOmrRate; 
        console.log("loyalityPointAmmount", loyalityPointAmmount)
        getCurrentPrice(totalPrice, loyalityPointAmmount)
      }else if (usedLoyality === false){
        console.log("else IF")
        getCurrentPrice(totalPrice, 0)
        
      }
      setPriceLoad(false)
    }

    const getCurrentPrice = (price, loyalityAmmount) => {
      const vetAmmount = (+price*vatPercentage)/100;
      const totalPriceAfterVat = vetAmmount + price;
      if(price <= DeliveryInfo.free){
       
        const totalAndDelivery =((parseFloat(totalPriceAfterVat) + parseFloat(DeliveryInfo.flat)))
        const currentTotal =  (totalAndDelivery - parseFloat(loyalityAmmount))
        setTotalPrice(currentTotal)
        console.log(totalAndDelivery, currentTotal)
        setDeliveryChargeApplied(true)
        setDeliveryFee(DeliveryInfo.flat)
        console.log("total IF",totalPriceAfterVat, loyalityAmmount, currentTotal)
      }else{
        const currentTotal =  (parseFloat(totalPriceAfterVat) - parseFloat(loyalityAmmount))
        setTotalPrice(currentTotal)
        setDeliveryChargeApplied(false)
        setDeliveryFee(0)
        console.log("total ELSE",totalPriceAfterVat, loyalityAmmount, currentTotal)
      }
    }

    const getLoayaltyPoints = async() => {
     const formdata = new FormData()
     formdata.append("emp_id", userID)
     const res = await postData(`${REACT_APP_API_URL}/api/v1/outlet/getloyalitybyemp`, formdata)
     try {
            if(res.data.msg == 'Operation Successful'){
              sessionStorage.setItem("loyalityPoints", JSON.stringify(res.data.data[0].point))
              setLoyalityPoints(res.data.data[0].point)
            }
     } catch (error) { 
     }
    }

  
        const getDecrement = (id) => {
              let products = [...props.productList]
              products.map((i) => {
                if (i.prod_id == id) {
                  if (i.quantity > 1) {
                    i.quantity = i.quantity ? i.quantity - 1 : 1
                  }
                  else if (i.quantity === 1) {
                    i.quantity = i.quantity
                    i.isAdd = false
                    // props.removecart(id)
                  }
                  else {
                    i.quantity = 0
                    i.isAdd = false
                    // props.removecart(id)
                  }
                }
              })
           
              let r = localStorage.getItem('cart')
              // console.log("carts", r)
              if (r !=  null) {
                let val = JSON.parse(r)
                val.map((i) => {
                  if (i.prod_id === id) {
                    if (i.quantity > 1) {
                      i.quantity = i.quantity ? i.quantity - 1 : 1
                      localStorage.setItem('cart', JSON.stringify(val))
                    }
                    else if (i.quantity === 1) {
                      i.quantity = i.quantity
                      i.cartprice = i.defPrice
                      i.isAdd = false
                      let a = val.filter((j) => j.prod_id !==  id)
                      localStorage.setItem('cart', JSON.stringify(a))
                    }
                    else {
                      i.quantity = 0
                      i.isAdd = false
                      let a = val.filter((j) => j.prod_id !== id)
                      localStorage.setItem('cart', JSON.stringify(a))
                    }
                  }
                })
          
              }
              props.updateProductList(products)
              CalculateTotal()
              getCookingTime(products)
        }

        const getCounter = (id) => {
          const products = [...props.productList]
             console.log("props", props)
             products.map(function (item) {
              if(item.prod_id ===  id){
                if(item.product_qty == 0)
                item.quantity = item.quantity ? item.quantity + 1 : 1;
                else if(item.product_qty > item.quantity)
                item.quantity = item.quantity ? item.quantity + 1 : 1;
                else{
                  item.quantity = item.quantity ? item.quantity : 1;
                  toast.warning("sorry! item has limited quantity")
                }
               
              }else{
                item.quantity = item.quantity ? item.quantity : 1;
                
              }
            });
          let r = localStorage.getItem('cart')
          if (r !=  null) {
            let val = JSON.parse(r)
            val.map((i) => {
              if (i.prod_id == id) {
                if(i.product_qty == 0)
                i.quantity = i.quantity ? i.quantity + 1 : 1
                else if(i.product_qty > i.quantity)
                i.quantity = i.quantity ? i.quantity + 1 : 1
                else
                i.quantity = i.quantity ? i.quantity : 1
              }else{
                i.quantity = i.quantity ? i.quantity : 1
              }
            })
            localStorage.setItem('cart', JSON.stringify(val))
          }
          else {
            let cart = props.productList.filter((i) => i.isAdd)
            localStorage.setItem('cart', JSON.stringify(cart))
          }

          props.updateProductList(products)
          CalculateTotal()
          getCookingTime(products)

          }

          const handleDeleteItem = (prod) => {
              console.log(prod)
          }

    return (
        <>
        {load ? <div className="page-loader"><Spin /></div> :      <> 

        <div className="cart-items-container">
            <p>{t('cart')} ( {MyCartItems && MyCartItems.length} {t('items')} )</p>

            {MyCartItems && MyCartItems.length > 0 ? MyCartItems.map(prod => {
                return  <div className="cart-items">
                <div className="item-details">
                    <p>{prod.name[props.locale]}</p>
                    <ButtonGroup className="action-btn">
                        <Button size="small" onClick={() => getDecrement(prod.prod_id)} type="ghost" ><FaMinus /></Button>
                        <Button size="small"  type="ghost" >{prod.quantity}</Button>
                        <Button size="small" onClick={() => getCounter(prod.prod_id)} type="ghost" > <FaPlus /></Button>
                    </ButtonGroup>
                </div>
                <div className={`item-action ${props.locale == "en" ? '' : 'text-left'}`}>
                   <p> <FaTrashAlt onClick={() => handleDeleteItem(prod)} /></p>

                    <p><Statistic valueStyle={{ color: '#d7d7d7', fontSize:"16px" }} prefix="SR" value={(prod.discount_price == "0.00" ? prod.price * prod.quantity : prod.discount_price * prod.quantity) } precision={3}/></p>
                </div>
               

            </div>
          
            }) : <div className="no-prod-available" style={{padding:"0px"}}>
            <img  src={process.env.REACT_APP_PUBLIC_URL + '/images/NoItemAvailable.svg' } alt="No Product Available" />
            <p>
              {t('noItemAvailableCart')}
            </p>
            </div> }
        </div>
        <br />
       
       
        
    <Spin spinning={priceLoad} delay={500}>
    <div className="mt-5 border-dashed"></div>
    <Row >
        <Col flex="auto"><p className="checkout-total-heading mb-0">{t('subTotal')}</p>
        <p style={{fontSize:"12px"}}>({t('inclusiveTexes')})</p></Col>
        <Col flex="auto"><p className={`checkout-total-value ${props.locale == "en" ? '' : 'text-left'}`}><Statistic valueStyle={{ color: '#fff', fontSize:"16px" }} prefix="SR" value={subTotal} precision={3}/></p></Col>
    </Row>
    <Row className="mt-3 mb-3 bg-primary pl-3 pr-3 pt-2">
        <Col flex="auto"><p className="checkout-total-heading">{t('cookingTime')}</p></Col>
        <Col flex="auto"><p className={`checkout-total-value ${props.locale == "en" ? '' : 'text-left'}`}><Statistic valueStyle={{ color: '#fff', fontSize:"16px" }} suffix="Minutes" value={cookingTime == "-Infinity" ? 0 : cookingTime} precision={2}/></p></Col>
    </Row>
    </Spin>
    </>
   }
    </>);
});


CartItems.propTypes = {
    addToCart: propTypes.func.isRequired,
    allProducts:propTypes.func.isRequired,
    updateProductList: propTypes.func.isRequired,
    clearCart: propTypes.func.isRequired,
    getCartItems: propTypes.func.isRequired,
    mycart:propTypes.array.isRequired,
    locale: propTypes.string.isRequired,
};

const mapStateToProps = state => ({
    productList:state.product_List.productList,
    category:state.category.category,
    mycart:state.cart.cart,
    locale: state.i18n.locale

  });


const mapDispatchToProps = { addToCart, allProducts, updateProductList, clearCart, getCartItems};

// export default connect(mapStateToProps, mapDispatchToProps)(Products);
export default connect(mapStateToProps, mapDispatchToProps, null, { forwardRef: true })(CartItems)