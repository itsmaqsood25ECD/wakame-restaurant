import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import MyProfileUpdate from '../../containers/myprofile'

export const Profile = (props) => {
  return <div className='container'>
    <MyProfileUpdate />
  </div>;
};

Profile.propTypes = {

};

const mapStateToProps = (state) => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(Profile);

