import * as types from './types'

export function allProducts(data) {

    return {
        type:types.SET_PRODUCT_LIST,
        payload:data
    }
}

export const updateProductList = (Products)=> {
    return async dispatch => {
      await dispatch ({
         type: types.UPDATE_PRODUCT_LIST,
         payload:Products
      })
    }
  }