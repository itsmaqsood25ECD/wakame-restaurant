import React, {useState, useEffect} from 'react'
import {Container} from "reactstrap";
import { Row, Col, Input, Button } from "antd";
import {Link} from 'react-router-dom'
import './AppFooter.css'
import { getData } from '../../services/api';
import {useTranslation} from 'react-i18next'


const {REACT_APP_PUBLIC_URL} = process.env

const Footer = () => {
    const {REACT_APP_BASE_URL} = process.env
    const {t} = useTranslation();
    const [socialLinks, setSocialLinks] = useState({});

    useEffect(()=> {
        getSocialLinks()
    }, [])
  
    const getSocialLinks = async() => {
       const res = await getData(`${REACT_APP_BASE_URL}/api/v1/outlet/social_media`)
        try {
            console.log(res);
            if(res.data.msg == "Operation Successful"){
                console.log(res.data.data)
                setSocialLinks(res.data.data)
            }
        } catch (error) {
            
        }
    }

    return (
        <footer className="footer">
            <Container>
                <Row className='text-center text-md-left'>
                    <Col xs={12} lg={6} >
                        <div className="footer-logo">
                            <img src={REACT_APP_PUBLIC_URL+'/images/Logo_White.png'} alt="footer logo" />
                        </div>
                        <div className="d-flex social-links-outer">
                            {socialLinks.Facebook != '' ? <a className="footer-social-link" href={socialLinks.Facebook} rel="noopener noreferrer" target="_blank">
                                <img src={REACT_APP_PUBLIC_URL+'/images/footer/facebook.svg'} alt="footer-icons" />
                            </a> : ''}
                            {socialLinks.TripAdvisor != '' ? <a className="footer-social-link" href={socialLinks.TripAdvisor} rel="noopener noreferrer" target="_blank">
                                <img src={REACT_APP_PUBLIC_URL+'/images/footer/tripadvisor.svg'} alt="footer-icons" />
                            </a> : ''}
                            {socialLinks.Instagram != '' ? <a className="footer-social-link" href={socialLinks.Instagram} rel="noopener noreferrer" target="_blank">
                                <img src={REACT_APP_PUBLIC_URL+'/images/footer/instagram.svg'} alt="footer-icons" />
                            </a> : ''}
                            {socialLinks.Twitter != '' ? <a className="footer-social-link" href={socialLinks.Twitter} rel="noopener noreferrer" target="_blank">
                                <img src={REACT_APP_PUBLIC_URL+'/images/footer/twitter.svg'} alt="footer-icons" />
                            </a> : ''}
                        </div>
                    </Col>
                    <Col xs={12} lg={6} >
                        <div className="footer-title">
                            {t('footer-aboutus')}
                        </div>
                        <div className="footer-link">
                        <Link to="/">
                        {t('footer-location')}
                        </Link>
                        </div>
                        <div className="footer-link">
                        <Link to="/">
                        {t('footer-delivery')}
                        </Link>
                        </div>
                 
                    </Col>
                    <Col xs={12} lg={6} >
                        <div className="footer-title">
                            {t('footer-menu')}
                        </div>
                        <div className="footer-link">
                        <Link to="/">
                        {t('footer-ourMenu')}
                        </Link>
                        </div>
                        <div className="footer-link">
                        <Link to="/">
                        {t('footer-takeAway')}
                        </Link>
                        </div>
                        <div className="footer-link">
                        <Link to="/">
                        {t('footer-delivery')}
                        </Link>
                        </div>
                        <div className="footer-link">
                        <Link to="/">
                        {t('footer-tableReservation')}
                        </Link>
                        </div>
                    </Col>
                    <Col xs={12} lg={6} >
                        {/* <div className="footer-title">
                            CONTACT US
                        </div>
                        <div className="footer-link">

                        <p>Subscribe to our mailing list</p>
                        </div>
                        <div className="footer-link">
                        <Input.Group compact>
                        <Input placeholder="Enter your Email" type="email" style={{ width: '70%' }} />
                        <Button type="primary" style={{ width: '30%' }}>
                            SEND
                        </Button>
                        </Input.Group>
                       </div> */}
                    </Col>
                    
                </Row>
              
            </Container>
<div className="footer-copyright">
<Container>
<p>@2021 {t('footer-wakame')} {t('footer-allRightReserved')}</p>
</Container>
</div>
        </footer>
    )
}

export default Footer;