import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import Aboutus from '../../containers/aboutus/index'

export const AboutUs = (props) => {
  return <div className='container'>
    <Aboutus />
  </div>;
};

AboutUs.propTypes = {
};

const mapStateToProps = (state) => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(AboutUs);
