import * as types from './types'


export const setActiveBranch = (data) => async dispatch => {

    dispatch({
        type:types.SET_ACTIVE_BRANCH,
        payload:data
    })

}