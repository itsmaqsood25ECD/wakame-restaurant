import React, {useState} from 'react';
import BranchDetails from '../containers/Menu/MenuBranchDetails'
import CategoryList from '../containers/Menu/categoryList'
import ProductList from '../containers/Menu/MenuProducts'
import CartItems from '../containers/checkout/cartItems'
import Cart from '../containers/checkout/cart'
import propTypes from 'prop-types';
import {Row, Col, Button} from 'antd'
import {toast} from 'react-toastify'
import { connect } from 'react-redux';
import { useHistory } from "react-router-dom";
import {useTranslation} from 'react-i18next'

const MainMenu = (props) => {
    let history = useHistory();
    const {t} = useTranslation();
    const [activeBranch, setActiveBranch] = useState(localStorage.getItem('Active_Branch') ? JSON.parse(localStorage.getItem('Active_Branch')) : '' )

  const  handleCheckoutLocation = (branch) => {
      console.log("activeBranch", activeBranch)
      console.log("branch", branch)
        if(activeBranch !== ''){
            if(activeBranch.outlet_busy === "1"){
                toast.warning("Outlet is busy at the moment! please try after sometime ")
            }else{
                history.push("/checkout");
            }
        }
    }

    return (
         <>
         <BranchDetails />
         <section className="container">
             <Row gutter={16}>
                 <Col xs={24} lg={4} md={6} style={props.locale == 'en' ? {borderRight:"1px solid #aaa", padding:"0px"} : {borderLeft:"1px solid #aaa", padding:"0px"}}>
                  <CategoryList  />
                 </Col>
                 <Col xs={24} lg={14} md={12}>
                 <ProductList />
                 </Col>
                 <Col xs={24} lg={6} md={6}>
                     <Cart />
                     <Button onClick={() => handleCheckoutLocation(activeBranch)} className="mb-5 mt-5" type="primary" block size="large">{t('checkout')}</Button>
                 </Col>
             </Row>
          </section>
        </>
    );
};


MainMenu.propTypes = {
    locale: propTypes.string.isRequired,
};

const mapStateToProps = state => ({
    locale: state.i18n.locale
  });
  
  const mapDispatchToProps = { };
  
export default connect(mapStateToProps, mapDispatchToProps)(MainMenu);
  
