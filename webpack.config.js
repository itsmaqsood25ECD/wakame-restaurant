"use strict";
var webpack = require("webpack");
const path = require("path");
try {
  console.log("Hello Nhat");

  const BundleAnalyzerPlugin =
    require("webpack-bundle-analyzer").BundleAnalyzerPlugin;
  const proxy = {
    "/gdc": {
      changeOrigin: true,
      cookieDomainRewrite: "localhost",
      secure: false,
      target: "https://freshdesk-dev.na.gooddata.com",
      headers: {
        host: "https://freshdesk-dev.na.gooddata.com",
        origin: null,
      },
      onProxyReq(proxyReq) {
        proxyReq.setHeader("accept-encoding", "identity");
      },
    },
  };
  const HtmlWebPackPlugin = require("html-webpack-plugin");
  const { CleanWebpackPlugin } = require("clean-webpack-plugin");
  const MiniCssExtractPlugin = require("mini-css-extract-plugin");
  module.exports = {
    entry: {
      main: ["@babel/polyfill", `${process.cwd()}/src/index.js`],
    },
    output: {
      globalObject: "this",
      path: `${process.cwd()}/app/scripts`,
      filename: "[name].[contenthash:8].js",
      chunkFilename: "[name].[contenthash:8].js",
      publicPath: "./scripts",
    },
    devtool: "source-map",
    devServer: {
      proxy,
    },
    resolve: {
      mainFields: ["module", "browser", "main"],
      // polyfill "process" and "util" for lru-cache, webpack 5 no longer does that automatically
      fallback: {
        process: require.resolve("process/browser"),
        util: require.resolve("util/"),
        http: require.resolve("stream-http"),
        https: require.resolve("https-browserify"),
        stream: require.resolve("stream-browserify"),
        zlib: require.resolve("browserify-zlib"),
        path: require.resolve("path-browserify"),
      },
      alias: {
        "./lodash.min": "lodash/lodash.js",
      },
    },
    module: {
      rules: [
        {
          test: /\.(js|jsx|ts|tsx|test.js)$/,
          exclude: /node_modules/,
          use: {
            loader: "babel-loader",
            options: {
              plugins: ["lodash"],
              presets: [["@babel/env", { targets: { node: 6 } }]],
            },
          },
        },
        {
          test: /\.(css|scss)$/,
          use: ["style-loader", "css-loader"],
        },
        {
          test: /\.(jpe?g|gif|png|svg|ico|eot|woff2?|ttf)$/,
          use: [
            {
              loader: "file-loader",
              options: {
                name: "[name][contenthash:8].[ext]",
                outputPath: "/assets/resource",
                esModule: false,
              },
            },
          ],
        },
        {
          test: /\.html$/,
          use: [
            {
              loader: "html-loader",
            },
          ],
        },
        {
          test: /\.jsx?$/,
          loader: "babel-loader",
          include: [
            // transpile code of react-intl to work in IE11
            // https://formatjs.io/docs/react-intl/upgrade-guide-3x/#webpack
            path.join(__dirname, "node_modules/react-intl"),
            path.join(__dirname, "node_modules/intl-messageformat"),
            path.join(__dirname, "node_modules/intl-messageformat-parser"),
          ],
        },
      ],
    },
    plugins: [
      new CleanWebpackPlugin({
        dangerouslyAllowCleanPatternsOutsideProject: true,
        dry: false,
      }),
      new MiniCssExtractPlugin({
        filename: "[name].[contenthash:8].css",
        chunkFilename: "[name].[contenthash:8].css",
      }),
      new HtmlWebPackPlugin({
        template: `${process.cwd()}/public/index.html`,
        filename: `${process.cwd()}/app/index.html`,
      }),
      // fix "process is not defined" error
      new webpack.ProvidePlugin({
        process: "process/browser",
      }),
      new BundleAnalyzerPlugin({ analyzerMode: "static" }),
      new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
      new webpack.ContextReplacementPlugin(/moment[/\\]locale$/, /en/),
    ],
    optimization: {
      moduleIds: "deterministic",
      runtimeChunk: "single",
      splitChunks: {
        cacheGroups: {
          vendor: {
            test: /[\\/]node_modules[\\/]/,
            name: "vendors",
            priority: -10,
            chunks: "all",
          },
        },
      },
    },
  };
} catch (err) {
  console.log("err", err);
  console.log("Failed");
}
