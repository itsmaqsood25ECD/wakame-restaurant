import React,{useState, useEffect} from 'react';
import propTypes from 'prop-types';
import { connect } from 'react-redux';
import {postData} from '../../services/api'
import {addToCart, clearCart} from '../../redux/actions/cart'
import {allProducts, updateProductList} from '../../redux/actions/products'
import '../styles/product.css'
import {Button, Spin, Card, Row, Col, Statistic } from 'antd'
import {ButtonGroup } from 'reactstrap';
import {toast} from 'react-toastify'
import { FaMinus, FaPlus } from 'react-icons/fa';
import { useTranslation } from 'react-i18next';



const { Meta } = Card;

const Products = (props) => {
  const {t} = useTranslation()
  const{REACT_APP_API_URL, REACT_APP_PUBLIC_URL} = process.env
    let storedBranch = localStorage.getItem("Active_Branch");
    let branches = storedBranch ? JSON.parse(storedBranch): {};
    let cart = localStorage.getItem('cart') ? localStorage.getItem('cart') : '[]'
    let cartItems = JSON.parse(cart)
    const [product, setProductList] = useState([])
    const [BaseImagePath, setBaseImagePath] = useState('')
    const [load, setLoad] = useState(false)

    useEffect(() => {
        getProducts()
        console.log("props ~~ ",props)
    }, [props.category])
  
    useEffect(() => {
        setProductList(props.productList)
    },[props.productList])
    

    async function getProducts () {
        setLoad(true)
        const formData = new FormData();
        formData.append('outlet_id', branches && branches.outlet_id);
        formData.append("cat_id", props.category.cat_id );
        formData.append("page", 0);
        const res = await postData(`${REACT_APP_API_URL}/api/v1/outlet/getproducts`, formData)
        try {
            setLoad(false)
            console.log(res)
            if(res.data.status === 200) {
                setBaseImagePath(res.data.image_path)
                handleUpdateProducts(res.data.data)
              }
            } catch (error) {
            setLoad(false)
        }
    }

    const handleUpdateProducts = (filteredProduct) => {
        console.log("filteredProduct 1", filteredProduct)
        props.updateProductList(filteredProduct)
        console.log("filteredProduct 2", filteredProduct)
        localStorage.setItem('product_list', JSON.stringify(filteredProduct))
        let productList = [...filteredProduct]
        if (cartItems !==null) {
          cartItems = JSON.parse(cart)
          // console.log("cart123", cartItems, cartItems.length)
          let items = cartItems
          if (cartItems.length > 1) {
            let uniq = {}
            items = cartItems.filter(obj => !uniq[obj.prod_id] && (uniq[obj.prod_id] = true))
          }
          items.forEach((i) => {
            props.addToCart(i)
          })
          props.clearCart(items)
        }
    
        if (cartItems !==null && cartItems.length > 0) {
          filteredProduct.map((i) => {
            cartItems.map((j) => {
              if (j.prod_id == i.prod_id) {
                i.isAdd = true;
                i.quantity = j.quantity;
                
                localStorage.setItem('product_list', JSON.stringify(productList))
    
              }
            })
          })
        }

        props.updateProductList(productList)

    }
  
    const AddItemToCart = (item) => {
      if (cartItems !==null && cartItems.length > 0) {
         if( cartItems[0].outlet_id == item.outlet_id){
          AddedToCart(item)
         }else {
           localStorage.removeItem('cart')
           AddedToCart(item)
         }
        }else {
          AddedToCart(item)
        }
      }

    const AddedToCart = (item) => {
      let products = [...props.productList]
      products.map((i) => {
        if (i.prod_id === item.prod_id) {
          return i.isAdd = true,
            i.quantity = i.quantity ? i.quantity : 1
        }
      })
      props.addToCart(item)
      setTimeout(async () => {
        let cart = props.productList.filter((i) => i.isAdd)
        let uniq = {}
        let r = localStorage.getItem('cart')
        if (r == null) {
          // console.log("first add", cart)
          localStorage.setItem('cart', JSON.stringify(cart))
        }
        else {
          let totalCart = cart.concat(JSON.parse(r))
          let uniqueItems = totalCart.filter(obj => !uniq[obj.prod_id] && (uniq[obj.prod_id] = true))
  
          localStorage.setItem('cart', JSON.stringify(uniqueItems))
          props.clearCart(uniqueItems)
        }
        
        props.updateProductList(products)
        
      }, 100)
    }


        const getDecrement = (id) => {

            let products = [...props.productList]
              products.map((i) => {
                if (i.prod_id == id) {
                  if (i.quantity > 1) {
                    i.quantity = i.quantity ? i.quantity - 1 : 1
                  }
                  else if (i.quantity === 1) {
                    i.quantity = i.quantity
                    i.isAdd = false
                    // props.removecart(id)
                  }
                  else {
                    i.quantity = 0
                    i.isAdd = false
                    // props.removecart(id)
                  }
                }
              })
           
              let r = localStorage.getItem('cart')
              // console.log("carts", r)
              if (r !=  null) {
                let val = JSON.parse(r)
                val.map((i) => {
                  if (i.prod_id === id) {
                    if (i.quantity > 1) {
                      i.quantity = i.quantity ? i.quantity - 1 : 1
                      localStorage.setItem('cart', JSON.stringify(val))
                    }
                    else if (i.quantity === 1) {
                      i.quantity = i.quantity
                      i.cartprice = i.defPrice
                      i.isAdd = false
                      let a = val.filter((j) => j.prod_id !==  id)
                      localStorage.setItem('cart', JSON.stringify(a))
                    }
                    else {
                      i.quantity = 0
                      i.isAdd = false
                      let a = val.filter((j) => j.prod_id !== id)
                      localStorage.setItem('cart', JSON.stringify(a))
                    }
                  }
                })
          
              }

              props.updateProductList(products)
              
        }

        const getCounter = (id) => {

            const products = [...props.productList]
             console.log("props", props)
             products.map(function (item) {
              if(item.prod_id ===  id){
                if(item.product_qty == 0)
                item.quantity = item.quantity ? item.quantity + 1 : 1;
                else if(item.product_qty > item.quantity)
                   item.quantity = item.quantity ? item.quantity + 1 : 1;
                else{
                  item.quantity = item.quantity ? item.quantity : 1;
                  toast.warning("sorry! item has limited quantity")
                }
                
              }else{
                item.quantity = item.quantity ? item.quantity : 1;
                
              }
            });
          
          
          let r = localStorage.getItem('cart')
          if (r !=  null) {
            let val = JSON.parse(r)
            val.map((i) => {
              if (i.prod_id == id) {
                if(i.product_qty == 0)
                i.quantity = i.quantity ? i.quantity + 1 : 1
                else if(i.product_qty > i.quantity)
                i.quantity = i.quantity ? i.quantity + 1 : 1
                else
                i.quantity = i.quantity ? i.quantity : 1
              }else{
                i.quantity = i.quantity ? i.quantity : 1
              }
            })
            localStorage.setItem('cart', JSON.stringify(val))
          }
          else {
            let cart = props.productList.filter((i) => i.isAdd)
            localStorage.setItem('cart', JSON.stringify(cart))
          }

          props.updateProductList(products)

          }



    return (
            <>
            {load ? <div className="page-loader"><Spin /></div> :  <> <p className="active-category-name">{props && props.category && props.category.cat_name && `${props.category.cat_name[props.locale]} ( ${product && product.length} )`} </p>
            <Row gutter={15}>
            {props.productList && props.productList.length > 0 ? props.productList.map(prod => {
            return  <Col className='pl-0 pr-0' xs={24} md={8} lg={8} style={{marginBottom:"20px"}}> 
            <Card
            className="item-card d-none d-md-block"
            hoverable
            loading={load}
            style={{ width: '100%', marginTop:"10px",height:"100%" }}
            cover={prod.image != '' ? <img className="item-image" alt={prod.name[props.locale]} src={BaseImagePath + prod.image} /> : <img className="item-image" src={REACT_APP_PUBLIC_URL+ "/images/NO_IMG.jpg"} alt={prod.name[props.locale]}></img>}
            actions={[
            <div className="text-left p-2">
            {prod.discount_price == "0.00" ? <Statistic className="item-price" prefix="SR" value={prod.price} precision={3}/> : <div><Statistic className="item-price item-price-before" prefix="SR" value={prod.price} precision={3}/> <Statistic className="item-price item-price-after" prefix="SR" value={prod.discount_price} precision={3}/></div>}
            </div>,
            <>{prod.hasOwnProperty("isAdd") && prod.isAdd === true ? <ButtonGroup><Button onClick={() => getDecrement(prod.prod_id)} size="middle" type="primary" ><FaMinus /></Button><Button size="middle"  type="primary" >{prod.quantity}</Button><Button onClick={() => getCounter(prod.prod_id)} size="middle" type="primary" > <FaPlus /></Button></ButtonGroup>  : <Button className="item-add-btn" onClick={() => AddItemToCart(prod)} type="dashed" >ADD</Button>}</>,
            ]}
            >
            <Meta title={prod.name[props.locale]} description={prod.description[props.locale]} />
            </Card>
            <Row className='item-mobile-card d-md-none d-flex pl-0 pr-0'>
              <Col xs={8}>
              {prod.image != '' ? <img className="item-image-mobile " alt={prod.name[props.locale]} src={BaseImagePath + prod.image} /> : <img className="item-image-mobile" src={REACT_APP_PUBLIC_URL+ "/images/NO_IMG.jpg"} alt={prod.name[props.locale]}></img>}
              </Col>
              <Col xs={16}>
                <div className='item-content-mobile'>
                  <p className='item-title-mobile'>{prod.name[props.locale]}</p>
                  <p className='item-title-description'>{prod.description[props.locale]}</p>
                  <div className='d-flex justify-content-between'>
                  <div className="text-left item-content-mobile-price pt-2 pb-2">
                  {prod.discount_price == "0.00" ? <Statistic className="item-price" prefix="SR" value={prod.price} precision={3}/> : <><Statistic className="item-price item-price-before" prefix="SR" value={prod.price} precision={3}/> <Statistic className="item-price item-price-after" prefix="SR" value={prod.discount_price} precision={3}/></>}
                  </div>
                  <div>{prod.hasOwnProperty("isAdd") && prod.isAdd === true ? <ButtonGroup><Button onClick={() => getDecrement(prod.prod_id)} size="middle" type="primary" ><FaMinus /></Button><Button size="middle"  type="primary" >{prod.quantity}</Button><Button onClick={() => getCounter(prod.prod_id)} size="middle" type="primary" > <FaPlus /></Button></ButtonGroup>  : <Button className="item-add-btn item-add-btn-mobile" onClick={() => AddItemToCart(prod)} type="solid" >ADD</Button>}</div>
                  </div>
                </div>
              </Col>
            </Row>
            </Col>
            }) : <> {props.productList && props.productList.length === 0 && <div className="no-prod-available">
            <img  src={process.env.REACT_APP_PUBLIC_URL + '/images/NoItemAvailable.svg' } alt="No Product Available" />
            <p>
             {t('noItemAvailable')}
            </p>
            </div> } </>}
            </Row>
            </>
            }
            </>);
            };


Products.propTypes = {
    addToCart: propTypes.func.isRequired,
    allProducts:propTypes.func.isRequired,
    updateProductList: propTypes.func.isRequired,
    clearCart: propTypes.func.isRequired,
    locale: propTypes.string.isRequired,
};

const mapStateToProps = state => ({
    productList:state.product_List.productList,
    category:state.category.category,
    cart:state.cart.cart,
    locale: state.i18n.locale

  });


  const mapDispatchToProps = { addToCart, allProducts, updateProductList, clearCart};


export default connect(mapStateToProps, mapDispatchToProps)(Products);
