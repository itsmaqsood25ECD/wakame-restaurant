import { Alert, Button } from 'antd'
import React, { useEffect, useState } from 'react'
import { connect } from 'react-redux'
import { postData } from '../services/api'
import {Row, Col, Statistic, Spin} from 'antd'
import moment from 'moment'
import {ArrowLeftOutlined } from '@ant-design/icons';
import './styles/order-list.css'
import propTypes from 'prop-types';
import { useTranslation } from "react-i18next";

export const OrderList = (props) => {

const {t} = useTranslation()
const {REACT_APP_API_URL, REACT_APP_PUBLIC_URL} = process.env
const [userID, setUserID] = useState(localStorage.getItem('userID') ? localStorage.getItem('userID') : '')
const [orderList, setOrderList] = useState([])
const [upcomingOrder, setUpcomingOrder] = useState([])
const [orderHistory, setOrderHistory] = useState([])
const [orderDetails, setOrderDetails] = useState('')
const [toggleOrderDetails, setToggleOrderDetails] = useState(false)
const [page, setPage] = useState(0)
const [Load, setLoad] = useState(false)
useEffect(() => {
    getAllOrders()
}, []);

useEffect(()=> {

},[orderDetails])

const getAllOrders = async() => {
setLoad(true)
const formData = new FormData()
formData.append('emp_id', userID)
formData.append('page', page)
const res = await postData(`${REACT_APP_API_URL}/api/v1/outlet/orderviewbyuserid`, formData)
try {
if(res.data.msg === "Operation Successful"){
setOrderList(res.data.data)

console.log("order", res.data.data) 
if(res.data.data.length > 0){
    const comingOrder = res.data.data.filter((odr) => {
        return odr.order_status == 'neworder'
    })
    const orderHistory =  res.data.data.filter((odr) => {
        return odr.order_status != 'neworder'
    })
    setUpcomingOrder(comingOrder)
    setOrderHistory(orderHistory)
}
}
setLoad(false)
} catch (error) {
setLoad(false)
}
}

const getFullAddress = (address) => {
if(address != null || address != undefined || address != '', address != 'undefined'){
const add = address
const parseAddress = JSON.parse(add)
return parseAddress && parseAddress.complete_address
}else{
return 'Default Address Not Available'
}
}
const CancelOrder = async(order) => {
    setLoad(true)
    const formData = new FormData()
    formData.append('order_id', order.order_id)
const res = await postData(`${REACT_APP_API_URL}/api/v1/outlet/cancelorder`, formData)
try {
if(res.data.msg === "Operation Successful"){
    window.location.reload()
    setLoad(false)
}
setLoad(false)
} catch (error) {
setLoad(false)
}
}
const getStatus = (status) => {
    switch(status) {
    case 'confirm':
    return `${t('orderConfirmed')}`;
    case 'pickedup':
    return `${t('orderPickedUp')}`;
    case 'cancel':
    return `${t('orderCancelled')}`;
    case 'neworder':
    return `${t('orderPlaced')}`;
    default:
    return `${t('waitingForStatus')}`;
}
}
const getTimeRemaining = (addedTime) => {
    const timeAdNow = moment(addedTime).format('DDMMYY,hh:mm a')
    const timeSpent = moment(timeAdNow, "DDMMYY,hh:mm a").fromNow();
    const arrayTimeLeft = timeSpent.split(" ")
    const min = arrayTimeLeft[0]
    const minText = arrayTimeLeft[1]
if(minText == "minutes" || minText == 'few' || minText == 'seconds'){
if(min == 'a' || min == 'an' || minText == 'seconds' && min < 60 || minText == 'minutes' && min < 3){
    return true
} else{
    return false
}
}else {
    return false  
}  
} 
const getMinRemaining = (addedTime) => {
    const timeAdNow = moment(addedTime).format('DDMMYY,hh:mm a')
    const timeSpent = moment(timeAdNow, "DDMMYY,hh:mm a").fromNow();
    return timeSpent;
} 
const handleOrderDetails = (order) => {
    console.log(order)
    setOrderDetails(order)
    setToggleOrderDetails(true)
}

return (
<div>
{Load ? <div className="page-loader"><Spin /></div> : <>{toggleOrderDetails == true ? <><>
<p className="mt-3 ml-3 text-white text-uppercase section-subtitle d-flex align-items-center" style={{maxWidth:"max-content", cursor:"pointer"}} onClick={() => setToggleOrderDetails(false)}><ArrowLeftOutlined /><span className="ml-2 mr-2"> {t('orderDetails')}</span></p>
{/* {console.log("orderDetails", orderDetails)} */}
{<div className="order-container">
    <div className="pt-2 pb-2">
    <Row>
        <Col xs={24} md={4}>
        <img className="order-image" src={process.env.REACT_APP_PUBLIC_URL+ "/images/NO_IMG.jpg"} alt=""/>
        </Col>
        <Col xs={24} md={12}>
            <div>
                <p>{orderDetails.outlet_name}</p>
                <p>{getFullAddress(orderDetails.address)}</p>
                <p>{t('order')} #{orderDetails.order_id}</p>
                <div className="d-flex justify-content-between" style={{maxWidth:280}}>
                    {getTimeRemaining(orderDetails.added_at) ? <Button type="ghost" onClick={()=> CancelOrder(orderDetails)} className="btn-danger-outline text-uppercase">{t('cancelOrder')}</Button> : ''} 
                </div>
            </div>
        </Col>
        <Col xs={24} md={8}>
            <div>
            <p>{getTimeRemaining(orderDetails.added_at) ? <div className="text-white text-center">
                <p>{getMinRemaining(orderDetails.added_at)}</p>
                <p className="text-gray text-sm">{t('orderCancelationNote')}</p>
                </div> : <>
                
            {orderDetails.order_status == 'neworder' && <p className="text-white text-right text-lg font-style-bold"> <img src={REACT_APP_PUBLIC_URL + '/images/icons/pending-filled.svg'} alt="pending" /> <span className="pl-2 pr-2">{t('pending')}</span></p>}
            {orderDetails.order_status == 'confirm' && <p className="text-white text-right text-lg font-style-bold"> <img src={REACT_APP_PUBLIC_URL + '/images/icons/checkmark-filled.svg'} alt="confirmed" /> <span className="pl-2 pr-2">{t('orderConfirmed')}</span></p>}
            </>}</p>
            </div>
        </Col>
    </Row>
    <div className="border-2 border-gray border-bottom mt-3 mb-2 w-100"></div>
    </div>
    <div className="mt-2">
    <p className="mt-3 text-white text-uppercase section-subtitle d-flex align-items-center">{t('orderedItems')}</p>
    <Row gutter={16} className="d-flex justify-content-center">
        <Col xs={24} md={8}><p>{t('items')}</p></Col>
        <Col xs={24} md={8}><p>{t('qtyprc')}</p></Col>
        <Col xs={24} md={8}><p className="text-right">{t('subTotal')}</p></Col>
    </Row>
        {orderDetails.order_details.map(item => {
        return <Row gutter={16} className="d-flex justify-content-center">
                <Col xs={24} md={8}><p className="mb-0">{item.name[props.locale]}</p></Col>
                <Col xs={24} md={8}><p className="mb-0">{item.quantity} x {item.discount_price != "0.00" ? item.discount_price : item.price}</p></Col>
                <Col xs={24} md={8} className="text-right"><Statistic valueStyle={{ color: '#fff', fontSize:"16px" }} prefix="SR" value={item.quantity*(item.discount_price != "0.00" ? item.discount_price : item.price)} precision={3}/></Col>
            </Row>
        })}
        <div className="order-divider mt-2 mb-2 w-100"></div>
        <Row gutter={16} className="d-flex justify-content-center">
            <Col xs={24} md={12}><p className="font-weight-bold">{t('totalItem')}</p></Col>
            <Col xs={24} md={12} className="text-right"><Statistic valueStyle={{ color: '#fff', fontSize:"16px" }} prefix="SR" value={orderDetails.total_price} precision={3}/></Col>
        </Row>
        <Row gutter={16} className="d-flex justify-content-center">
            <Col xs={24} md={12}><p className="font-weight-bold text-gray">{t('deliveryCharges')}</p></Col>
            <Col xs={24} md={12} className="text-right"><Statistic valueStyle={{ color: '#fff', fontSize:"16px" }} prefix="SR" value={orderDetails.deliveryfee} precision={3}/></Col>
        </Row>
        <div className="order-divider mt-2 mb-2 w-100"></div>
        <Row gutter={16} className="d-flex justify-content-center">
            <Col xs={24} md={12}><p className="font-weight-bold mb-0">{t('grandTotal')}</p></Col>
            <Col xs={24} md={12} className="text-right"><Statistic valueStyle={{ color: '#fff', fontSize:"16px" }} prefix="SR" value={orderDetails.final_price} precision={3}/></Col>
        </Row>
        <div className="border-2 border-gray border-bottom mt-2 mb-2 w-100"></div>
        <div className="d-flex order-extra-details">
            <div className="odr-title">
            <p className="mb-0 text-gray text-sm">{t('serviceType')}</p>
            </div>
            <div className="odr-desc">
                <p className="text-white ">
                {orderDetails.service_method}
                </p>
            </div>
        </div>
        <div>
        <div className="order-extra-details d-flex ">
            <div className="odr-title">
            <p className="mb-0 text-gray text-sm">{t('orderedOn')}</p>
            </div>
            <div className="odr-desc">
                <p className="text-white ">
                {moment(orderDetails.added_at).format('lll')}
                </p>
            </div>
        </div>
        <div className="order-extra-details d-flex ">
            <div className="odr-title">
            <p className="mb-0 text-gray text-sm">{t('deliveryOn')}</p>
            </div>
            <div className="odr-desc">
            <p className="text-white ">
            {t('notApplicable')}
            </p>
            </div>
        </div>
        
        <div className="order-extra-details d-flex ">
            <div className="odr-title">
            <p className="mb-0 text-gray text-sm">{t('paymentMode')}</p>
            </div>
            <div className="odr-desc">
            <p className="text-white ">
                {orderDetails.payment_method}
            </p>
            </div>
        </div>
        <div className="order-extra-details d-flex ">
            <div className="odr-title">
            <p className="mb-0 text-gray text-sm">{t('loyalityPointUsed')}</p>
            </div>
            <div className="odr-desc">
            <p className="text-white ">
                {orderDetails.loyalityused}
            </p>
            </div>
        </div>
        <div className="order-extra-details d-flex ">
            <div className="odr-title">
            <p className="mb-0 text-gray text-sm">{t('loyalityPointEarned')}</p>
            </div>
            <div className="odr-desc">
            <p className="text-white ">
                {orderDetails.loyalityearned == null ? 0 : orderDetails.loyalityearned}
            </p>
            </div>
        </div>
        <div className="order-extra-details d-flex ">
            <div className="odr-title">
            <p className="mb-0 text-gray text-sm">{t('couponUsed')}</p>
            </div>
            <div className="odr-desc">
            <p className="text-white ">
                {orderDetails.voucher == '' ? `${t('noCouponUsed')}`: orderDetails.voucher}
            </p>
            </div>
        </div>
        <div className="order-extra-details d-flex ">
            <div className="odr-title">
            <p className="mb-0 text-gray text-sm">{t('deliveryAddress')}</p>
            </div>
            <div className="odr-desc">
            <p className="text-white ">
                {JSON.parse(orderDetails.address).complete_address}, {t('address-houseFlat')} : {JSON.parse(orderDetails.address).house_flat_no}, {t('address-landmark')} : {JSON.parse(orderDetails.address).landmark}
            </p>
            </div>
        </div>
        <div className="order-extra-details d-flex ">
            <div className="odr-title">
            <p className="mb-0 text-gray text-sm">{t('deliveryNote')}</p>
            </div>
            <div className="odr-desc">
            <p className="text-white ">
                {orderDetails.order_note == '' ? `${t('noDeliveryNotesAvailable')}` : orderDetails.order_note}
            </p>
            </div>
        </div>
                </div>
    </div>
    
</div>}
</></> : <>
<p className="mt-3 ml-3 text-white text-uppercase section-subtitle">{t('onGoingOrder')}</p>
{upcomingOrder && upcomingOrder.length > 0 ? upcomingOrder.map(order => {
return <div className="order-container">
    <div className="order-divider pt-2 pb-2">
    <Row>
        <Col xs={24} md={4}>
        <img className="order-image" src={process.env.REACT_APP_PUBLIC_URL+ "/images/NO_IMG.jpg"} alt=""/>
        </Col>
        <Col xs={24} md={12}>
            <div>
                <p>{order.outlet_name}</p>
                <p>{getFullAddress(order.address)}</p>
                <p>{t('order')} #{order.order_id}</p>
                <div className="d-flex justify-content-between" style={{maxWidth:280}}>
                    <Button onClick={ () => handleOrderDetails(order)} type="ghost" className="btn-primary-outline">{t('viewDetails')}</Button>
                    {getTimeRemaining(order.added_at) ? <Button type="ghost" onClick={()=> CancelOrder(order)} className="btn-danger-outline text-uppercase">{t('cancelOrder')}</Button> : ''} 
                </div>
            </div>
        </Col>
        <Col xs={24} md={8}>
            <div>
            <p>  
            {order.order_status == 'neworder' && <p className="text-white text-right text-lg font-style-bold"> <img src={REACT_APP_PUBLIC_URL + '/images/icons/pending-filled.svg'} alt="pending" /> <span className="pl-2 pr-2">{t('pending')}</span></p>}
            {order.order_status == 'confirm' && <p className="text-white text-right text-lg font-style-bold"> <img src={REACT_APP_PUBLIC_URL + '/images/icons/checkmark-filled.svg'} alt="confirmed" /> <span className="pl-2 pr-2">{getStatus(order.order_status)}</span></p>}
            </p>
            </div>
        </Col>
    </Row>
    </div>
    <div className="mt-2">
        <Row gutter={16} className="d-flex justify-content-center">
            <Col xs={24} md={8}>
                <p className="mb-0 text-gray text-sm">{t('items')}</p>
                <p className="text-white ">
                {order.order_details.map(item => {
                return <span> {item.name[props.locale]} x {item.quantity},</span>
                })}
                </p>
                
            </Col>
            <Col xs={24} md={8}>
            <p className="mb-0 text-gray text-sm">{t('paymentMode')}</p>
            <p className="text-white ">
                {order.payment_method}
            </p>
            
            </Col>
            <Col xs={24} md={8}>
            <p className="mb-0 text-gray text-sm">{t('totalPrice')}</p>
            <p className="text-white ">
            <Statistic valueStyle={{ color: '#fff', fontSize:"16px" }} prefix="SR" value={order.final_price} precision={3}/>
            </p>
            </Col>
        </Row>

        <Row gutter={16} className="d-flex justify-content-center">
            <Col xs={24} md={8}>
            <p className="mb-0 text-gray text-sm">{t('serviceMode')}</p>
                <p className="text-white ">
                    {order.service_method}
                </p>
            </Col>
            <Col xs={24} md={8}>
            <p className="mb-0 text-gray text-sm">{t('orderedOn')}</p>
            <p className="text-white ">
                {moment(order.added_at).format('lll')}
            </p>
            </Col>
            <Col xs={24} md={8}>
            <p className="mb-0 text-gray text-sm">{t('deliveryOn')}</p>
            <p className="text-white ">
                {getStatus(order.order_status)}
            </p>
            </Col>
        </Row>
    </div>
    
</div>
}) : <div className="no-prod-available" style={{padding:"0px"}}>
<img  src={process.env.REACT_APP_PUBLIC_URL + '/images/NoItemAvailable.svg' } alt="No Product Available" />
<p>
   {t('noOrderPlaced')}
</p>
</div> }
<p className="mt-5 ml-3 text-white text-uppercase section-subtitle">{t('pastOrder')}</p>
{orderHistory && orderHistory.length > 0 ? orderHistory.map(order => {
return <div className="order-container">
<div className="order-divider pt-2 pb-2">
<Row>
    <Col xs={24} md={4}>
    <img className="order-image" src={process.env.REACT_APP_PUBLIC_URL+ "/images/NO_IMG.jpg"} alt=""/>
    </Col>
    <Col xs={24} md={12}>
        <div>
            <p>{order.outlet_name}</p>
            <p>{getFullAddress(order.address)}</p>
            <p>{t('order')} #{order.order_id}</p>
            <div className="d-flex justify-content-between" style={{maxWidth:280}}>
                <Button onClick={ () => handleOrderDetails(order)} type="ghost" className="btn-primary-outline">{t('viewDetails')}</Button>
                
            </div>
        </div>
    </Col>
    <Col xs={24} md={8}>
        {order.order_status == 'cancel' && <p className="text-white text-right text-lg font-style-bold"><img src={REACT_APP_PUBLIC_URL + '/images/icons/cancel-circle.svg'} alt="cancel" /> <span className="pl-2 pr-2">Cancelled</span></p>}
        {order.order_status == 'pickedup' && <p className="text-white text-right text-lg font-style-bold"><img src={REACT_APP_PUBLIC_URL + '/images/icons/checkmark-filled.svg'} alt="pickup" /> <span className="pl-2 pr-2">Pickedup</span></p>}
        {order.order_status == 'delivered' && <p className="text-white text-right text-lg font-style-bold"><img src={REACT_APP_PUBLIC_URL + '/images/icons/checkmark-filled.svg'} alt="delivered" /> <span className="pl-2 pr-2">Delivered</span></p>}
    </Col>
</Row>
</div>
<div className="mt-2">
    <Row gutter={16} className="d-flex justify-content-center">
        <Col xs={24} md={8}>
            <p className="mb-0 text-gray text-sm">{t('items')}</p>
            <p className="text-white ">
            {order.order_details.map(item => {
            return <span> {item.name[props.locale]} x {item.quantity},</span>
            })}
            </p>
            
        </Col>
        <Col xs={24} md={8}>
        <p className="mb-0 text-gray text-sm">{t('paymentMode')}</p>
        <p className="text-white ">
            {order.payment_method}
        </p>
        
        </Col>
        <Col xs={24} md={8}>
        <p className="mb-0 text-gray text-sm">{t('totalPrice')}</p>
        <p className="text-white ">
        <Statistic valueStyle={{ color: '#fff', fontSize:"16px" }} prefix="SR" value={order.final_price} precision={3}/>
        </p>
        </Col>
    </Row>

    <Row gutter={16} className="d-flex justify-content-center">
        <Col xs={24} md={8}>
        <p className="mb-0 text-gray text-sm">{t('serviceMode')}</p>
            <p className="text-white ">
                {order.service_method}
            </p>
        </Col>
        <Col xs={24} md={8}>
        <p className="mb-0 text-gray text-sm">{t('orderedOn')}</p>
        <p className="text-white ">
            {moment(order.added_at).format('lll')}
        </p>
        </Col>
        <Col xs={24} md={8}>
        <p className="mb-0 text-gray text-sm">{t('deliveryOn')}</p>
        <p className="text-white ">
            {getStatus(order.order_status)}
        </p>
        </Col>
    </Row>
</div>

</div>
}) : <div className="no-prod-available" style={{padding:"0px"}}>
<img  src={process.env.REACT_APP_PUBLIC_URL + '/images/NoItemAvailable.svg' } alt="No Product Available" />
<p>
   {t('noOrderPlaced')}
</p>
</div> }
</> } </>}
    
</div>
)
}


OrderList.propTypes = {
locale: propTypes.string.isRequired,
};

const mapStateToProps = (state) => ({
locale: state.i18n.locale
})

const mapDispatchToProps = {

}

export default connect(mapStateToProps, mapDispatchToProps)(OrderList)