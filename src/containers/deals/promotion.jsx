import React, { useEffect, useState } from 'react'
import propTypes from 'prop-types';
import { connect } from 'react-redux'
import { getData } from '../../services/api';
import {Link} from 'react-router-dom'
import {addToCart, clearCart} from '../../redux/actions/cart'
import {allProducts, updateProductList} from '../../redux/actions/products'
import {Button, Card, Row, Col, Statistic } from 'antd'
import {ButtonGroup } from 'reactstrap';
import {toast} from 'react-toastify'
import { FaMinus, FaPlus } from 'react-icons/fa';
import {useTranslation} from 'react-i18next'

const { Meta } = Card;
export const Promotion = (props) => {
    const{REACT_APP_PUBLIC_URL} = process.env
    const {t} = useTranslation()
    let cart = localStorage.getItem('cart') ? JSON.parse(localStorage.getItem('cart')) : ''
    let cartItems = cart
    const Branch = localStorage.getItem('Active_Branch') ? JSON.parse(localStorage.getItem('Active_Branch')) : '';
    const [load, setLoad] = useState(false);
    const [isPromotions, setIsPromotions] = useState(false);
    const [promotion, setPromotion] = useState([]);
    

    useEffect(() => {
        
        if(Branch !== ''){
            getPromotions()
        }else{
            setIsPromotions(false)
        }

    }, [])



    const getPromotions = async() => {
      setLoad(true)
      const res = await getData(`/api/v1/outlet/promotion?outlet_id=${Branch.outlet_id}`)
      try {
        
        if(res.data.msg == "Operation Successful"){
            if(res.data.data.length > 0){
               const promo = res.data.data
                console.log(promo.slice(0,4))
                handleUpdateProducts(promo.slice(0,4))
                setPromotion(promo.slice(0,4))
                setIsPromotions(true)
               
            }
            setLoad(false)
        }
      } catch (error) {
         setLoad(false)
      }
      
    }

    const handleUpdateProducts = (filteredProduct) => {
        props.updateProductList(filteredProduct)
        localStorage.setItem('product_list', JSON.stringify(filteredProduct))
        let productList = [...filteredProduct]
        if (cartItems !== null ) {
          let items = cartItems
          if (cartItems.length > 1) {
            let uniq = {}
            items = cartItems.filter(obj => !uniq[obj.prod_id] && (uniq[obj.prod_id] = true))
          }
          items.forEach((i) => {
            props.addToCart(i)
          })
          props.clearCart(items)
        }
    
        if (cartItems !==null && cartItems.length > 0) {
          filteredProduct.map((i) => {
            cartItems.map((j) => {
              if (j.prod_id == i.prod_id) {
                i.isAdd = true;
                i.quantity = j.quantity;
                localStorage.setItem('product_list', JSON.stringify(productList))
    
              }
            })
          })
        }
        setPromotion(productList)
        props.updateProductList(productList)
        

    }
    const getDecrement = (id) => {

        let products = [...props.productList]
          products.map((i) => {
            if (i.prod_id == id) {
              if (i.quantity > 1) {
                i.quantity = i.quantity ? i.quantity - 1 : 1
              }
              else if (i.quantity === 1) {
                i.quantity = i.quantity
                i.isAdd = false
                // props.removecart(id)
              }
              else {
                i.quantity = 0
                i.isAdd = false
                // props.removecart(id)
              }
            }
          })
       
          let r = localStorage.getItem('cart')
          // console.log("carts", r)
          if (r !=  null) {
            let val = JSON.parse(r)
            val.map((i) => {
              if (i.prod_id === id) {
                if (i.quantity > 1) {
                  i.quantity = i.quantity ? i.quantity - 1 : 1
                  localStorage.setItem('cart', JSON.stringify(val))
                }
                else if (i.quantity === 1) {
                  i.quantity = i.quantity
                  i.cartprice = i.defPrice
                  i.isAdd = false
                  let a = val.filter((j) => j.prod_id !==  id)
                  localStorage.setItem('cart', JSON.stringify(a))
                }
                else {
                  i.quantity = 0
                  i.isAdd = false
                  let a = val.filter((j) => j.prod_id !== id)
                  localStorage.setItem('cart', JSON.stringify(a))
                }
              }
            })
      
          }

          props.updateProductList(products)
          
    }

    const getCounter = (id) => {

        const products = [...props.productList]
         console.log("props", props)
         products.map(function (item) {
          if(item.prod_id ===  id){
            if(item.product_qty == 0)
            item.quantity = item.quantity ? item.quantity + 1 : 1;
            else if(item.product_qty > item.quantity)
               item.quantity = item.quantity ? item.quantity + 1 : 1;
            else{
              item.quantity = item.quantity ? item.quantity : 1;
              toast.warning("sorry! item has limited quantity")
            }
            
          }else{
            item.quantity = item.quantity ? item.quantity : 1;
            
          }
        });
      
      
      let r = localStorage.getItem('cart')
      if (r !=  null) {
        let val = JSON.parse(r)
        val.map((i) => {
          if (i.prod_id == id) {
            if(i.product_qty == 0)
            i.quantity = i.quantity ? i.quantity + 1 : 1
            else if(i.product_qty > i.quantity)
            i.quantity = i.quantity ? i.quantity + 1 : 1
            else
            i.quantity = i.quantity ? i.quantity : 1
          }else{
            i.quantity = i.quantity ? i.quantity : 1
          }
        })
        localStorage.setItem('cart', JSON.stringify(val))
      }
      else {
        let cart = props.productList.filter((i) => i.isAdd)
        localStorage.setItem('cart', JSON.stringify(cart))
      }

      props.updateProductList(products)

      }

      const AddedToCart = (item) => {
        let products = [...props.productList]
        products.map((i) => {
          if (i.prod_id === item.prod_id) {
            return i.isAdd = true,
              i.quantity = i.quantity ? i.quantity : 1
          }
        })
        props.addToCart(item)
        setTimeout(async () => {
          let cart = props.productList.filter((i) => i.isAdd)
          let uniq = {}
          let r = localStorage.getItem('cart')
          if (r == null) {
            // console.log("first add", cart)
            localStorage.setItem('cart', JSON.stringify(cart))
          }
          else {
            let totalCart = cart.concat(JSON.parse(r))
            let uniqueItems = totalCart.filter(obj => !uniq[obj.prod_id] && (uniq[obj.prod_id] = true))
    
            localStorage.setItem('cart', JSON.stringify(uniqueItems))
            props.clearCart(uniqueItems)
          }
          
          props.updateProductList(products)
          
        }, 100)
      }

      const AddItemToCart = (item) => {
        if (cartItems !==null && cartItems.length > 0) {
           if( cartItems[0].outlet_id == item.outlet_id){
            AddedToCart(item)
           }else {
             localStorage.removeItem('cart')
             AddedToCart(item)
           }
          }else {
            AddedToCart(item)
          }
        }

    return (
        isPromotions == true ?  <div className="container">
            <div className="d-flex justify-content-between">
                <div>
                    <p class="text-white text-uppercase section-subtitle"> {t('deal')}</p>
                </div>
                <div>
                    <Link to="/deals">
                    {t('seeAllDeals')}
                    </Link>
                </div>
            </div>
            <Row gutter={15}>
                {console.log("props.productList", props.productList)}
            {promotion.length > 0 && promotion.map((prod) => {
                return <Col xs={12} md={6} lg={6} style={{marginBottom:"20px"}}> 
                    <Card
                        className="item-card"
                        hoverable
                        loading={load}
                        style={{ width: "100%", marginTop:"10px",height:"100%" }}
                        cover={prod.image != '' ? <img className="item-image" alt="example" src={'https://wakameuat.upappfactory.app/wakame-admin/uploads/' + prod.image} /> : <img className="item-image" src={REACT_APP_PUBLIC_URL+ "/images/NO_IMG.jpg"} height="200" alt="poduct image"></img>}
                        actions={[
                            <div className="text-left p-2">
                              {prod.discount_price == "0.00" ? <Statistic className="item-price" prefix="SR" value={prod.price} precision={3}/> : <div><Statistic className="item-price item-price-before" prefix="SR" value={prod.price} precision={3}/> <Statistic className="item-price item-price-after" prefix="SR" value={prod.discount_price} precision={3}/></div>}
                              </div>,
                            <>{prod.hasOwnProperty("isAdd") && prod.isAdd === true ? <ButtonGroup><Button onClick={() => getDecrement(prod.prod_id)} size="middle" type="primary" ><FaMinus /></Button><Button size="middle"  type="primary" >{prod.quantity}</Button><Button onClick={() => getCounter(prod.prod_id)} size="middle" type="primary" > <FaPlus /></Button></ButtonGroup>  : <Button className="item-add-btn" onClick={() => AddItemToCart(prod)} type="dashed" >{t('add')}</Button>}</>,
                          ]}
                          >
                          <Meta title={prod.name[props.locale]} description={prod.description[props.locale]} />
                          </Card>
                </Col>

            })}
            </Row>
        </div> : ''
    )
}

Promotion.propTypes = {
    addToCart: propTypes.func.isRequired,
    allProducts:propTypes.func.isRequired,
    updateProductList: propTypes.func.isRequired,
    clearCart: propTypes.func.isRequired,
    locale: propTypes.string.isRequired,

}

const mapStateToProps = (state) => ({
    productList:state.product_List.productList,
    category:state.category.category,
    cart:state.cart.cart,
    locale: state.i18n.locale
})

const mapDispatchToProps = {
    addToCart, allProducts, updateProductList, clearCart
}

export default connect(mapStateToProps, mapDispatchToProps)(Promotion)


