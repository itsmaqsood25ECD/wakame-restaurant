import React,{useState} from 'react'
import { Form, Button, Input, Spin } from 'antd';
import {  toast } from 'react-toastify';
import { postData } from '../../services/api';
import { useHistory } from "react-router-dom";
import propTypes from 'prop-types';
import {connect} from 'react-redux';
import { useTranslation } from "react-i18next";

const {TextArea} = Input;

const Index = (props) => {
    
    const {t} = useTranslation()
    const {REACT_APP_API_URL} = process.env
    const [userDetails, setUserDetails] = useState({})
    const [userID, setUserID] = useState(localStorage.getItem('userID') ? localStorage.getItem('userID') : '')
    const [load, setLoad] = useState(false)
    const [form] = Form.useForm()
    let history = useHistory();
    const onFinish = async(value) => {
        setLoad(true)
        const formData = new FormData();
        formData.append('emp_id', userID)
        formData.append('name', value.firstname+ " " + value.lastname)
        formData.append('phone', value.phone)
        formData.append('email', value.email)
        formData.append('feedback', value.feedback)
        const res = await postData(`${REACT_APP_API_URL}/api/v1/outlet/feedback`, formData)
        try {
            setLoad(false)
            if(res.data.msg == "Operation Successful"){
                toast.success("updated successfully")
                history.push("/feedback/success");
            }else{
                toast.success("oops! something went wrong") 
            }


        } catch (error) {
            
        }
        console.log(value)
       
    }



    return (
        <div>
        
        <Spin spinning={load} >

           <Form 
              name="feedback-form"
              className="feedback-form" 
              form={form}
              size="large"
              onFinish={onFinish}
            >
                    <Form.Item
                        name="firstname"
                        rules={[{
                            required: true,
                            message:  `${t('err-firstName')}`
                        }
                    ]}>
                        <Input
                           name="name"
                            type="text"
                            varient="bottom-outline"
                            placeholder={t('firstName')}/>
                    </Form.Item>
                    <Form.Item
                        name="lastname"
                        rules={[{
                            required: true,
                            message: `${t('err-lastName')}`
                        }
                    ]}>
                        <Input
                           name="lastname"
                            type="text"
                            varient="bottom-outline"
                            placeholder={t('lastName')}/>
                    </Form.Item>
                    <Form.Item
                        name="phone"
                        rules={[{
                            required: true,
                            message: `${t('err-mobileNumber')}`
                        }
                    ]}>
                        <Input
                        maxlength='9'
                        addonBefore="+966"
                        type="number"
                        min="0"
                        max="999999999"
                        varient="bottom-outline"
                        placeholder={t('mobileNumber')}/>
                    </Form.Item>
                    <Form.Item
                        name="email"
                        rules={[{
                            required: true,
                            message:`${t('err-emailId')}`
                        }]}
                    >
                        <Input
                        name="email"
                            type="email"
                            placeholder={t('emailId')}
                            varient="bottom-outline"/>
                    </Form.Item>
                    <Form.Item 
                        name="feedback"
                        rules={[{
                            required: true,
                            message: `${t('err-feedback')}`
                        }]}>
                    <TextArea  varient="bottom-outline" showCount maxLength={500}  />
                    </Form.Item>

                    <Form.Item>
                        <Button type="primary" htmlType="submit" className="mt-3 text-uppercase login-form-button">
                          Sumbit
                        </Button>
                    </Form.Item>
                </Form>
           
            </Spin>
        </div>
    )
}

Index.propTypes = {
    locale: propTypes.string.isRequired,
  };
  
  const mapStateToProps = state => ({
      locale: state.i18n.locale
    });
    
    const mapDispatchToProps = { };
    
  export default connect(mapStateToProps, mapDispatchToProps)(Index);
  
