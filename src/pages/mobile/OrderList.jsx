import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import OrderHistory from '../orderList'

export const OrderList = (props) => {
  return <div className='container'>
    <OrderHistory />
  </div>;
};

OrderList.propTypes = {
 
};

const mapStateToProps = (state) => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(OrderList);
