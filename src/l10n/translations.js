const translations = {
    en: {
      concerts: {
        title: "Shows",
      },
      comments: {
        title: "%{count} comments",
        title_0: "%{count} comments",
        title_1: "%{count} comment",
      },
    },
    ar: {
      concerts: {
        title: "الحفلات",
      },
      comments: {
        title: "%{count} تعليقات",
        title_0: "%{count} comments",
        title_1: "%{count} comment",
      },
    },
  };
  export default translations;