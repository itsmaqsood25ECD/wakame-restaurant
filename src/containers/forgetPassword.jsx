import React,{useState, useEffect} from "react";
import {useDispatch, useSelector} from 'react-redux'
import {showLoginModal, showForgetPassModal, showResetPassModal} from '../redux/actions/toggleAction'
import {Form, Input, Button, Modal, Spin} from 'antd';
import SignUpForm from './signup'
import './styles/login.css'
import {  toast } from 'react-toastify';
import {postData} from '../services/api'
import OtpInput from 'react-otp-input';
import {useTranslation} from 'react-i18next'
import propTypes from 'prop-types';
import { connect } from 'react-redux';
import { t } from "i18next";


const ForgetPassword = (props) => {

    const {REACT_APP_API_URL} = process.env
    const [visible,
        setVisible] = useState()
        const [phone, setPhone] =  useState()
        const [numberEdit, setNumberEdit] = useState(false)
        const [sentOTP, setOTPSent] = useState(false)
        const [otp, setOTP] = useState('')
        const [otpRecived, setOtpRecived] = useState('')
        const [load, setLoad] = useState(false)

    const dispatch = useDispatch();

    const state_visible = useSelector(state => state.toggleStack.showForgetPassModal)

    useEffect(() => {
        setVisible(state_visible);
    }, [state_visible]);

    const onClose = () => {
        dispatch(showForgetPassModal(false))
        dispatch(showLoginModal(true))

    };

  


    const handleforgetPassword = async () => {
        var matchingCase = /^\+?([0-9]{2})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{3})$/;
        if(!(phone.match(matchingCase))){
            toast.error("Enter Valid Mobile Number")
            }else{
                setLoad(true)
                const formData = new FormData();
                formData.append("phone", phone)
                setPhone(phone)
                console.log(formData, phone)
                const res = await postData(`${REACT_APP_API_URL}/api/v1/outlet/forget_password`, formData)
                try {
                   console.log("res", res)
                    if(res.data.status === 200){
                    setOtpRecived(res.data.data)
                    setNumberEdit(true)
                    setLoad(false)
                    setOTPSent(true)
                    toast.success("OTP sent")
                   }else{

                       console.log(res)
                       setLoad(false)
                       toast.error(res.data.msg)
                   }
                   
               } catch (error) {
                setLoad(false)
               }
            }
    }

    const verifyOTP = () => {
        if(otp == otpRecived){
            sessionStorage.setItem("mobiletoReset", phone)
            dispatch(showForgetPassModal(false))
            dispatch(showResetPassModal(true))
        }else {
            toast.error("Otp is not correct Please Try again !")
        }
        
    }

    return (
        <div>
          
            <Modal width={400} title="" footer={null} visible={visible} onCancel={onClose}>
                
               <Spin spinning={load} >

                        <div className="center">
                    <h2>{t('forgetPassword')}</h2>
                    <p></p>
                </div>
                <Form // id="components-form-normal-login"
                    name="normal_login" className="login-form" initialValues={{
                    remember: false,
                    username:'',
                    password:''

                }} size="large" >
                <br/>
                     <Form.Item
                        name="mobile"
                        rules={[{
                            required: true,
                            message:`${t('err-mobileNumber')}`
                        }
                    ]}>
                        <Input
                        maxlength='9'
                        addonBefore="+966"
                        type="number"
                        min="0"
                        max="999999999"
                        varient="bottom-outline"
                        disabled={numberEdit}
                        name="phone"
                        onChange={(e) => setPhone(e.target.value)}
                        placeholder={t('mobileNumber')}
                            />
                    </Form.Item>
                    <Form.Item>
                    <Button onClick={() => setNumberEdit(!numberEdit)} type="link" className="t">
                        {t('changeNumber')}
                        </Button>

                        <Button onClick={handleforgetPassword} type="link" className="login-form-forgot" >
                        {t('getOTP')}
                        </Button>
                    </Form.Item>
 
                   {sentOTP && <>
                   
                   <p>{t('otpSucces')} {phone}</p>
                   <Form.Item>
 <OtpInput
    value={otp}
    onChange={(e) => setOTP(e)}
    numInputs={6}
    isInputNum={true}
    containerStyle={{justifyContent:"center"}}
    inputStyle="ant-input otp-input-box"
    separator={<span></span>}
/>
 </Form.Item>
<Form.Item>
    <Button type="primary" onClick={verifyOTP} className="login-form-button">
    {t('verify')}
    </Button>
</Form.Item> 
</>} 


                   
                </Form>
           </Spin>
           </Modal>
            <SignUpForm/>
        </div>
    );
};



ForgetPassword.propTypes = {
    locale: propTypes.string.isRequired,
};

const mapStateToProps = state => ({
    locale: state.i18n.locale
  });
  
  const mapDispatchToProps = { };
  
export default connect(mapStateToProps, mapDispatchToProps)(ForgetPassword);
