import React,{ useState, useEffect } from "react";
import {Drawer,Spin, Form, Input, Button} from 'antd'
import {useDispatch, useSelector} from 'react-redux'
import {showAddressDrawer} from '../../redux/actions/toggleAction'
// import {setAddress} from '../redux/actions/adress'
import PlacesAutocomplete from 'react-places-autocomplete';
import {
  geocodeByAddress,
  getLatLng,
} from 'react-places-autocomplete';
import { Map, Marker } from 'google-maps-react'
import Geocode from "react-geocode";
import {postData} from '../../services/api'
import { toast } from "react-toastify";
import { useTranslation } from "react-i18next";
import {connect} from 'react-redux'

const AddressManagment = (props) => {
  const {t} = useTranslation()
  Geocode.setApiKey("AIzaSyBDo7DxFYD-1vsWruVTR-w-ZifQXTeY5-o");
  Geocode.setLanguage("en");
  Geocode.setRegion("ind");
  Geocode.setLocationType("ROOFTOP");
  Geocode.enableDebug();
  const {REACT_APP_API_URL} = process.env;  
  const [visible, setVisible] = useState()
  const [address, setAddress] = useState('')
  const [centerMap, setCenterMap] = useState({
      lat: 35.5496939,
      lng: -120.7060049
      })
    const [form] = Form.useForm()
    const dispatch = useDispatch();
    
    const state_visible = useSelector( state => state.toggleStack.showAddressDrawer)

     useEffect(() => {
        setVisible(state_visible);       
     }, [state_visible, ]);

     useEffect(() => {
      form.setFieldsValue({
        complete_address:address
       })
     }, [form, address])

    

    const dragComplete = (one, two, three) => {
    
      const { latLng } = three;
       const lat = latLng.lat();
       const lng = latLng.lng();
        console.log(lat, lng);
        const latlng = {
          lat:lat,
          lng:lng
        }
        setCenterMap({
          ...centerMap,
          lat:lat,
          lng:lng
        })

        HandleGeoLocation(latlng)


    }


   const handleSelect = address => {
      geocodeByAddress(address)
        .then(results => {
          getLatLng(results[0])
          console.log(results)
          setAddress(results[0].formatted_address)
          setCenterMap({
            ...centerMap,
            lat:results[0].geometry.location.lat(),
            lng:results[0].geometry.location.lng()
          })
        })
        .then(latLng => {
          console.log('Success', latLng)
          HandleGeoLocation(latLng)

          setCenterMap({
            ...centerMap,
            lat:latLng.lat,
            lng:latLng.lng
          })
        } )
        .catch(error => console.error('Error', error));
    };
     
     const HandleGeoLocation = (latLng) => {

      Geocode.fromLatLng(latLng.lat, latLng.lng).then(
        (response) => {
          const address = response.results[0].formatted_address;
          setAddress(address);
          let city, state, country;
          for (let i = 0; i < response.results[0].address_components.length; i++) {
            for (let j = 0; j < response.results[0].address_components[i].types.length; j++) {
              switch (response.results[0].address_components[i].types[j]) {
                case "locality":
                  city = response.results[0].address_components[i].long_name;
                  break;
                case "administrative_area_level_1":
                  state = response.results[0].address_components[i].long_name;
                  break;
                case "country":
                  country = response.results[0].address_components[i].long_name;
                  break;
                default:
                  break;
                
              }
            }
          }
          console.log(city, state, country);
          console.log(address);
          setAddress(address);
        },
        (error) => {
          console.error(error);
        }
      );

     }



 const onClose = () => {
   dispatch(showAddressDrawer(false))
  };

  const onFinish = async(value) => {
    console.log(value)
    const formData = new FormData();
    formData.append('complete_address', value.complete_address)
    formData.append('house_flat_no', value.house_flat_no)
    formData.append('landmark', value.landmark)
    formData.append('name_of_address', value.name_of_address)
    formData.append('latitude', centerMap.lat)
    formData.append('longitude', centerMap.lng)
    formData.append('action', 'add')
    const res = await postData(`${REACT_APP_API_URL}/api/v1/outlet/add_address`, formData)
    try {
      console.log(res)
      if(res.data.status === 200){
        toast.success(`${t('address-addedSuccessfully')}`)
        dispatch(showAddressDrawer(false))
        window.location.reload()

      }else {
        toast.error(res.data.msg)
      }
      
    } catch (error) {
      console.log(error)
    }
  }



    return (
        <div>
            <Drawer
          title={<div className={props.locale == 'en' ? '': "text-left"}>{t('address-myAddresses')}</div>}
          placement={props.locale == 'en' ? 'left': 'right'}
          closable={true}
          onClose={onClose}
          visible={visible}
          key="left"
          className="responsive-drawer"
        >
          <div className="Drawer-header">
            
          </div>
          <Map 
 google={window.google} 
 zoom={15}
 initialCenter={centerMap}
 center={centerMap}

//  onClick = {changeMarkerPosition}
 containerStyle={{  
  position: "sticky",
 width: "335px",
 maxWidth: "100%",
 height: "300px"
}}



 >
   <Marker 
    position={centerMap}
    draggable={true}
    name={'Current location'}

    onDragend={(one, two, three) => dragComplete(one, two, three)}
   icon={{
      url: `${process.env.REACT_APP_PUBLIC_URL}/images/map-pin.svg`,
      anchor: new window.google.maps.Point(32.5,66),
      scaledSize: new window.google.maps.Size(64,64)
    
    }}>
     {/* <img src={`${process.env.REACT_APP_HOME_URL}/images/map.pin.svg`}></img> */}
</Marker>
   </Map>
          <Form 
              name="my-profile-form"
              className="login-form" 
              form={form}
              size="large"
              onFinish={onFinish}
            >
              <Form.Item
                        name="complete_address"
                        rules={[{
                            required: true,
                            message: `${t('address-errSearchYourAddress')}`
                        }
                    ]}>

          <PlacesAutocomplete
        value={address}
        onChange={value  => setAddress(value)}
        onSelect={handleSelect}
      >
        {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => (
          <div>
            {/* <label htmlFor="ant-input">Address</label> */}
      <Input
       value={address}
          type="text"
          {...getInputProps({
            
            placeholder: `${t('address-searchYourAddress')}`,
            className: 'location-search-input ant-input',
          })}

          varient="bottom-outline"
          />
            <div className="autocomplete-dropdown-container">
              {loading && <Spin>{t('common-Loading')}</Spin>}
              {suggestions.map(suggestion => {
                const className = suggestion.active
                  ? 'suggestion-item--active'
                  : 'suggestion-item';
                // inline style for demonstration purpose
                const style = suggestion.active
                  ? { backgroundColor: '#0000', cursor: 'pointer' }
                  : { backgroundColor: '#0000', cursor: 'pointer' };
                return (
                  <div
                    {...getSuggestionItemProps(suggestion, {
                      className,
                      style,
                    })}
                  >
                    <div>{suggestion.description}</div>
                  </div>
                );
              })}
            </div>
          </div>
        )}
      </PlacesAutocomplete>
      </Form.Item>
      <Form.Item
      name="house_flat_no"
      rules={[{
          required: true,
          message: `${t('address-errHouseFlat')}`
      }
      ]}>
      <Input
          name="house_flat_no"
          type="text"
          varient="bottom-outline"
          placeholder={`${t('address-houseFlat')}`}
          />
      </Form.Item>
      <Form.Item
        name="landmark"
        rules={[{
            required: true,
            message: `${t('address-errLandmark')}`
        }
    ]}>
        <Input
            name="landmark"
            type="text"
            varient="bottom-outline"
            placeholder={`${t('address-landmark')}`}/>
      </Form.Item>
      <Form.Item
        name="name_of_address"
        rules={[{
            required: true,
            message:  `${t('address-errNameOfAddress')}`
        }
    ]}>
        <Input
            name="name_of_address"
            type="text"
            varient="bottom-outline"
            placeholder={`${t('address-nameOfAddress')}`}/>
      </Form.Item>

      <Form.Item>
      <Button type="primary" htmlType="submit" className="login-form-button">
      {t('address-saveAddress')}
      </Button>
      </Form.Item>
    </Form>
  </Drawer>
        </div>
    );
};

const mapStateToProps = state => ({
  locale: state.i18n.locale
});


const mapDispatchToProps = {};


export default connect(mapStateToProps, mapDispatchToProps)(AddressManagment);
