import React from "react";
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavItem,
  NavLink,
  Nav
} from 'reactstrap';
import PropTypes from "prop-types";
import {showAddressListDrawer, showLoginModal, showBranchListModal} from '../../redux/actions/toggleAction'
import {getCartItems} from '../../redux/actions/cart'
import {setActiveBranch} from '../../redux/actions/ActiveBranch'
import {Container} from 'react-bootstrap'
import {Link, withRouter} from 'react-router-dom'
import { connect } from "react-redux";
import LoginModal from '../../containers/login'
import CartIcon from './CartIcon'
import ResetPasswordModal from '../../containers/ResetPassword'
import AddressListDrawer from '../../containers/address/addressListsDrawer'
import AddressDrawer from '../../containers/address/addressManagement'
import BranchListModal from '../../containers/BranchManagement'
import BranchViewModal from '../../containers/branch/branchView'
// import {useDispatch} from 'react-redux'
import LanguageSwitcher from "../UI/LanguageSwitcher";
import { Badge, Drawer } from 'antd';
import {withTranslation} from 'react-i18next'

import "./AppBar.css";

const {REACT_APP_PUBLIC_URL} = process.env;
const defautAddress = localStorage.getItem("default_address") ? JSON.parse(localStorage.getItem("default_address")) : null

class AppBar extends React.Component {  
  state = {
    isActive: false,
    scrolling:false,
    isOpen:false,
    CartItemCount:0,
    visible:false,
    token: localStorage.getItem('token') ? localStorage.getItem('token') : '',
    user: localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')): {
      "first_name": "",
      "last_name": ""
    }
  };

  showDrawer = () => {
      this.setState({...this.state, visible:true});
    };

  onClose = () => {
    this.setState({...this.state, visible:false});
    };

 componentDidMount() {
      window.addEventListener('scroll', this.handleScroll);
  }
  
  componentWillUnmount() {
      window.removeEventListener('scroll', this.handleScroll);
  }
  
  handleScroll = (event) => {
    if (window.scrollY === 0 && this.state.scrolling === true) {
      this.setState(prevState => ({ scrolling: false}));
    }
    else if (window.scrollY !== 0) {
      this.setState(prevState => ({ scrolling: true}));
    }
}



  toggleIsActive = () => {
    this.setState(prevState => ({ isActive: !prevState.isActive }));
  };

OpenLoginModal = () => {
    this.props.showLoginModal(true)
}
 OpenAddress = () => {
  this.props.showAddressListDrawer(true)
}
OpenBranch = () => {
  this.props.showBranchListModal(true)
  this.forceUpdate();
}
  toggle = () => this.setState({...this.state, isOpen: !this.state.isOpen});


  render() {
    const { t } = this.props;
    const active_branch = localStorage.getItem('Active_Branch') ? JSON.parse(localStorage.getItem('Active_Branch')) : ''
  
    return (
      <>
     <header className={this.state.scrolling ? `header header-bg fixed-top d-none d-md-block`: `header transparent fixed-top d-none d-md-block`}>
         <Container fluid={true}>
            <Navbar dark expand="md"  style={{width:"100%", padding:"0px 0px"}}>
              <Link className="navbar-brand" to="/">
            <img className="logo" src={REACT_APP_PUBLIC_URL + '/images/Logo_White.png'} alt=""/>
            </Link>
              <NavbarToggler onClick={this.toggle} />
              <Collapse isOpen={this.state.isOpen} navbar>
                <Nav className={this.props.locale === 'en' ? `mr-auto` : `ml-auto`} navbar>
                    <NavItem onClick={this.state.token ? () => this.OpenAddress() : () => this.OpenLoginModal()} className="header-location-dropdown">
                        <img src={REACT_APP_PUBLIC_URL + `/images/header/location-pin.svg`} alt="location icon" />
                       { defautAddress !== null ? <p>{defautAddress.complete_address}</p> : <p>
                         {t('header-selectAddress')}
                         </p>} 
                        <img src={REACT_APP_PUBLIC_URL + '/images/header/down-icon.svg'} alt="drop down" />
                    </NavItem>
                    <NavItem onClick={() => this.OpenBranch()} className="header-location-dropdown">
                      <p>{active_branch !== '' ? active_branch.outlet_name[this.props.locale] : `${t('header-selectBranch')}`}</p>
                        <img src={REACT_APP_PUBLIC_URL + '/images/header/down-icon.svg'} alt="drop down" />
                    </NavItem>
                </Nav>
                <NavLink className="header-link" href="/coupon"><p>{t('header-coupon')}</p></NavLink>
                <LanguageSwitcher />
                  <div className="ml-2 mr-2">
                  <Badge
                  className="site-badge-count-109 text-white"
                  count={5}
                  style={{ backgroundColor: '#52c41a' }}>
                      <img style={{opacity:0.65, margin:"0px 5px"}} src={REACT_APP_PUBLIC_URL + '/images/header/notification-icon.svg'} alt="notification" />
                  </Badge>
                 </div>
                  <div className="ml-2 mr-2">
                <Link to="/checkout">
                 <CartIcon props={this.props}/>
                </Link>
                </div>
                  {this.state.token ? <Link to="/myprofile"><div className="ml-2 mr-2">

                  <img  style={{opacity:0.65, margin:"0px 5px"}} src={REACT_APP_PUBLIC_URL + '/images/header/user-account.svg'} alt="user icon" />

                  </div></Link> :  <div style={{cursor:"pointer"}} onClick={() => this.OpenLoginModal()} className="ml-2 mr-2">
                  <img  style={{opacity:0.65, margin:"0px 5px"}} src={REACT_APP_PUBLIC_URL + '/images/header/user-account.svg'} alt="user icon" />
                  </div>}
            </Collapse>
            </Navbar>
      </Container>  
    </header>

    {/* top bar */}
    <section className="mobile-app-topbar d-flex justify-content-around d-md-none">
      <div onClick={this.state.token ? () => this.OpenAddress() : () => this.OpenLoginModal()} className="header-location-dropdown">
      <img src={REACT_APP_PUBLIC_URL + `/images/header/location-pin.svg`} alt="location icon" />
      { defautAddress !== null ? <p>{defautAddress.complete_address}</p> : <p>
      {t('header-selectAddress')}
      </p>} 
      <img src={REACT_APP_PUBLIC_URL + '/images/header/down-icon.svg'} alt="drop down" />
      </div>
      <div onClick={() => this.OpenBranch()} className="header-location-dropdown">
        <p>{active_branch !== '' ? active_branch.outlet_name[this.props.locale] : `${t('header-selectBranch')}`}</p>
        <img src={REACT_APP_PUBLIC_URL + '/images/header/down-icon.svg'} alt="drop down" />
      </div>
    </section>
    <section className="mobile-app-bar d-flex d-md-none">
      <div className={this.props.location.pathname === '/' ? 'active appbar-icon' : 'appbar-icon'}>
          <Link to="/">
          <img src={`${REACT_APP_PUBLIC_URL} ${this.props.location.pathname === '/' ?  '/images/appbar/active/logo_icon.png' : '/images/appbar/logo_icon.png'}`} alt="homePage"></img>
          </Link>
      </div>
      <div className={this.props.location.pathname === '/coupon' ? 'active appbar-icon' : ' appbar-icon'}>
          <Link to="/coupon">
          <img src={`${REACT_APP_PUBLIC_URL} ${this.props.location.pathname === '/coupon' ? '/images/appbar/active/discount.png' : '/images/appbar/discount.png'}`} alt="coupon"></img>
          </Link>
      </div>
      <div className={this.props.location.pathname === '/checkout' ? 'active appbar-icon' : ' appbar-icon'}>
          <Link to="/checkout">
          <img src={`${REACT_APP_PUBLIC_URL} ${this.props.location.pathname === '/checkout' ? '/images/appbar/active/cart-icon.png' : '/images/appbar/cart-icon.png'}`} alt="cart"></img>
          </Link>
      </div>
      <div className={this.props.location.pathname === '/notification' ? 'active appbar-icon' : ' appbar-icon'}>
          <Link to="/notification">
          <img src={`${REACT_APP_PUBLIC_URL} ${this.props.location.pathname === '/notification' ? '/images/appbar/active/notification-icon.png' : '/images/appbar/notification-icon.png'}`} alt="notification"></img>
          </Link>
      </div>
      <div className={this.props.location.pathname === '/myprofile' ? 'active appbar-icon' : ' appbar-icon'}>
          <div onClick={this.state.token ? () => this.showDrawer() : () => this.OpenLoginModal()}>
          <img src={`${REACT_APP_PUBLIC_URL} ${this.props.location.pathname === '/myprofile' ? '/images/appbar/active/profile_icon.png' : '/images/appbar/profile_icon.png'}`} alt="profile"></img>
          </div>
      </div>
    </section>
    <Drawer 
    title={this.state.user.first_name + ' ' + this.state.user.last_name} 
    placement="right" 
    onClose={() => this.onClose()} visible={this.state.visible}
    footer={
      <div className="acc-menu-item">
      <img src={REACT_APP_PUBLIC_URL + '/images/myAccount/logout.svg'} alt="my profile icon" />
      Logout
    </div>
    }
    >
       <div className="account-list-mobile">
          <div className="acc-menu-item">
            <Link to="/m.myprofile" onClick={() => this.onClose()}>
            <img src={REACT_APP_PUBLIC_URL + '/images/myAccount/myProfile.svg'} alt="my profile icon" />
            My Profile
            </Link>
          </div>
          <div className="acc-menu-item">
            <Link to="/m.order-history" onClick={() => this.onClose()}>
            <img src={REACT_APP_PUBLIC_URL + '/images/myAccount/orderHistory.svg'} alt="my profile icon" />
            Order History
            </Link>
          </div>
          <div className="acc-menu-item">
            <Link to="/m.manage-addresses" onClick={() => this.onClose()}>
            <img src={REACT_APP_PUBLIC_URL + '/images/myAccount/manageAddresses.svg'} alt="my profile icon" />
            Manage Addresses
            </Link>
          </div>
          <div className="acc-menu-item">
            <Link to="/m.loyalty-points" onClick={() => this.onClose()}>
            <img src={REACT_APP_PUBLIC_URL + '/images/myAccount/loyaltyPoints.svg'} alt="my profile icon" />
            Loyalty Points
            </Link>
          </div>
          <div className="acc-menu-item">
            <Link to="/m.feedback" onClick={() => this.onClose()}>
            <img src={REACT_APP_PUBLIC_URL + '/images/myAccount/feedback.svg'} alt="my profile icon" />
            Feedback
            </Link>
          </div>
          <div className="acc-menu-item">
            <Link to="/m.about-us" onClick={() => this.onClose()}>
            <img src={REACT_APP_PUBLIC_URL + '/images/myAccount/aboutUs.svg'} alt="my profile icon" />
            About us
            </Link>
          </div>
          <div className="acc-menu-item">
            <Link to="/m.contact-us" onClick={() => this.onClose()}>
            <img src={REACT_APP_PUBLIC_URL + '/images/myAccount/contactUs.svg'} alt="my profile icon" />
            Contact us
            </Link>
          </div>
          <div className="acc-menu-item">
            <Link to="/m.language" onClick={() => this.onClose()}>
            <img src={REACT_APP_PUBLIC_URL + '/images/myAccount/language.svg'} alt="my profile icon" />
            Language
            </Link>
          </div>
       </div>
       </Drawer>
  {this.props.showAddressListDrawer && <AddressListDrawer />}  
  {this.props.showLoginModal && <LoginModal />} 
    <BranchListModal />
    <BranchViewModal />
    <AddressDrawer />
    <ResetPasswordModal />
    </>
    );
  }
}


AppBar.propTypes = {
  showLoginModal: PropTypes.func.isRequired,
  locale: PropTypes.string.isRequired,
  showAddressListDrawer: PropTypes.func.isRequired,
  showBranchListModal: PropTypes.func.isRequired,
  setActiveBranch:PropTypes.func.isRequired,
  DefaultAddress:PropTypes.object.isRequired,
  getCartItems:PropTypes.func
  

};

const mapStateToProps = state => ({
  showLoginModal: state.toggleStack.showLoginModal,
  showAddressListDrawer: state.toggleStack.showAddressListDrawer,
  showAddressDrawer:state.toggleStack.AddressDrawer,
  showBranchLists:state.toggleStack.showBranchLists,
  selectedBranch:state.branch.selectedBranch,
  DefaultAddress:state.DefaultAddress.default_address,
  locale: state.i18n.locale
});

const mapDispatchToProps = { showLoginModal, showAddressListDrawer, showBranchListModal, setActiveBranch, getCartItems};

export default connect(mapStateToProps, mapDispatchToProps)(withTranslation()(withRouter(AppBar)));

