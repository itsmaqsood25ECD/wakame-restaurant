import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import FeedbackManagement from '../../containers/feedback/index'

export const Feedback = (props) => {
  return <div className='container'>
    <FeedbackManagement />
  </div>;
};

Feedback.propTypes = {
};

const mapStateToProps = (state) => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(Feedback);
