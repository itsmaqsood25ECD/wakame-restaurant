import * as types from './types'
import {showLoginModal, showBranchListViewModal, showAddressDrawer} from './toggleAction'

export const isLoggedin = (data) => async dispatch => {
    
    dispatch({
        type:types.AUTH_LOGIN,
        payload:data
    })

}