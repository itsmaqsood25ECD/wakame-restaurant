import * as types from './types'

export function setCategory(data) {

    return {
        type:types.SET_CATEGORY,
        payload:data
    }
}