import React, {useEffect, useState, useRef} from 'react';
import CartItems from '../containers/checkout/cartItems'
import CheckoutProducts from '../containers/checkout/checkoutProducts'
import {Row, Col, Button, DatePicker,TimePicker, Collapse, Tabs} from 'antd'
import { toast } from "react-toastify";
import propTypes from 'prop-types';
import { connect } from 'react-redux';
import {useDispatch, useSelector} from 'react-redux'
import {showLoginModal,showBranchListModal, showAddressListDrawer} from '../redux/actions/toggleAction'
import {
    ClockCircleOutlined,
    WalletOutlined
} from '@ant-design/icons'
import moment from 'moment'
import { useHistory } from "react-router-dom";
import {useTranslation} from 'react-i18next'

const { Panel } = Collapse;
const { TabPane } = Tabs;

const Checkout = (props) => {
    const dispatch = useDispatch();
    const {t} = useTranslation();
    const [BookingDate, setBookingDate] = useState('')
    const [BookingTime, setBookingTime] = useState('')
    const [activeBranch, setActiveBranch] = useState(localStorage.getItem('Active_Branch') ? JSON.parse(localStorage.getItem('Active_Branch')) : '' )

    const isLogged = localStorage.getItem('token') ?  localStorage.getItem('token') : '';
    const isBranchSelected = localStorage.getItem('Active_Branch') ? localStorage.getItem('Active_Branch') : '';
    const isDefaultAddressSelected = localStorage.getItem('default_address') ? localStorage.getItem('default_address') : '';

    let history = useHistory();

    const checkoutRef = useRef();
    const onCheckout = (BookingDate, BookingTime, paymentMethod) => {
       if(BookingDate == undefined || BookingDate == '' || BookingDate == null){
        toast.error("please Select Date");
       }else if(BookingTime == undefined || BookingTime == '' || BookingTime == null){
        toast.error("please Select Time");
       }else if(checkoutRef.current) {
        checkoutRef.current.checkoutProduct(BookingDate, BookingTime, paymentMethod);
        }
    };

   const handleCheckout = (BookingDate, BookingTime, paymentMethod) => {
        console.log(isLogged, isBranchSelected, isDefaultAddressSelected)
        if(isLogged == ''){
            dispatch(showLoginModal(true))
        }else if(isBranchSelected == ''){
            dispatch(showBranchListModal(true))
        }else if(isDefaultAddressSelected == ''){
            dispatch(showAddressListDrawer(true))
        }else{
            onCheckout(BookingDate, BookingTime, paymentMethod)
        }
    }

     useEffect(() => {
        if(activeBranch !== ''){
            if(activeBranch.outlet_busy === "1"){
                toast.error("current outlet is busy at the moment! please try after sometime or try other branch")
                history.push("/");
            }
        }
         
     }, [activeBranch])
    
          function callback(key) {
            console.log(key);
          }
    
          function disabledDate(current) {
            // Can not select days before today and today
            return current && current <= moment().startOf('day');
          }
    
        const DeliveryDateTimeHeader = (<>
             <p className="text-md-1 text-white d-flex align-items-center"><ClockCircleOutlined /> <span className="ml-1 mr-1">{t('chooseDateTime')}</span></p>  
        </>)
    
          const choosePaymentMethodHeader = (<>
            <p className="text-md-1 text-white d-flex align-items-center"><WalletOutlined /> <span className="ml-1 mr-1">{t('choosePaymentMethod')}</span></p>  
       </>)

    return (
         <>
         <section className="container mt-5">
             <Row gutter={16}>
                 <Col xs={{ order: 2, span:24 }} lg={{order: 1, span:18}} md={18}>
                    <div className="m-md-4 d-md-block d-none">
                        <Collapse
                            defaultActiveKey={['1']}
                            onChange={callback}
                            accordion
                            className="checkout-collapse"
                            expandIconPosition={props.locale == 'en' ? 'right' : 'left'}
                            expandIcon={({ isActive }) => isActive ? "" : <p className="text-md font-weight-bold text-primary">{t('change')}</p>}
                            >
                            <Panel header={DeliveryDateTimeHeader} key="1">
                            <div className="mb-2 d-flex">
                                    <DatePicker
                                    placeholder={t('selectDate')}
                                    disabledDate={disabledDate}
                                    className="table-booking-picker ml-4 mr-2" onChange={value => setBookingDate(value)} />
                                    <TimePicker placeholder={t('selectTime')} format="HH:mm" className="table-booking-picker ml-2 mr-2" type="time" onChange={value => setBookingTime(value)} />
                                    </div>
                            </Panel>
                            <Panel header={choosePaymentMethodHeader} key="2">
                            <Tabs className="myprofile-tabs payment-gateway-tab" tabPosition={props.locale == 'en' ? 'left' : 'right'} defaultActiveKey="1" onChange={callback}>
                                            
                                            <TabPane tab={t('debitCard')} key="1">
                                              <p className="mt-2">{t('comingsoon')}</p>
                                            </TabPane>
                                            <TabPane tab={t('creditCard')} key="2">
                                            <p className="mt-2">{t('comingsoon')}</p>
                                            
                                            {/* <p className="text-white text-md">Pay using credit Card</p>
                                                <Button onClick={() => handleCheckout(BookingDate, BookingTime, 'Credit Card')} type="primary">PLACE ORDER</Button>
                                             */}
                                            </TabPane>
                                            <TabPane tab={t('payByCash')} key="4">
                                                <p className="mt-2 text-white text-md">{t('placeCash')}</p>
                                                <Button onClick={() => handleCheckout(BookingDate, BookingTime, 'COD')} type="primary">PLACE ORDER</Button>
                                            </TabPane>
                                        </Tabs>
                            </Panel>
                            </Collapse>
                    </div>
                    <div className="d-block d-md-none">
                    {DeliveryDateTimeHeader}
                        <div className="mb-2 d-flex">
                            <DatePicker
                            placeholder={t('selectDate')}
                            disabledDate={disabledDate}
                            className="table-booking-picker ml-4 mr-2" onChange={value => setBookingDate(value)} />
                            <TimePicker placeholder={t('selectTime')} format="HH:mm" className="table-booking-picker ml-2 mr-2" type="time" onChange={value => setBookingTime(value)} />
                        </div>
                        <Collapse
                       
                            onChange={callback}
                            accordion
                            className="checkout-collapse"
                            expandIconPosition={props.locale == 'en' ? 'right' : 'left'}
                            expandIcon={({ isActive }) => isActive ? "" : <p className="text-md font-weight-bold text-primary">choose payment</p>}
                            >
                            <Panel header={t('debitCard')} key={t('debitCard')}>
                            <p className="mt-2">{t('comingsoon')}</p>
                            </Panel>
                            <Panel header={t('creditCard')} key={t('creditCard')}>
                            <p className="mt-2">{t('comingsoon')}</p>
                            </Panel>
                            <Panel header={t('payByCash')} key={t('payByCash')}>
                            <p className="mt-2 text-white text-md">{t('placeCash')}</p>
                                                <Button onClick={() => handleCheckout(BookingDate, BookingTime, 'COD')} type="primary">PLACE ORDER</Button>
                                           
                            </Panel>
                            </Collapse>
                    </div>
                 </Col>
                 <Col xs={{ order: 1, span:24 }} lg={{order: 2, span:6}} md={6}>
                     <CartItems ref={checkoutRef} />
                 </Col>
             </Row>

         </section>
        </>
    );
};



Checkout.propTypes = {
    locale: propTypes.string.isRequired,
};

const mapStateToProps = state => ({
    locale: state.i18n.locale
  });
  
  const mapDispatchToProps = { };
  
export default connect(mapStateToProps, mapDispatchToProps)(Checkout);
