import * as types from '../actions/types'

const initialState = {
    loading:true,
    error:null,
    productList:  []
}

export const getProductList = (state = initialState, action) => {
    switch(action.type) {
        case types.SET_PRODUCT_LIST:
            return {
                ...state,
                productList:action.payload,
                loading:false,
                error:null
            }
        default: 
        return state
    }
}

export const updateProductList = (state = initialState, action) => {

      switch(action.type) {
        case types.UPDATE_PRODUCT_LIST:
            return {
                ...state,
                productList:action.payload,
                loading:false,
                error:null
            }
        default: 
        return state
    }
}
