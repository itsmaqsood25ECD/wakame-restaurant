import React,{useState, useEffect} from 'react'
import {DatePicker,TimePicker, Collapse, Tabs, Button} from 'antd'
import propTypes from 'prop-types';
import { connect } from 'react-redux';
import moment from 'moment'
import {
    ClockCircleOutlined,
    WalletOutlined
} from '@ant-design/icons'

const { Panel } = Collapse;
const { TabPane } = Tabs;

function CheckoutProducts(props) {

  const [BookingDate, setBookingDate] = useState('')
  const [BookingTime, setBookingTime] = useState('')

  useEffect(() => {
   console.log("props.locale", props.locale)
  }, [])
      function callback(key) {
        console.log(key);
      }

      function disabledDate(current) {
        // Can not select days before today and today
        return current && current <= moment().startOf('day');
      }

    const DeliveryDateTimeHeader = (<>
         <p className="text-md-1 text-white d-flex align-items-center"><ClockCircleOutlined /> <span className="ml-1 mr-1">Choose Date And Time</span></p>  
    </>)

      const choosePaymentMethodHeader = (<>
        <p className="text-md-1 text-white d-flex align-items-center"><WalletOutlined /> <span className="ml-1 mr-1">Choose Payment Method</span></p>  
   </>)

    return (
        <div className="m-md-4">
       <Collapse
          defaultActiveKey={['1']}
          onChange={callback}
          accordion
          className="checkout-collapse"
          expandIconPosition={props.locale == 'en' ? 'left' : 'right'}
          expandIcon={({ isActive }) => isActive ? "" : <p className="text-md font-weight-bold text-primary">Change</p>}
        >
          <Panel header={DeliveryDateTimeHeader} key="1">
          <div className="mb-2 d-flex">
                <DatePicker
                 disabledDate={disabledDate}
                 className="table-booking-picker ml-4 mr-2" onChange={value => setBookingDate(value)} />
                <TimePicker className="table-booking-picker ml-2 mr-2" type="time" onChange={value => setBookingTime(value)} />
                </div>
          </Panel>
          <Panel header={choosePaymentMethodHeader} key="2">
          <Tabs className="myprofile-tabs payment-gateway-tab" 
          tabPosition="top" 
          defaultActiveKey="1" 
          onChange={callback}
          >
            <TabPane tab="Dabit Card (Mada)" key="1">
              Coming Soon
            </TabPane>
            <TabPane tab="Credit Card" key="2">
              Coming Soon
            </TabPane>
            <TabPane tab="Pay By Cash" key="3">
              <p className="text-white text-lg">Pay by cash once the order is delivered at your door.</p>
              <Button className='checkout-btn' type="primary">PLACE ORDER</Button>
            </TabPane>
          </Tabs>
          </Panel>
        </Collapse>
        </div>
    )
}

CheckoutProducts.propTypes = {
  locale: propTypes.string.isRequired,
}

const mapStateToProps = state => ({
  locale: state.i18n.locale
});

const mapDispatchToProps = { };

export default connect(mapStateToProps, mapDispatchToProps)(CheckoutProducts);


