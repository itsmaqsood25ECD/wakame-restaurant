import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import MyAddressList from '../../containers/address/addressLists'

export const ManageAddress = (props) => {
  return <div className='container'>
    <MyAddressList />
  </div>;
};

ManageAddress.propTypes = {

};

const mapStateToProps = (state) => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(ManageAddress);
