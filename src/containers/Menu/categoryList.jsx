import React,{useState, useEffect} from 'react';
import propTypes from 'prop-types';
import { connect } from 'react-redux';
import {postData} from '../../services/api'
import {setCategory} from '../../redux/actions/category'
import '../styles/categoryList.css'
import {Spin} from 'antd'
import { useTranslation } from 'react-i18next';

const Categories = (props) => {
    const {REACT_APP_API_URL} = process.env
    const {t} = useTranslation()
    let storedBranch = localStorage.getItem("Active_Branch");
    let branches = storedBranch ? JSON.parse(storedBranch): '';
    const [category, setCategoryList] = useState([])
    const [selectedCategory, setSelectedCategory] = useState('')
    const [load, setLoad] = useState(false)

    useEffect(() => {
        getCategory()
    }, [props.selectedBranch])

    async function getCategory () {
        setLoad(true)
        const formData = new FormData();
        formData.append('outlet_id', branches.outlet_id);
        const res = await postData(`${REACT_APP_API_URL}/api/v1/outlet/getcategories`, formData)
        try {
            setLoad(false)
            console.log(res)
            if(res.data.msg === 'Operation Successful') {
               if(res.data.data.length > 0 ){
                  const activeCat =  res.data.data.filter((cat) => {
                      return cat.status == 1
                  }) 
                  console.log(activeCat)
                  activeCat.length > 0 && setCategoryList(activeCat)
                  activeCat.length > 0 && setSelectedCategory(activeCat[0].cat_id) 
                  activeCat.length > 0 && props.setCategory(activeCat[0])
               }
             
              
            }else{
                setCategoryList([])
            }

        } catch (error) {
            setLoad(false)
        }
    }

    const handleCategorySelect = async (cat) => {
        setSelectedCategory(cat.cat_id)
        props.setCategory(cat)
    }

    return (
        <div>
           {load ? <div className="page-loader"> <Spin /> </div>: <div>
               <ul className="category-list">
                   {category && category.length > 0 ? category.map(cat => {
                       return <> {cat.status != 0 && <li onClick={() => handleCategorySelect(cat)} className={`category-item ${props.locale == 'en' ? 'active-right' : 'active-left'}  ${selectedCategory === cat.cat_id ? 'active' : ''}`}>{cat.cat_name[props.locale]} <span className="cat-product-count">({cat.total_product})</span></li>}</>
                   }) : <> {category && category.length === 0 && <div>
                       <p>{t('common-nocatfound')}</p>
                       </div>} </>}
                  </ul>
               </div>} 
        </div>
    );
};


Categories.propTypes = {
    setCategory: propTypes.func.isRequired,
    locale: propTypes.string.isRequired,
};

const mapStateToProps = state => ({
    selectedBranch: state.branch.selectedBranch,
    category:state.category.category,
    locale: state.i18n.locale
  });


  const mapDispatchToProps = { setCategory};


export default connect(mapStateToProps, mapDispatchToProps)(Categories);
