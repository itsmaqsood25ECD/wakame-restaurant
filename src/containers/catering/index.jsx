import React, {useState} from 'react';
import BranchDetails from '../Menu/MenuBranchDetails'
import CategoryList from '../Menu/categoryList'
import ProductList from './menuProducts'
import CartItems from '../checkout/cartItems'
import Cart from '../checkout/cart'
import {Row, Col, Button} from 'antd'
import {Link} from 'react-router-dom'
import CheckoutProducts from '../checkout/checkoutProducts';
import {toast, ToastContainer} from 'react-toastify'
import { useHistory } from "react-router-dom";

const MainMenu = () => {
    let history = useHistory();
    const [activeBranch, setActiveBranch] = useState(localStorage.getItem('Active_Branch') ? JSON.parse(localStorage.getItem('Active_Branch')) : '' )

  const  handleCheckoutLocation = (branch) => {
      console.log("activeBranch", activeBranch)
      console.log("branch", branch)
        if(activeBranch !== ''){
            if(activeBranch.outlet_busy === "1"){
                toast.warning("Outlet is busy at the moment! please try after sometime ")
            }else{
                history.push("/checkout");
            }
        }
    }

    return (
         <>
         <BranchDetails />
         <section className="container">
             <Row gutter={16}>
                 <Col xs={24} lg={4} md={6} style={{borderRight:"1px solid #aaa", padding:"0px"}}>
                  <CategoryList  />
                 </Col>
                 <Col xs={24} lg={14} md={12}>
                 <ProductList />
                 </Col>
                 <Col xs={24} lg={6} md={6}>
                     <Cart />
                     
                     <Button onClick={() => handleCheckoutLocation(activeBranch)} className="mb-5 mt-5" type="primary" block size="large">CHECKOUT</Button>
                    
                 </Col>
             </Row>
             
         </section>
          
        </>
    );
};





export default MainMenu;
