import * as types from './types'

export function addToCart(data) {

    return {
        type:types.ADD_TO_CART,
        payload:data
    }
}

export function getCartItems(){
    return {
        type:types.GET_CART_ITEMS
    }
}



export const clearCart = ( product) => {
    return {
      type: types.CLEAR_CART,
      payload:product
    }
  }