import React from "react";
import { connect } from "react-redux";
import { PropTypes } from "prop-types";
import {
 DropdownMenu,
  DropdownItem,
  UncontrolledDropdown,
  DropdownToggle,
} from 'reactstrap';

import { supportedLocales } from "../../config/i18n";
import { setLocaleWithFallback } from "../../redux/actions/i18n";
import {withTranslation} from 'react-i18next'

class LanguageSwitcher extends React.Component {
  handleLanguageLinkClick = (e, code) => {
    e.preventDefault();
    this.props.setLocaleWithFallback(code);
  };

  render() {
    const {t} = this.props
    return (
<UncontrolledDropdown  inNavbar>
<DropdownToggle className="Language-dropdown" nav caret>
<span>{t('header-language')}</span>
</DropdownToggle>
<DropdownMenu  right>
          {Object.keys(supportedLocales).map(code => (
            <DropdownItem
              href="#"
              key={code}
              active={code === this.props.locale}
              onClick={e => this.handleLanguageLinkClick(e, code)}
            >
              {supportedLocales[code]}
            </DropdownItem>
          ))}
        </DropdownMenu>
</UncontrolledDropdown>
  
    );
  }
}

LanguageSwitcher.propTypes = {
  locale: PropTypes.string.isRequired,
  setLocaleWithFallback: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({ locale: state.i18n.locale });

const mapDispatchToProps = { setLocaleWithFallback };

export default connect(mapStateToProps, mapDispatchToProps)(withTranslation()(LanguageSwitcher));