import {store} from "./store";

export default function getCurrentLocale() {
    const state = store.getState();
      return state.i18n.locale;
}