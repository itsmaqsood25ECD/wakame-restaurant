import * as types from '../actions/types'

const initialState = {
    loading:true,
    error:null,
    selectedBranch:localStorage.getItem('Active_Branch') ?  localStorage.getItem('Active_Branch') : {}
}

export const ActiveBranchReducer = (state = initialState, action) => {
    switch(action.type) {
        case types.SET_ACTIVE_BRANCH:
            return {
                ...state,
                selectedBranch:action.payload,
                loading:false,
                error:null
            }
        default: 
        return state
    }
}