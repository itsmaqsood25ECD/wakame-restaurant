import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import ContactUsManagement from '../../containers/contact-us/index'

export const ContactUs = (props) => {
  return <div className='container'>
    <ContactUsManagement />
  </div>;
};

ContactUs.propTypes = {
};

const mapStateToProps = (state) => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(ContactUs);
