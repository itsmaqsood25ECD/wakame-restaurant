import React,{ Fragment ,useState, useEffect } from "react";
import {useDispatch, useSelector} from 'react-redux'
import {showLoginModal, showSignupModal} from '../redux/actions/toggleAction'
import { Form, Input, Button, Checkbox, Modal, Radio, Spin } from 'antd';
import './styles/login.css'
import {  toast } from 'react-toastify';
import axios from "axios";
import {connect} from 'react-redux'
import { propTypes } from "prop-types";
import {useTranslation} from 'react-i18next'

const {REACT_APP_PUBLIC_URL, REACT_APP_API_URL} = process.env

const SignupManagement = (props) => {
  const {t} = useTranslation();
  const dispatch = useDispatch();
    const [visible, setVisible] = useState()
    const [loading, setLoading] = useState(false)
    const [firstname, setFirstName] = useState('')
    const [lastname, setLastName] = useState('')
    const [phone, setPhone] = useState('')
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [confirm_password, setConfirmPassword] = useState('')
    const [agreed, setAgreed] = useState(false)
    
    const state_visible = useSelector( state => state.toggleStack.showSignupModal)

     useEffect(() => {
        
        setVisible(state_visible);


     }, [state_visible]);


 const onClose = () => {
   dispatch(showLoginModal(false))
   dispatch(showSignupModal(false))
  };

  const handleShowLogin = () => {
    dispatch(showSignupModal(false))
    dispatch(showLoginModal(true))
  }

  const onFinish = (values) => {
    console.log('Received values of form: ', values);
    Validate(values)
  
  };

  const Validate = (values) => {
    var matchingCase = /^\+?([0-9]{2})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{3})$/;
    var passwordRegExp = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,16}$/;

    if(!(values.mobile.match(matchingCase))){
    toast.error(`${t('err-mobileNumber')}`)
    }else if(!(values.password.match(passwordRegExp))){
      toast.error(`${t('err-passwordValidationRegx')}`)
  }else if(values.password !== values.confirmPassword){
    toast.error(`${t('err-passwordValidationSame')}`)
    }else if(values.prefferdLanguage === undefined || values.prefferdLanguage === null || values.prefferdLanguage === ''){
      toast.error(`${t('err-selectLanguage')}`)
    }else if(!agreed){
      toast.error(`${t('err-termsCondition')}`)
    }else {
      SignUP(values)
    }


  }

  const SignUP = (values) => {
    // setLoading(true)
    const formData = new FormData();
    formData.append('first_name', values.firstname);
    formData.append('last_name', values.lastname);
    formData.append('phone', values.mobile);
    formData.append('email', values.email);
    formData.append('password', values.password);
    formData.append('confrim_password', values.confirmPassword);
    formData.append('language', values.prefferdLanguage);
   
    axios.post(`${REACT_APP_API_URL}/api/v1/outlet/signup`, formData)
    .then(async(res) => {
      console.log(res)
      setLoading(false)
      if(res.data.msg === 'Operation Successful'){
       
        console.log("res.data.token", res.data.token)
        localStorage.setItem('userID', res.data.data)
        localStorage.setItem('token', JSON.stringify(res.data.token))
        
        toast.success("Signup Successfully")
        setFirstName('')
        setLastName('')
        setPhone('')
        setEmail('')
        setPassword('')
        setConfirmPassword('')
        setAgreed(false)
        dispatch(showSignupModal(false))
        
      } else {
        toast.error(res.data.msg)
      }
      

    }).catch (function (error) {
        console.log('Request failed', error);
    });

  }


    return (
        <Fragment>
 
          <Modal width={400} title="" footer={null} visible={visible}  onCancel={onClose}>
          <Spin spinning={loading}  tip="Please Wait..." size="large" >
          
   
              <div className="center">
                  <h2>{t('signUp')}</h2>
                  <p>{t('alreadyHaveAnAccount')} <Button type="link" onClick={handleShowLogin}>{t('login')}</Button> {t('fromHere')} </p>
              </div>
              <br />
              <Form
                name="normal_signup"
                className="login-form"
                initialValues={{
                 agreedtac : true,
                 prefferdLanguage:'EN',
                 firstname:firstname,
                 lastname:lastname,
                 mobile:phone,
                 password:password,
                 confirmPassword:confirm_password,
                 email:email
                }}
                size="large"
                onFinish={onFinish}
              >

                <Form.Item
                  name="firstname"
                  rules={[
                    {
                      required: true,
                      message: `${t('err-firstName')}`,
                    },
                  ]}
                >
                  <Input varient="bottom-outline"  placeholder={t('firstName')} />
                </Form.Item>
                <Form.Item
                  name="lastname"
                  rules={[
                    {
                      required: true,
                      message:`${t('err-lastName')}`,
                    },
                  ]}
                >
                  <Input
                    
                    type="text"
                    placeholder={t('lastName')}
                    varient="bottom-outline"
                  />
                </Form.Item>
                <Form.Item
                  name="mobile"
                  rules={[
                    {
                      required: true,
                      message: 'Please enter your mobile number!',
                    },
                  ]}
                >
                  <Input
                    maxlength='9'
                    type="number"
                    addonBefore="+966"
                    min="0"
                    max="999999999"
                    placeholder={t('mobileNumber')}
                    varient="bottom-outline"
                  />
                </Form.Item>
                <Form.Item
                  name="email"
                  rules={[
                    {
                      required: true,
                      message: 'Please enter your email id!',
                    },
                  ]}
                >
                  <Input
                    
                    type="email"
                    placeholder={t('emailId')}
                    varient="bottom-outline"
                  />
                </Form.Item>
                <Form.Item
                name="password"
                rules={[
                {
                required: true,
                message: 'Please enter your password!',
                },
                ]}
                >
                <Input.Password

                type="password"
                placeholder={t('password')}
                varient="bottom-outline"
                />
                </Form.Item>
                <Form.Item
                name="confirmPassword"
                rules={[
                {
                required: true,
                message: 'Please enter your password!',
                },
                ]}
                >
                <Input.Password

                type="password"
                placeholder={t('confirmPassword')}
                varient="bottom-outline"
                />
                </Form.Item>
                <Form.Item name="prefferdLanguage"  label="">
                  <p>{t('preferredLanguage')}</p>
                <Radio.Group className="custom-wakame-radio" >
      <Radio.Button value="EN"> <img height="25" src={REACT_APP_PUBLIC_URL + '/images/english-icon.png'} alt="english" /> English </Radio.Button>
      <Radio.Button value="AR"><img height="25" src={REACT_APP_PUBLIC_URL + '/images/arabic-icon.png'} alt="arabic"/> Arabic</Radio.Button>
    </Radio.Group>

                </Form.Item>
                <Form.Item>
                  <Form.Item name="agreedtac" valuePropName="" >
                    <Checkbox onChange={(e) => setAgreed(e.target.checked)}>{t('iAgreeToThe')} <a href="/terms&condtion">{t('TermsAndPrivacyPolicy')}</a> {t('ofWakame')}</Checkbox>
                  </Form.Item>
                </Form.Item>
                <Form.Item>
                  <Button type="primary" htmlType="submit" className="login-form-button">
                  {t('signUp')}
                  </Button>
                </Form.Item>
             
              </Form>
              <div className="center">
                  <p>{t('or')}</p>
              <Button size="large" className="btn-block gmail-login-button">
              {t('loginWithGmail')}
              </Button>
              <Button  size="large" className="btn-block facebook-login-button">
              {t('loginWithFacebook')}
              </Button>
              </div>
           
          </Spin>
          </Modal>

        </Fragment>
    );
};


SignupManagement.propTypes = {
};

const mapStateToProps = state => ({
  locale: state.i18n.locale
});

const mapDispatchToProps = { };

export default connect(mapStateToProps, mapDispatchToProps)(SignupManagement);
