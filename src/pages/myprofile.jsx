import React, {useState,useEffect, Fragment} from "react";
import { Tabs, Button } from 'antd';
import MyProfileUpdate from '../containers/myprofile'
import MyAddressList from '../containers/address/addressLists'
import LoyalityManagement from '../containers/Loyality/index'
import FeedbackManagement from '../containers/feedback/index'
import AboutUs from '../containers/aboutus/index'
import ContactUsManagement from '../containers/contact-us/index'
import OrderHistory from './orderList'
import './styles/myprofile.css'
import propTypes from 'prop-types';
import {connect} from 'react-redux';
import { useTranslation } from "react-i18next";

const { TabPane } = Tabs;

const MyProfile = (props) => {
  const {t} = useTranslation()
  const [user, setUser] = useState({})
  useEffect(() => {
  const usr = localStorage.getItem('user') ? localStorage.getItem('user') : ''
  const pasrseUsr =  JSON.parse(usr)
  setUser(pasrseUsr)
   }, []);

    function callback(key) {
        console.log(key);
      }

      const logoutME = () => {
        localStorage.removeItem('token')
        localStorage.removeItem('user')
        window.location.replace('/')
      }

    return (
        <Fragment>
           <section style={{marginTop:"150px"}}>
                  <div className="container">
                  <div style={{color:"#ffffffb5"}}>
                    <h3 style={{color:"#ffffffb5"}}>{user && user.first_name} {user && user.last_name}</h3>
                    <div>
                      <div>
                        <p>{user && user.email} * +966-{user && user.phone}</p>
                      </div>
                      <div>
                        <Button className={props.locale == 'en' ? 'float-right' : 'float-left'} onClick={logoutME} type="ghost" style={{color:"#fff",marginTop: "-40px",width:"120px"}}>{t('logout')}</Button>
                      </div>
                    </div>
                    </div>
                    <section style={{marginTop:"100px", marginBottom:"100px"}}>
                    <Tabs className="myprofile-tabs" tabPosition="left" defaultActiveKey="1" onChange={callback}>
                          <TabPane tab={t('myProfile')} key="1" >
                            <section style={{marginTop:"20px", maxWidth:"380px"}}>
                              <p className="text-white text-uppercase section-subtitle">{t('editProfile')}</p>
                              <MyProfileUpdate />
                            </section>
                          </TabPane>
                          <TabPane tab={t('orderHistory')} key="2">
                            <OrderHistory />
                          </TabPane>
                          <TabPane tab={t('manageAddress')} key="3">
                            <MyAddressList />
                          </TabPane>
                          <TabPane tab={t('loyalityPoints')} key="4">
                          <p className="text-white text-uppercase section-subtitle mt-4">Loyality Points</p>
                            <LoyalityManagement />
                          </TabPane>
                          <TabPane tab={t('referralProgramme')} key="5">
                             Coming Soon
                          </TabPane>
                          <TabPane tab={t('feedback')} key="6">
                          <p className="m-0 text-white text-uppercase section-subtitle mt-4 text-capitilize">{t('feedback')}</p>
                           <p>{t('leaveFeedback')}</p>
                           <section style={{marginTop:"20px", maxWidth:"380px"}}>
                           <FeedbackManagement />
                           </section>
                          </TabPane>
                          <TabPane tab={t('footer-aboutus')} key="7">
                          <p className="m-0 text-white text-uppercase section-subtitle mt-4">{t('aboutWakame')}</p>
                          <section style={{marginTop:"20px", maxWidth:"580px"}}>
                            <AboutUs />
                          </section>
                          </TabPane>
                          <TabPane tab={t('footer-contactUs')} key="8">
                          <p className="m-0 text-white text-uppercase section-subtitle mt-4 text-capitilize">{t('footer-contactUs')}</p>
                          <p>{t('submitInquiries')}</p>
                            <ContactUsManagement />
                          </TabPane>
                          <TabPane tab={t('header-language')} key="9">
                            comingSoon
                          </TabPane>
                      </Tabs>
                    </section>
                  </div>
              </section>
          </Fragment>
    )
  }


MyProfile.propTypes = {
  locale: propTypes.string.isRequired,
};

const mapStateToProps = state => ({
    locale: state.i18n.locale
  });
  
  const mapDispatchToProps = { };
  
export default connect(mapStateToProps, mapDispatchToProps)(MyProfile);
