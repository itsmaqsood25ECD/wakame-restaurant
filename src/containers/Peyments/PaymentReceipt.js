import React,{useState,useEffect} from 'react';
import config from './Configuration.json';
import { format } from 'date-fns';
import CryptoJS from "crypto-js";
import ResponseMessage from './ResponseCode.json';
import axios from 'axios'
const setRequest = {
    "firstName": "",
    "lastName": "",
    "address": "",
    "city": "",
    "state": "",
    "zipCode": "",
    "phoneNumber": "",
    "trackid": "",
    "terminalId": "",
    "customerEmail": "",
    "action": '10',
    "merchantIp": "",
    "password": "",
    "currency": "",
    "country": "",
    "transid": "",
    "amount": "",
    "tokenOperation": "",
    "cardToken": "",
    "requestHash": "",
    "udf1": "",
    "udf2": "",
    "udf3": "",
    "udf4": "",
    "udf5": ""
}
let currentDate="";
let transId="";
let cardToken="";
let orderAmount="";

function generateHashSHA256(hashSequence){
    let hash = CryptoJS.SHA256(hashSequence).toString()  
    return hash;
}

const getURLParameters = () => {
    let urlParameters={};
    if (window.location.search.substring(1).length !== 0) {
        let sPageURL = window.location.search.substring(1);
        let sURLVariables = sPageURL.split("&");
        for (let i = 0; i < sURLVariables.length; i++) {
          let sParameterName = sURLVariables[i].split("=");
          urlParameters[sParameterName[0].split(",")]=sParameterName[1].split(",")
        }
    }
    return urlParameters;
}

const getRequestData = () => {
    let urlParameters = getURLParameters();
    setRequest.trackid=urlParameters.TrackId.toString();
    setRequest.terminalId=config.terminalId;
    setRequest.password=config.password;
    setRequest.currency=localStorage.getItem('getCurrency');
    setRequest.transid=urlParameters.TranId.toString();
    setRequest.amount=Number(urlParameters.amount.toString());
    let responseHashSequence = urlParameters.TrackId.toString()+"|"+config.terminalId+"|"+config.password+"|"+config.merchantkey+"|"+Number(urlParameters.amount.toString())+"|"+localStorage.getItem('getCurrency');
    let responseHash = generateHashSHA256(responseHashSequence)
    setRequest.requestHash=responseHash;
    currentDate=urlParameters.transDate? (urlParameters.transDate.toString()).toLocaleString():(format(new Date(), 'yyyy-MM-dd HH:mm'))+"";
    transId=urlParameters.TranId.toString();
    orderAmount=urlParameters.amount.toString();
    if((urlParameters.cardToken.toString()+"")==='null')
        cardToken="";
    else if(urlParameters.cardToken)
        cardToken=""+urlParameters.cardToken.toString();
    
};

function PaymentReceipt(props){
    const [status,setStatus] = useState({"alertMessage":"","alertType":"","isToShowReceipt":false});
    // Similar to componentDidMount and componentDidUpdate:
  useEffect(() => {
    let urlParameters = getURLParameters();
    let reqHashSequence= urlParameters.TranId + "|" + config.merchantkey + "|" + urlParameters.ResponseCode + "|" + urlParameters.amount;
    let reqHash = generateHashSHA256(reqHashSequence)
    //console.log("reqHashSequence : ",reqHashSequence)
    //console.log("reqHash : ",reqHash)
    //console.log("urlParameters.responseHash : ",urlParameters.responseHash)
    getRequestData();
  if(urlParameters.responseHash+"" === reqHash+""){
    axios.post(config.service_url,setRequest)
        .then((res) => {
            //console.log("payment receipt res : ",res)
            let getResponse=JSON.parse(JSON.stringify(res.data)), alertType="alert alert-success";
            orderAmount=urlParameters.amount
            if(getResponse.responseCode+"" === urlParameters.ResponseCode+"" && urlParameters.ResponseCode+""!=="000")
                alertType = "alert alert-danger";
            else if(res.data.responseCode!=="000")
                alertType='alert alert-danger';
            setStatus({...status,...{"alertType":alertType,"alertMessage":ResponseMessage[getResponse.responseCode]}})
        }).catch((error) => {
            console.log(error)
        });
  }else{
    setStatus({...status,...{"isToShowReceipt":true}})
  }},[]);
    

    return (
        <div className="cotainer" >
      <div className="row justify-content-center" style={!status.isToShowReceipt? {} : { display: 'none' }}>
          <div className="col-md-8">
              <div className="card">
                  <div style={{color: "white",backgroundColor:"#007bff"}} className="card-header">
                      <h1>Payment Receipt</h1>
                  </div>
                  <div className="card-body">
                      <form action="" method="">
                          <div className="form-group row">
                              <label htmlFor="transDate" className="col-md-12 col-form-label text-md-center"><strong>Transaction Date:- {currentDate}</strong></label>
                          </div>
                          {transId?<div className="form-group row" >
                              <label htmlFor="transId" className="col-md-4 col-form-label text-md-right">Transaction Id</label>
                              <div className="col-md-6">
                                  <input type="text" id="transId" className="form-control" value={transId} name="transId" disabled autoFocus/>
                              </div>
                          </div>
                            :null}
                          
                          {cardToken?<div className="form-group row" >
                                <label htmlFor="cardToken" className="col-md-4 col-form-label text-md-right">Card Token</label>
                              <div className="col-md-6">
                                  <input type="text" id="cardToken" className="form-control" value={cardToken}  name="cardToken" disabled/>
                              </div></div>
                              :null}
                          
                              
                              {orderAmount?<div className="form-group row">
                              <label htmlFor="amount" className="col-md-4 col-form-label text-md-right">Order Amount</label>
                              <div className="col-md-6">
                                  <input type="text" id="amount" className="form-control" value={orderAmount} name="amount" disabled/>
                              </div>
                          </div>
                                :null}
                          
                          
  
                      </form>
                  </div>
                  <div className="card-footer">
                      <h1>Status:</h1>
                      <div className={status.alertType} role="alert">
                          

                          <strong>
                          {status.alertMessage}
                          </strong>
                      </div>
                  </div>
  
              </div>
          </div>
      </div>
      <div style={status.isToShowReceipt? {} : { display: 'none' }}>
      <div className="alert alert-danger" role="alert">
            <strong>
            Hash Does Not Match!!
            </strong>
        </div>
      </div>
  </div>
    )
}

export default PaymentReceipt;