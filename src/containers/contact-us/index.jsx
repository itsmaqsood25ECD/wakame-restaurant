import React,{useState, useEffect, Fragment} from "react";
import { Form, Button, Input, Select, Spin, Row, Col } from 'antd';
import {  toast } from 'react-toastify';
import {
    MobileOutlined, 
    WhatsAppOutlined, 
    PhoneOutlined, 
    ClockCircleOutlined,
    EnvironmentOutlined
} from '@ant-design/icons'
import './contact-us.css'
import { getData, postData } from "../../services/api";
import propTypes from 'prop-types';
import {connect} from 'react-redux';
import { useTranslation } from "react-i18next";

const { Option } = Select;
const {TextArea} = Input;

function Index(props) {
    const {REACT_APP_API_URL} = process.env;
    const {t} = useTranslation()
    const [userID, setUserID] = useState(localStorage.getItem('userID') ? localStorage.getItem('userID') : '')
    const [activeBranch, setActiveBranch] = useState(localStorage.getItem('Active_Branch') ? JSON.parse(localStorage.getItem('Active_Branch')) : {})
    const [Branches, setBranches] = useState([])
    const [outletID, setOutletID] = useState()
    const [inquiryType, setInquiryType] = useState()
    const [load, setLoad] = useState(false)
    const [form] = Form.useForm()

    
   useEffect(() => {
       setActiveBranch(localStorage.getItem('Active_Branch') ? JSON.parse(localStorage.getItem('Active_Branch')) : {})
       getBranchOutlet()
   }, []);


   const onFinish = async(value) => {
       const formData = new FormData()
       formData.append("outlet_id", outletID)
       formData.append("type",  inquiryType)
       formData.append("name",  value.name)
       formData.append("phone",  value.phone)
       formData.append("email",  value.email)
       formData.append("message", value.message)
       formData.append("emp_id", userID)
       const res = await postData(`${REACT_APP_API_URL}/api/v1/outlet/contact`, formData)
       try {
        console.log(res)
        setLoad(false)
        if(res.data.status === 200){
            toast.success("Requested Successfully")
        }
           
       } catch (error) {
        toast.error("Something went wrong !")
       }
    }

    const onBranchChange = (value) => {
        setOutletID(value)
    }
    
    const onInquiryTypeChange = (value) => {
        setInquiryType(value)
    }

    async function getBranchOutlet () {
        const config = {
               "headers": {
                 "Access-Control-Allow-Origin": "*",
               }
        }
        const res = await getData(`${REACT_APP_API_URL}/api/v1/outlet/getoutlets`, '', config)
        try {
            console.log(res)
            setLoad(false)
            if(res.data.status === 200){
              setBranches(res.data.data)
              
            }

        } catch (error) {
            
        }

    }


    return (
        <Fragment>
        
        <Row>
            <Col xs={24} md={10} lg={10}>
            <Spin spinning={load} >
        <Form 
              name="feedback-form"
              className="feedback-form" 
              form={form}
              size="large"
              onFinish={onFinish}
            >
                <Form.Item
                        name="branchname"
                        rules={[{
                            required: true,
                            message: `${t('err-branchName')}`
                        }
                    ]}>
                      <Select
                        placeholder={t('selectBranch')}
                        onChange={onBranchChange}
                        allowClear
                        >
                        {
                            Branches.length > 0 ? Branches.map(branch => {
                                return <Option value={branch.outlet_id}>{branch.outlet_name[props.locale]}</Option>
                            }) : ''
                        }
                        

                        </Select>
                    </Form.Item>
                    <Form.Item
                        name="inquiryType"
                        rules={[{
                            required: true,
                            message: `${t('err-inquiryType')}`
                        }
                    ]}>
                       <Select
                        placeholder={t('typeOfInquiry')}
                        onChange={onInquiryTypeChange}
                        allowClear
                        >
                        <Option value="General">{t('general')}</Option>
                        <Option value="Event">{t('event')}</Option>
                        <Option value="other">{t('other')}</Option>
                        </Select>
                    </Form.Item>
                    
                    <Form.Item
                        name="name"
                        rules={[{
                            required: true,
                            message: `${t('err-name')}`
                        }
                    ]}>
                        <Input
                           name="name"
                            type="text"
                            varient="bottom-outline"
                            placeholder={t('name')}/>
                    </Form.Item>
                    <Form.Item
                        name="phone"
                        rules={[{
                            required: true,
                            message: `${t('err-mobileNumber')}`
                        }
                    ]}>
                        <Input
                            maxlength='10'
                            name="phone"
                            type="tel"
                            varient="bottom-outline"
                            placeholder={t('mobileNumber')}/>
                    </Form.Item>
                    <Form.Item
                        name="email"
                        rules={[{
                            required: true,
                            message: `${t('err-emailId')}`
                        }]}
                    >
                        <Input
                        name="email"
                            type="email"
                            placeholder={t('emailId')}
                            varient="bottom-outline"/>
                    </Form.Item>
                    <Form.Item 
                        name="message"
                        rules={[{
                            required: true,
                            message: `${t('err-feedback')}`
                        }]}>
                    <TextArea  varient="bottom-outline" showCount maxLength={500}  />
                    </Form.Item>

                    <Form.Item>
                        <Button type="primary" htmlType="submit" className="mt-3 text-uppercase login-form-button">
                          {t('submit')}
                        </Button>
                    </Form.Item>
                </Form>
        </Spin>
        
            </Col>
            <Col xs={24} md={14} lg={14}>
               {Object.keys(activeBranch).length === 0 && activeBranch.constructor === Object != '' ? <div>
              </div> : <div>
              <div className="contact-us-branch-contact-view p-3 ml-md-5 mr-md-5">
                  <p className="font-weight-bold">{activeBranch.outlet_name[props.locale]}</p>
                    <div className="d-flex mb-2">
                        <div className="branch-view-contact-icon bg-white text-primary">
                           <MobileOutlined />
                        </div>
                        <div>
                            <p className="branch-view-contact-details">{activeBranch.phone}</p>
                        </div>
                    </div>
                    <div className="d-flex mb-2">
                        <div className="branch-view-contact-icon bg-white text-primary">
                        <WhatsAppOutlined />
                        </div>
                        <div>
                            <p className="branch-view-contact-details">{activeBranch && activeBranch.whatsup}</p>
                        </div>
                    </div>
                    <div className="d-flex mb-2">
                        <div className="branch-view-contact-icon bg-white text-primary">
                        <PhoneOutlined />
                        </div>
                        <div>
                            <p className="branch-view-contact-details">{activeBranch && activeBranch.telphone}</p>
                        </div>
                    </div>
                    <div className="d-flex mb-2">
                        <div className="branch-view-contact-icon bg-white text-primary">
                        <ClockCircleOutlined />
                        </div>
                        <div>
                            <p className="branch-view-contact-details">{activeBranch && activeBranch.opentime} - {activeBranch && activeBranch.closetime}</p>
                        </div>
                    </div>
                    <div className="d-flex mb-2">
                        <div className="branch-view-contact-icon bg-white text-primary">
                        <EnvironmentOutlined />
                        </div>
                        <div>
                            <p className="branch-view-contact-details">{activeBranch && activeBranch.address && activeBranch.address[props.locale]}</p>
                        </div>
                    </div>
                    </div>
                  </div>}
            </Col>
        </Row>

               
        </Fragment>
    )
}


Index.propTypes = {
    locale: propTypes.string.isRequired,
  };
  
  const mapStateToProps = state => ({
      locale: state.i18n.locale
    });
    
    const mapDispatchToProps = { };
    
  export default connect(mapStateToProps, mapDispatchToProps)(Index);
  


