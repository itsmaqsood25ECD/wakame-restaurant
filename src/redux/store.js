import thunk from "redux-thunk";
import { createStore, applyMiddleware, compose } from "redux";
import {
  setLocale,
  loadTranslations,
  syncTranslationWithStore,
} from "react-redux-i18n";
import rootReducer from "./reducers/index";
import translations from "../l10n/translations";
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';


const persistConfig = {
  key: 'authType',
  storage: storage,
  whitelist: ['i18n'] // which reducer want to store
};

const pReducer = persistReducer(persistConfig, rootReducer);

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
  pReducer,
  composeEnhancers(applyMiddleware(thunk)),
);

syncTranslationWithStore(store);
store.dispatch(loadTranslations(translations));
store.dispatch(setLocale("en"));

const persistor = persistStore(store);

export { persistor, store};
