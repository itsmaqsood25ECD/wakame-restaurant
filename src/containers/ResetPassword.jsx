import React,{useState, useEffect} from "react";
import {useDispatch, useSelector} from 'react-redux'
import {showResetPassModal} from '../redux/actions/toggleAction'
import {Form, Input, Modal, Spin, Button} from 'antd';
import './styles/login.css'
import {  toast } from 'react-toastify';
import {postData} from '../services/api'
import {useTranslation} from 'react-i18next'
import propTypes from 'prop-types';
import { connect } from 'react-redux';
import { t } from "i18next";

const ResetPassword = () => {
    const {REACT_APP_API_URL} = process.env
    const [visible, setVisible] = useState()
    const [password, setPassword] =  useState('')
    const [repeatPassword, setRepeatPassword] = useState('')
    const [load, setLoad] = useState(false)
    const dispatch = useDispatch();
    const state_visible = useSelector(state => state.toggleStack.showResetPassModal)

    useEffect(() => {
        setVisible(state_visible);
    }, [state_visible]);

    const onClose = () => {
        dispatch(showResetPassModal(false))
    };

  
    const handleResetPassword = async () => {
        if(password !== repeatPassword){
            toast.error("Password and Repeat Password is not same")
            }else{
                setLoad(true)
                const formData = new FormData();
                formData.append("password", password)
                formData.append("mobile", sessionStorage.getItem("mobiletoReset"))
              
                const res = await postData(`${REACT_APP_API_URL}/api/v1/outlet/restpasswordbymobile`, formData)
                try {
                    if(res.data.status === 200){
                    setLoad(false)
                    sessionStorage.removeItem("mobiletoReset")
                    toast.success("Password Reset Successfully")
                    window.location.reload()
                   }else{
                       console.log(res)
                       setLoad(false)
                       toast.error(res.data.msg)
                   }
                   
               } catch (error) {
                setLoad(false)
               }
            }
    }

    return (
        <div>
          
            <Modal width={400} title="" footer={null} visible={visible} onCancel={onClose}>
                
               <Spin spinning={load} >

                        <div className="center">
                    <h2>{t('forgetPassword')}</h2>
                    <p></p>
                </div>
                <Form // id="components-form-normal-login"
                    name="normal_login" className="login-form" initialValues={{
                    remember: false,
                    username:'',
                    password:''

                }} size="large" >
                <br/>
                     <Form.Item
                        name="password"
                        rules={[{
                            required: true,
                            message: `${t('err-password')}`
                        }
                    ]}>
                        <Input.Password
                            type="password"
                            name="password"
                            onChange={(e) => setPassword(e.target.value)}
                            varient="bottom-outline"
                            placeholder="Enter New Password"/>
                    </Form.Item>
                    <Form.Item
                        name="repeatPassword"
                        rules={[{
                            required: true,
                            message: `${t('err-repeatPassword')}`
                        }
                    ]}>
                        <Input
                            type="password"
                            name="repeatPassword"
                            onChange={(e) => setRepeatPassword(e.target.value)}
                            varient="bottom-outline"
                            placeholder={t('repeatNewPassword')}/>
                    </Form.Item>
                    <Form.Item>
                    <Button type="primary" onClick={handleResetPassword} className="login-form-button">
                   {t('verify')}
                    </Button>
                    </Form.Item>
                </Form>
           </Spin>
           </Modal>
        </div>
    );
};


ResetPassword.propTypes = {
    locale: propTypes.string.isRequired,
};

const mapStateToProps = state => ({
    locale: state.i18n.locale
  });
  
  const mapDispatchToProps = { };
  
export default connect(mapStateToProps, mapDispatchToProps)(ResetPassword);
 