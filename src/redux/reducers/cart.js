import * as types from '../actions/types'

const initialState = {
    loading:true,
    error:null,
    cart: localStorage.getItem("cart") ? JSON.parse(localStorage.getItem("cart")) : []
}

export const GetCartItems = (state = initialState, action) => {
    switch(action.type) {
        case types.GET_CART_ITEMS:
            return {
                ...state,
                cart:localStorage.getItem("cart") ? JSON.parse(localStorage.getItem("cart")) : [],
                loading:false,
                error:null
            }
        default: 
        return state
    }
}

export const addToCart = (state = initialState, action) => {
    switch(action.type) {
        case types.ADD_TO_CART:
            return {
                ...state,
                cart:action.payload,
                loading:false,
                error:null
            }
        default: 
        return state
    }
}

export const clearCart = (state = initialState, action) => {

    switch(action.type) {
        case types.CLEAR_CART:
        let cartItems = [...action.payload]
        let uniq = {}
        
        cartItems = cartItems.length > 0 ? cartItems.filter(obj => !uniq[obj.prod_id] && (uniq[obj.prod_id] = true)) : action.payload
        return Object.assign({}, state, {
            cart : cartItems
        })
        default: 
        return state
    }
    
  }


//   const removefromCart = (state, action) => {
//     let cartItems = localStorage.getItem('MyCarts')
//     // let cartItems = [...state.cartItems]
//     if(cartItems !=  null){
//         cartItems = JSON.parse(cartItems)
//         let items = cartItems
//         if(cartItems.length > 1){
//         let uniq = {}
//          items = cartItems.filter(obj => !uniq[obj.prod_id] && (uniq[obj.prod_id] = true))
//         }
//       }
//     cartItems = cartItems.filter(r => r.id !=  action.id)
//     return Object.assign({}, state, {
//       cartItems
//     })
//   }