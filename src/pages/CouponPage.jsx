import React, { useEffect, useState  } from 'react'
import { getData } from '../services/api'
import { connect } from 'react-redux'
import propTypes from 'prop-types';
import {Button, Spin,Row, Col, Typography} from 'antd'
import { t } from 'i18next';

const {Paragraph} = Typography
export const CouponPage = (props) => {
    const {REACT_APP_API_URL, REACT_APP_PUBLIC_URL} = process.env
    const [Load, setLoad] = useState(false)
    const [CouponsList, setCouponsList] = useState([])

    useEffect(() => {
        getCoupons()
    }, [])

    const getCoupons = async() =>{
        setLoad(true)
        const res = await getData(`${REACT_APP_API_URL}/api/v1/outlet/couponall?page=0`)
        try {
          if(res.data.msg == 'Operation Successful'){
            setCouponsList(res.data.data)
            setLoad(false)
          }
          setLoad(false)
  
        } catch (error) {
            setLoad(false)
        }
      }

    return (
        <div>
          <div className="page-heading-section text-center bg-primary d-flex align-items-center justify-content-center" style={{height:200, marginTop:80}}>
            <p className="text-white text-lg">Coupons</p>
          </div>
          <div className="container">
          <Row gutter={16}>
         {Load ? <div className="page-loader"><Spin /></div> : <>  {CouponsList && CouponsList.length > 0 && CouponsList.map((coupon) => {
          return <Col xs={24} md={8} ><div className="card coupon-page-card coupon-card mb-2">
            <div className="card-body">
            <Button type="primary">{coupon.code}</Button>
            {coupon && coupon.coupon_title != '' && <p className="coupon-title">
              {coupon && props.locale == 'en' ? coupon.coupon_title :  coupon.coupon_title_ar}
              </p>}
              {coupon && coupon.coupon_desc != '' && <p className="coupon-desc">
              {coupon && props.locale == 'en' ? coupon.coupon_desc : coupon.coupon_desc_ar}
              </p>}
            <div className="d-flex justify-content-between">
              {coupon.outlet_name != '' && <div>
                <p className="mb-0 text-sm text-gray">{t('valid_at')}</p>
                <p>{coupon.outlet_name}</p>
              </div>}
              {coupon.offer_start_date != '' && <div>
                <p className="mb-0 text-sm text-gray">{t('valid_from')}</p>
                <p>{coupon.offer_start_date}</p>
              </div>}
              {coupon.offer_end_date != '' && <div>
                <p className="mb-0 text-sm text-gray">{t('valid_till')}</p>
                <p>{coupon.offer_end_date}</p>
              </div>}  
            </div>
            <Button type="ghost" className="apply-coupon-btn" ><Paragraph copyable={{ text: coupon.code, icon: [`${t('copyCode')}`, `${t('copied')}`] }}></Paragraph></Button>
            </div>
          </div> 
          </Col>
          })}</>}
          </Row>
          </div>
        </div>
    )
}


CouponPage.propTypes = {

  locale: propTypes.string.isRequired,
};

const mapStateToProps = state => ({
  locale: state.i18n.locale

});

export default connect(mapStateToProps, '')(CouponPage)
