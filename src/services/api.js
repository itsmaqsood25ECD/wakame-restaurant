import axios from "axios";

axios.defaults.validateStatus = status => {
  return status >= 200 && status <= 500;
};

axios.defaults.timeout = 20000;
axios.defaults.headers.common["Content-Type"] = "application/json";
axios.defaults.headers.common["accept"] = "application/json";
axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*';
axios.defaults.headers.common["crossDomain"] = true;
axios.defaults.headers.common["Authorization"] = localStorage.getItem("token") ? localStorage.getItem("token") : '';

/*
  Use axios interceptors to configure headers, show response error messages etc.
*/


axios.interceptors.request.use(
  config => {
    return config;
  },
  error => {
    return Promise.reject(error);
  }
);

axios.interceptors.response.use(
  response => {
    return response;
  },
  error => {
    if (error.response !==  undefined) {
      alert(error);
    }
    return Promise.reject(error);
  }
);

export function configureAPI() {
  axios.defaults.headers.post["crossDomain"] = true;
}

const proxy =  {
  // `proxy` means the request actually goes to the server listening
  // on localhost:3000, but the request says it is meant for
  // 'http://httpbin.org/get?answer=42'
  proxy: {
    host: 'https://wakameuat.upappfactory.app/wakame-admin'
  }
}




export function postData(url,header, body) {
  return axios.post(url,header, body);
}

export function postInfo(url, body) {
  return axios.post(url, body);
}
export function putData(url, header, body) {
  return axios.put(url, header, body);
}
export function deleteData(url, body) {
  return axios.delete(url, body);
}
export function getNews(url, body) {
  return axios.get(url, body);
}
export function getInfo(url, body, config) {
  return axios.get(url, body, config) ;
}

export function getData(url, body) {
  return axios.get(url, body);
}
export function getMiniStories(url, body) {
  return axios.get(url, body);
}
export function imageUpload(url, body){
  return axios.post(url, body);
}
export function getLocalNews(url) {
  return axios.get(url);
}
