import React, { Suspense } from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import "./styles/index.css";
import {store} from "./redux/store";
import App from "./app";
import { ConfigProvider } from 'antd';
import getCurrentLocale from './redux/get-current-locale'
import './styles/css/antd-dark.css'
import {Spin} from 'antd'
import './i18n'

const locale = getCurrentLocale()

ReactDOM.render(
    <Suspense fallback={<Spin />}>
      <ConfigProvider direction={locale !== 'ar' ? 'ltr': 'rtl'}>
  <Provider store={store}>
        <App />
  </Provider>
      </ConfigProvider>
      </Suspense>
  ,
  document.getElementById("root"),
);
