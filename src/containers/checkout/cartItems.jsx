import React, {   useState,   useEffect,   forwardRef,   useRef,   useImperativeHandle, } from "react";
import propTypes from 'prop-types';
import { connect } from 'react-redux';
import {getData, postData} from '../../services/api'
import {addToCart, clearCart, getCartItems} from '../../redux/actions/cart'
import {allProducts, updateProductList} from '../../redux/actions/products'
import '../styles/cart.css';
import {Button,Drawer, Spin, Radio ,Row, Col, Checkbox, Statistic, Input } from 'antd'
import {ButtonGroup } from 'reactstrap';
import { FaMinus, FaPlus, FaTrashAlt, FaPercent } from 'react-icons/fa';
import { InfoCircleOutlined } from '@ant-design/icons';
import moment from 'moment'
import {toast, ToastContainer} from 'react-toastify'
import { useHistory } from "react-router-dom";
import config from '../Peyments/Configuration.json';
import CryptoJS from "crypto-js"
import axios from 'axios'
import { useTranslation } from "react-i18next";

const generateHashSHA256 = (hashSequence) => {
  // hashSequence = trackid | terminalId | password | secret | amount | currency
  let hash = CryptoJS.SHA256(hashSequence).toString()
  return hash;
}

let resParameter={}

const CartItems = forwardRef((props, ref) => {
  let history = useHistory();
  const {t} = useTranslation()
  const {REACT_APP_API_URL} = process.env
    let storedBranch = localStorage.getItem("Active_Branch");
    let branches = storedBranch ? JSON.parse(storedBranch): '';
    let user = localStorage.getItem("user") ? JSON.parse(localStorage.getItem("user")) : ''
    let cartItems;
    let vatPercentage = 0;
    // let loyalityToOmrRate = 0.001;
    const [load, setLoad] = useState(false)
    const [loyaltyPoints, setLoyalityPoints] = 
    useState(sessionStorage.getItem("loyalityPoints") ? 
    JSON.parse(sessionStorage.getItem("loyalityPoints")) : 
    {loyality_available_points:0.0,
    loyality_point:0.0,
    loyality_used:0.0,
    loyality_used_this_month: 0.00})
    const [MyCartItems, SetMyCartItems] = useState([])
    const [IP, setIP] = useState('0.0.0.0');
    const [availableCoupons, setAvailableCoupons] = useState([])
    const [delivaryMethodType, setDelivaryMethodType] = useState(sessionStorage.getItem("delivaryMethodType") ?  JSON.parse(sessionStorage.getItem("delivaryMethodType")) : 'Take Away' )
    const [subTotal, setSubTotal] = useState()
    const [loyalityPointsAmountValue ,setLoyalityPointsAmount] = useState(0)
    const [totalPrice, setTotalPrice] = useState()
    const [priceLoad ,setPriceLoad] = useState(false)
    const [couponDetailsModal , setCouponDetailsModal] =  useState(false)
    const [delivaryNote ,setDelivaryNote] = useState(sessionStorage.getItem("delivaryNote") ? JSON.parse(sessionStorage.getItem("delivaryNote")) : '')
    const [usedLoyality, setUsedLoyality] = useState( sessionStorage.getItem("usedLoyality") ? JSON.parse(sessionStorage.getItem("usedLoyality")) : false)
    const [delivaryFee, setDelivaryFee] = useState(0)
    const [userID, setUserID] = useState(localStorage.getItem('userID') ? localStorage.getItem('userID') : '')
    const [toggleCoupon, setToggleCoupon] = useState(false)
    const [couponCodeApplied, setCouponCodeApplied] = useState('')
    const [Time, setTime] = useState('')
    const [Date, setDate] = useState('')
    const [double, setDouble] = useState(false);
    const [loyalityToOmrRate, setLoyalityToOmrRate] = useState(0)
    const [cookingTime, setCookingTime] = useState(0)
    const [isDelivaryChargeApplied, setDelivaryChargeApplied] = useState(false)
    const [delivaryInfo , setDelivaryInfo] = useState(sessionStorage.getItem("delivaryInfo") ? JSON.parse(sessionStorage.getItem("delivaryInfo")) : {
                                              flat: 0,
                                              free: 0
                                              })
                                              
    const { TextArea } = Input;
    
    useEffect(() => {
        getDelivaryCharges()
        cartItems = localStorage.getItem("cart") ? JSON.parse(localStorage.getItem("cart")) : []
        SetMyCartItems(cartItems)
        getCartItems()
        CalculateTotal()
        getCookingTime(cartItems)
    
    }, [props.productList, delivaryMethodType])

    useEffect(() => {
      getLoayaltyPoints()
      getLoayaltyPointsInfo()
      getCoupons()
      getIPData()
    },[])

    const getIPData = async () => {
      const res = await axios.get('https://geolocation-db.com/json/')
      console.log(res.data);
      setIP(res.data.IPv4)
    }

    const getCookingTime = (cartItems) => {
      if(cartItems != undefined || cartItems && cartItems.length > 0){
      const cookingTime = Math.max.apply(Math, cartItems.map(function(o) { return o.cooking_time; }))
      setCookingTime(cookingTime)
      }else {
        setCookingTime(0)
      }
    }

    useImperativeHandle(ref, () => ({
      checkoutProduct(date, time, paymentMethod) {
        setTime(moment(time).format('LT'))
        setDate(moment(date).format('l'))
        varifyCheckoutDetails(moment(date).format('l'), moment(time).format('LT'), paymentMethod)
      }
    }))

   const onSubmit = (odr_id) => {
    let index=0;
    let count=0;
    let queryParam="";
    let hashSequence=generateHashSHA256(odr_id+"|"+config.terminalId+"|"+config.password+"|"+config.merchantkey+"|"+totalPrice+"|"+'SAR')
    console.log(odr_id+"|"+config.terminalId+"|"+config.password+"|"+config.merchantkey+"|"+totalPrice+"|"+'SAR')
    const values = {
        firstName: user.first_name,
        lastName: user.last_name,
        address:  "",
        city:  "",
        state:  "",
        zipCode:  "",
        phoneNumber:  user.phone,
        trackid:  odr_id,
        terminalId:  config.terminalId,
        customerEmail:  user.email,
        action:  "1",
        merchantIp:  IP,
        password:  config.password,
        currency:  "SAR",
        country:  "SA",
        transid:  odr_id,
        amount:  totalPrice,
        tokenOperation:  "A",
        cardToken:  "",
        tokenizationType:  "0",
        requestHash:  hashSequence,
        udf1:  `${window.location.origin}/paymentStatus`,
        udf2:  `${window.location.origin}/paymentStatus`,
        udf3:  `${window.location.origin}paymentReceipt`,
        udf4:  "",
        udf5:  "",
    }
      setDouble(true);
      console.log("values", values)
      axios.post(config.service_url, values)
          .then((res) => {
              console.log("targetUrl "+res.data.targetUrl)
              console.log(res.data)
              resParameter=res.data;
              console.log("resParameter ",resParameter)
              if(resParameter.targetUrl+""==="null"){
                  for (let [key, value] of Object.entries(resParameter)) {
                      index=++index;
                      console.log(`${key} ${value}`); 
                  }
                  for (let [key, value] of Object.entries(resParameter)) {
                      count=++count;
                      queryParam=queryParam+key+"="+value;
                      if(count<index)
                          queryParam=queryParam+"&"
                  }
                  console.log("index : ",index,queryParam)
                  window.location.assign(window.location.origin.toString()+'/paymentStatus?'+queryParam)
                  setDouble(true)
                  setLoad(false)
              }else{
                  window.location.assign(res.data.targetUrl.replace('?','')+"?paymentid="+res.data.payid);
                  setDouble(true)
                  setLoad(false)
              }
          }).catch((error) => {
              console.log(error)
      });
  }

   const varifyCheckoutDetails = (date, time, paymentMethod) => {
     console.log(date, time, MyCartItems)
        if(MyCartItems == '' || MyCartItems && MyCartItems.length < 1 || MyCartItems == undefined){
          toast.error("please Add atleast one item in cart")
        }else if(delivaryMethodType == '' || delivaryMethodType == null){
          toast.error("please Select Take Away or Delivary method")
        }else {
          placeOrder(date, time, paymentMethod)
        }
   }

   const placeOrder = async(date, time, paymentMethod) => {
    setLoad(true)
    const formData = new FormData();
    formData.append('outlet_id', branches && branches.outlet_id);
    formData.append('order_details', JSON.stringify(MyCartItems));
    formData.append('order_status', 'neworder');
    formData.append('order_time_expected', cookingTime);
    formData.append('emp_id', userID)
    formData.append('payment_method', paymentMethod)
    formData.append('address', localStorage.getItem('default_address'))
    formData.append('deliveryfee', delivaryFee)
    formData.append('service_method', delivaryMethodType)
    formData.append('loyalityused', usedLoyality ? loyaltyPoints.loyality_available_points : 0)
    formData.append('total_price', subTotal)
    formData.append('final_price', totalPrice)
    formData.append('order_note', delivaryNote)
    formData.append('pickup_date', date)
    formData.append('pick_time', time)
    formData.append('voucher', couponCodeApplied)

    const res = await postData(`${REACT_APP_API_URL}/api/v1/outlet/orderadd`, formData)
    try {
       if(res.data.msg == "Operation Successful"){
         console.log("odr res", res)
         if(paymentMethod == 'COD'){
          toast.success(`${t('orderPlaced')}`)
          localStorage.removeItem("cart")
          history.push("/checkout/success");
         }
        
         if(paymentMethod ==  "Credit Card"){
          onSubmit(res.data.data)
         }
       
         
       }else {
         toast.error(`${t('orderNotPlaced')}`)
         setLoad(false)
       }
      } catch (error) {
        setLoad(false)
    }

   }

    const getCoupons = async() =>{
      const res = await getData(`${REACT_APP_API_URL}/api/v1/outlet/couponall?page=0`)
      try {
        if(res.data.msg == 'Operation Successful'){
          setAvailableCoupons(res.data.data)
        }

      } catch (error) {
        
      }
    }
    
    const CalculateTotal = () => {
      setPriceLoad(true)
      cartItems = localStorage.getItem("cart") ? JSON.parse(localStorage.getItem("cart")) : []
      let totalPrice = cartItems.reduce(function (accumulator, item) {
        return accumulator + (item.discount_price == "0.00" ? +item.price * item.quantity : +item.discount_price * item.quantity);
      }, 0);
      setSubTotal(totalPrice)
      if(usedLoyality === true){
        console.log("if")
        const loyalityPointAmmount = loyaltyPoints.loyality_available_points * loyalityToOmrRate; 
        console.log("loyalityPointAmmount", loyalityPointAmmount)
        setLoyalityPointsAmount(loyalityPointAmmount)
        getCurrentPrice(totalPrice, loyalityPointAmmount)
        
      }else if (usedLoyality === false){
        console.log("else IF")
        setLoyalityPointsAmount(0)
        getCurrentPrice(totalPrice, 0)
        
      }
      setPriceLoad(false)
    }

    const getCurrentPrice = (price, loyalityAmmount) => {
      let totalAndDelivary = 0;
      let currentTotal = 0;
      const vetAmmount = (+price*vatPercentage)/100;
      const totalPriceAfterVat = vetAmmount + price;
      if(price <= delivaryInfo.free){
        if(delivaryMethodType == 'Take Away'){
          totalAndDelivary = parseFloat(totalPriceAfterVat)
        }else if(delivaryMethodType == 'Delivery'){
          totalAndDelivary = ((parseFloat(totalPriceAfterVat) + parseFloat(delivaryInfo.flat)))
        }
        currentTotal =  (totalAndDelivary - parseFloat(loyalityAmmount))
        setTotalPrice(currentTotal)
        console.log(totalAndDelivary, currentTotal)
        setDelivaryChargeApplied(true)
        setDelivaryFee(delivaryInfo.flat)
        console.log("total IF",totalPriceAfterVat, loyalityAmmount, currentTotal)
        }else{
          setTotalPrice(price)
          setDelivaryChargeApplied(false)
          setDelivaryFee(0)
          console.log("total ELSE",totalPriceAfterVat, loyalityAmmount, currentTotal)
        }
    }

    const getCurrentPriceCoupon = (price, CouponAmmount) => {
      let totalAndDelivary = 0;
      let currentTotal = 0;
      const vetAmmount = (+price*vatPercentage)/100;
      const totalPriceAfterVat = vetAmmount + price;
      if(price <= delivaryInfo.free){
        if(delivaryMethodType == 'Take Away'){
          totalAndDelivary = parseFloat(totalPriceAfterVat)
        }else if(delivaryMethodType == 'Delivery'){
          totalAndDelivary = ((parseFloat(totalPriceAfterVat) + parseFloat(delivaryInfo.flat)))
        }
        currentTotal =  (totalAndDelivary - parseFloat(CouponAmmount))
        setTotalPrice(currentTotal)
        console.log(totalAndDelivary, currentTotal)
        setDelivaryChargeApplied(true)
        setDelivaryFee(delivaryInfo.flat)
        console.log("total IF",totalPriceAfterVat, CouponAmmount, currentTotal)
        }else{
          currentTotal =  (price - parseFloat(CouponAmmount))
          setTotalPrice(currentTotal)
          setDelivaryChargeApplied(false)
          setDelivaryFee(0)
          console.log("total ELSE",totalPriceAfterVat, CouponAmmount, currentTotal)
        }
    }

    const getLoayaltyPointsInfo = async() => {

      const res = await getData(`${REACT_APP_API_URL}/api/v1/outlet/getloyality`)
      try {
             if(res.data.msg == 'Operation Successful'){
              setLoyalityToOmrRate(res.data.data[0].redeem_point)
             }
      } catch (error) { 
      }
     }

    const getLoayaltyPoints = async() => {
     const formdata = new FormData()
     formdata.append("emp_id", userID)
     const res = await postData(`${REACT_APP_API_URL}/api/v1/outlet/getloyalitybyemp`, formdata)
     try {
            if(res.data.msg == 'Operation Successful'){
              sessionStorage.setItem("loyalityPoints", JSON.stringify(res.data.data[0].point))
              setLoyalityPoints(res.data.data[0].point)
            }
     } catch (error) { 
     }
    }

    const getDelivaryCharges = async() => {
      const res = await getData(`${REACT_APP_API_URL}/api/v1/outlet/delivery`)
      try {
             if(res.data.msg == 'Operation Successful'){
               setDelivaryInfo(res.data.data)
               sessionStorage.setItem("delivaryInfo", JSON.stringify(res.data.data))
             }
      } catch (error) { 
      }
     }

     const parseDate = (dt) => {
      var parts = dt.split('-');
      console.log("parts",parts)
      console.log(`${parts[2]}-${parts[1]}-${parts[0]}`)
      return `${parts[2]}-${parts[1]}-${parts[0]}`
     }

    const CheckCoupon = (coupon) => {
      const dateNow = moment().format('YYYY-MM-DD')
      const offerStartDate = parseDate(coupon.offer_start_date)
      const offerEndDate = parseDate(coupon.offer_end_date)
      console.log(dateNow, offerStartDate)
      if(moment(dateNow).isBefore(offerStartDate)){
        toast.error(`${t('coupon-err-dateBefore')} ${coupon.offer_start_date}!`)
      }else if(moment(dateNow).isAfter(offerEndDate)){
        toast.error(`${('coupon-err-dateAfter')} ${coupon.offer_end_date}`)
      }else if(branches.outlet_id != coupon.outlet_id){
        toast.error(`${t('coupon-err-branch')}`)
      }else if(coupon.min_spent_amount !== '' && subTotal < coupon.min_spent_amount){
        toast.error(`${t('coupon-err-amountMin')} ${coupon.min_spent_amount} ${t('coupon-err-toCart')}`)
      }else if(coupon.max_spent_amount !== '' && subTotal > coupon.max_spent_amount){
        toast.error(`${t('coupon-err-notAllowed')} ${coupon.max_spent_amount}`)
      }else if(coupon.status < 1){ 
        toast.error(`${t('coupon-err-notActive')}`)
      }else if(coupon.type == '%'){ 
        const couponAmt = subTotal / coupon.amount
        console.log("% amt", couponAmt)
        getCurrentPriceCoupon(subTotal, couponAmt)
        setCouponCodeApplied(coupon.code)
        setCouponDetailsModal(false)
        setToggleCoupon(true)
      }else if(coupon.type == 'Off'){ 
        console.log("Off",  coupon.amount)
        getCurrentPriceCoupon(subTotal, coupon.amount)
        setCouponCodeApplied(coupon.code)
        setCouponDetailsModal(false)
        setToggleCoupon(true)
      }else {
        getCurrentPriceCoupon(subTotal, 0)
        setCouponDetailsModal(false)
        setToggleCoupon(true)
      }
 
    }

    const removeCoupon = (coupon) => {
      getCurrentPriceCoupon(subTotal, 0)
      setCouponDetailsModal(false)
      setToggleCoupon(false)
      setCouponCodeApplied('')
    }


        const getDecrement = (id) => {
              let products = [...props.productList]
              products.map((i) => {
                if (i.prod_id == id) {
                  if (i.quantity > 1) {
                    i.quantity = i.quantity ? i.quantity - 1 : 1
                  }
                  else if (i.quantity === 1) {
                    i.quantity = i.quantity
                    i.isAdd = false
                    // props.removecart(id)
                  }
                  else {
                    i.quantity = 0
                    i.isAdd = false
                    // props.removecart(id)
                  }
                }
              })
           
              let r = localStorage.getItem('cart')
              // console.log("carts", r)
              if (r !=  null) {
                let val = JSON.parse(r)
                val.map((i) => {
                  if (i.prod_id === id) {
                    if (i.quantity > 1) {
                      i.quantity = i.quantity ? i.quantity - 1 : 1
                      localStorage.setItem('cart', JSON.stringify(val))
                    }
                    else if (i.quantity === 1) {
                      i.quantity = i.quantity
                      i.cartprice = i.defPrice
                      i.isAdd = false
                      let a = val.filter((j) => j.prod_id !==  id)
                      localStorage.setItem('cart', JSON.stringify(a))
                    }
                    else {
                      i.quantity = 0
                      i.isAdd = false
                      let a = val.filter((j) => j.prod_id !== id)
                      localStorage.setItem('cart', JSON.stringify(a))
                    }
                  }
                })
          
              }
              props.updateProductList(products)
              CalculateTotal()
              getCookingTime(products)
        }

        const getCounter = (id) => {
          const products = [...props.productList]
             console.log("props", props)
             products.map(function (item) {
              if(item.prod_id ===  id){
                if(item.product_qty == 0)
                item.quantity = item.quantity ? item.quantity + 1 : 1;
                else if(item.product_qty > item.quantity)
                item.quantity = item.quantity ? item.quantity + 1 : 1;
                else{
                  item.quantity = item.quantity ? item.quantity : 1;
                  toast.warning("Sorry! limited stock available for this item")
                }
              }else{
                item.quantity = item.quantity ? item.quantity : 1;
                
              }
            });
          let r = localStorage.getItem('cart')
          if (r !=  null) {
            let val = JSON.parse(r)
            val.map((i) => {
              if (i.prod_id == id) {
                if(i.product_qty == 0)
                i.quantity = i.quantity ? i.quantity + 1 : 1
                else if(i.product_qty > i.quantity)
                i.quantity = i.quantity ? i.quantity + 1 : 1
                else
                i.quantity = i.quantity ? i.quantity : 1
              }else{
                i.quantity = i.quantity ? i.quantity : 1
              }
            })
            localStorage.setItem('cart', JSON.stringify(val))
          }
          else {
            let cart = props.productList.filter((i) => i.isAdd)
            localStorage.setItem('cart', JSON.stringify(cart))
          }

          props.updateProductList(products)
          CalculateTotal()
          getCookingTime(products)

          }

          const handleDeliveryNote = (e) => {
            setDelivaryNote(e.target.value)
            sessionStorage.setItem("delivaryNote", JSON.stringify(e.target.value))
          }

          function handleChangeLoyality(e) {
            const loyalityPointAmmount = (loyaltyPoints.loyality_available_points * loyalityToOmrRate); 
            if(e.target.checked === true && loyalityPointAmmount > totalPrice){
              toast.error(`${t('err-loyalityPoint')}`)
            }else{
              sessionStorage.setItem("usedLoyality", JSON.stringify(e.target.checked))
              console.log(`checked = ${e.target.checked}`);
              setUsedLoyality(e.target.checked)
              if(e.target.checked === true){
                console.log("if")
                const loyalityPointAmmount = (loyaltyPoints.loyality_available_points * loyalityToOmrRate); 
                console.log("loyalityPointAmmount", loyalityPointAmmount)
                getCurrentPrice(subTotal, loyalityPointAmmount)
                
              }else if (e.target.checked === false){
                console.log("else IF")
                const loyalityPointAmmount = 0
                getCurrentPrice(subTotal, loyalityPointAmmount)
              }
            }
           
          }

          const handleDeleteItem = (prod) => {
              console.log(prod)
          }

          const handleDeliveryType = (e) => {
            sessionStorage.setItem("delivaryMethodType", JSON.stringify(e.target.value))
            setDelivaryMethodType(e.target.value)
            getCurrentPrice(subTotal, loyalityPointsAmountValue)
            removeCoupon()
          }

        const couponCodes = (
          <Drawer
          title={`${t('coupon-availableCoupon')}`}
          placement="right"
          closable={true}
          onClose={()=> setCouponDetailsModal(false)}
          visible={couponDetailsModal}
          key="left"
          className="responsive-drawer"
        >
          {availableCoupons && availableCoupons.length > 0 && availableCoupons.map((coupon) => {
          return <div className="card coupon-card mb-2">
            <div className="card-body">
            <Button type="primary">{coupon.code}</Button>
            {coupon && coupon.title != '' && <p className="coupon-title">
              {coupon && coupon.title}
              </p>}
              {coupon && coupon.desc != '' && <p className="coupon-desc">
              {coupon && coupon.desc}
              </p>}
            <div className="d-flex justify-content-between">
              {coupon.outlet_name != '' && <div>
                <p className="mb-0 text-sm text-gray">{t('valid_at')}</p>
                <p>{coupon.outlet_name}</p>
              </div>}
              {coupon.offer_start_date != '' && <div>
                <p className="mb-0 text-sm text-gray">{t('valid_from')}</p>
                <p>{coupon.offer_start_date}</p>
              </div>} 
              {coupon.offer_end_date != '' && <div>
                <p className="mb-0 text-sm text-gray">{t('valid_till')}</p>
                <p>{coupon.offer_end_date}</p>
              </div>}  
            </div>
            <Button onClick={() => CheckCoupon(coupon)} type="ghost" className="apply-coupon-btn">{t('applyCoupon')}</Button>
            </div>
          </div> 
          })}
          </Drawer>
        )

    return (
        <>
        {load ? <div className="page-loader"><Spin /></div> : <> 
        <div className="cart-items-container">
            <p>{t('cart')} ( {MyCartItems && MyCartItems.length} {t('items')} )</p>
            {MyCartItems && MyCartItems.length > 0 ? MyCartItems.map(prod => {
                return  <div className="cart-items">
                <div className="item-details">
                    <p>{prod.name[props.locale]}</p>
                    <ButtonGroup className="action-btn">
                        <Button size="small" onClick={() => getDecrement(prod.prod_id)} type="ghost" ><FaMinus /></Button>
                        <Button size="small"  type="ghost" >{prod.quantity}</Button>
                        <Button size="small" onClick={() => getCounter(prod.prod_id)} type="ghost" > <FaPlus /></Button>
                    </ButtonGroup>
                </div>
                <div className={`item-action ${props.locale == "en" ? '' : 'text-left'}`}>
                   <p> <FaTrashAlt onClick={() => handleDeleteItem(prod)} /></p>

                    <p><Statistic valueStyle={{ color: '#d7d7d7', fontSize:"16px" }} prefix="SR" value={(prod.discount_price == "0.00" ? prod.price * prod.quantity : prod.discount_price * prod.quantity) } precision={3}/></p>
                </div>
               

            </div>
          
            }) : <div className="no-prod-available" style={{padding:"0px"}}>
            <img  src={process.env.REACT_APP_PUBLIC_URL + '/images/NoItemAvailable.svg' } alt="No Product Available" />
            <p>
               {t('noItemAvailableCart')}
            </p>
            </div> }
        </div>
        <br />
        <TextArea onChange={handleDeliveryNote} value={delivaryNote} placeholder={t('additionalNote')} varient="bottom-outline" showCount  maxLength={500}  />
        <br />
        {usedLoyality == false ? <div className="mt-3 mb-3"> {toggleCoupon == true ? <div className="coupon-applied">
        <FaPercent />{couponCodeApplied} <span className="removeCouponbtn" onClick={() => removeCoupon()}><FaTrashAlt /></span>
        </div> : <Button size="large" onClick={() => setCouponDetailsModal(true)} className="apply-coupon-btn" type="dashed" block > <FaPercent />  {t('applyCoupon')}</Button>} 
         {couponCodes}
        </div> : ''
        }
       
        {toggleCoupon == false ? <div>
        <Checkbox 
        checked={usedLoyality}
        onChange={handleChangeLoyality} 
        style={{color:"#d7d7d7"}}>{t('redeemLoyaltyPoints')}</Checkbox>
        <p style={{color:"#519048", paddingLeft:"25px", paddingRight:"25px"}}>{t('yourAvailableLoyaltyPoints')} : {loyaltyPoints.loyality_available_points}</p>
        </div> : ''}
        <br />
        <Radio.Group onChange={handleDeliveryType} value={delivaryMethodType} className="checkout-mode-radio" size="large" defaultValue="Delivery" buttonStyle="solid">
      <Radio.Button value="Take Away">{t('takeAway')}</Radio.Button>
      <Radio.Button value="Delivery">{t('delivery')}</Radio.Button>
    </Radio.Group>
    {delivaryMethodType == 'Take Away' && <p className="text-white mt-2 mb-2">
    <InfoCircleOutlined /> {t('takeawaynote')}{branches.outlet_name[props.locale]}.
    </p> }
    <Spin spinning={priceLoad} delay={500}>
    <Row className="mt-5">
        <Col flex="auto"><p className="checkout-total-heading">{t('subTotal')}</p></Col>
        <Col flex="auto"><p className={`checkout-total-value ${props.locale == "en" ? '' : 'text-left'}`}><Statistic valueStyle={{ color: '#fff', fontSize:"16px" }} prefix="SR" value={subTotal} precision={3}/></p></Col>
    </Row>
    {delivaryMethodType == 'Delivery' ?
    <Row>
        <Col flex="auto"><p className="checkout-total-heading">{t('deliveryCharges')}</p></Col>
        <Col flex="auto"><p className={`checkout-total-value ${props.locale == "en" ? '' : 'text-left'}`}>{isDelivaryChargeApplied ? <Statistic valueStyle={{ color: '#fff', fontSize:"16px" }} prefix="SR" value={delivaryInfo.flat} precision={3}/> : <Statistic valueStyle={{ color: '#fff', fontSize:"16px" }} prefix="SR" value={0} precision={3}/> }</p></Col>
    </Row> : <Row>
        <Col flex="auto"><p className="checkout-total-heading">{t('deliveryCharges')}</p></Col>
        <Col flex="auto"><p className={`checkout-total-value ${props.locale == "en" ? '' : 'text-left'}`}>{t('notApplicable')}</p></Col>
    </Row>
      }
    {/* <Row>
        <Col flex="auto"><p className="checkout-total-heading">VAT</p></Col>
        <Col flex="auto"><p className={`checkout-total-value ${props.locale == "en" ? '' : 'text-left'}`}>15%</p></Col>
    </Row> */}
    <div className="border-dashed"></div>
    <Row>
        <Col flex="auto"><p className="checkout-total-heading mb-0">{t('total')}</p>
        <p style={{fontSize:"12px"}}>({t('inclusiveTexes')})</p>
        </Col>
        <Col flex="auto"><p className={`checkout-total-value ${props.locale == "en" ? '' : 'text-left'}`}><Statistic valueStyle={{ color: '#fff', fontSize:"16px" }} prefix="SR" value={totalPrice} precision={3}/></p></Col>
    </Row>
    <Row className="mt-3 mb-3 bg-primary pl-3 pr-3 pt-2">
        <Col flex="auto"><p className="checkout-total-heading">{t('cookingTime')}</p></Col>
        <Col flex="auto"><p className={`checkout-total-value ${props.locale == "en" ? '' : 'text-left'}`}><Statistic valueStyle={{ color: '#fff', fontSize:"16px" }} suffix="Minutes" value={cookingTime == "-Infinity" ? 0 : cookingTime} precision={2}/></p></Col>
    </Row>
    </Spin>
    </>
   }
   <ToastContainer />
    </>);
});


CartItems.propTypes = {
    addToCart: propTypes.func.isRequired,
    allProducts:propTypes.func.isRequired,
    updateProductList: propTypes.func.isRequired,
    clearCart: propTypes.func.isRequired,
    getCartItems: propTypes.func.isRequired,
    mycart:propTypes.array.isRequired,
    locale: propTypes.string.isRequired
};

const mapStateToProps = state => ({
    productList:state.product_List.productList,
    category:state.category.category,
    mycart:state.cart.cart,
    locale: state.i18n.locale

  });


const mapDispatchToProps = { addToCart, allProducts, updateProductList, clearCart, getCartItems};

export default connect(mapStateToProps, mapDispatchToProps, null, { forwardRef: true })(CartItems)