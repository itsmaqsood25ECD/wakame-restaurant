import { combineReducers } from "redux";
import { i18nReducer } from "react-redux-i18n";
import comments from "./comments";
import concerts from "./concerts";
import { toggleReducer } from "./toggleReducer";
import {ActiveBranchReducer} from './ActiveBranch';
import {selectedCategory} from './category'
import {addToCart, clearCart, GetCartItems} from './cart'
import {getProductList, updateProductList} from './products'
import {DefaultAddress} from './DefaultAddress'

const rootReducer = combineReducers({
    i18n: i18nReducer,
    toggleStack:toggleReducer, 
    concerts, comments, 
    branch:ActiveBranchReducer, 
    category:selectedCategory, 
    cart:addToCart, cart:clearCart, 
    cartItems:GetCartItems, 
    product_List:getProductList, 
    product_List:updateProductList,
    DefaultAddress:DefaultAddress 
 });

export default rootReducer;
