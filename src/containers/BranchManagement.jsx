import React,{ useState, useEffect } from "react";
import {Drawer,Spin, Rate , Button, Badge} from 'antd'
import {useDispatch, useSelector} from 'react-redux'
import {showBranchListModal} from '../redux/actions/toggleAction'
import {setActiveBranch} from '../redux/actions/ActiveBranch'
import { Scrollbars } from 'react-custom-scrollbars';
import {getData} from '../services/api'
import './styles/branchesList.css'
import { useTranslation } from "react-i18next";
import propTypes from 'prop-types';
import { connect } from 'react-redux';

const BrnachManagmentList = (props) => {
    const {REACT_APP_API_URL} = process.env
    const {t} = useTranslation()
    const [visible, setVisible] = useState()
    const [Branches, setBranches] = useState([])
    const [load, setLoad] = useState(true)

    const dispatch = useDispatch();
    
    const state_visible = useSelector( state => state.toggleStack.showBranchLists)

     useEffect(() => {
        setVisible(state_visible);
        getBranchOutlet()
        
     }, [state_visible]);

     async function getBranchOutlet () {
         const config = {
                "headers": {
                  "Access-Control-Allow-Origin": "*",
                }
         }
         const res = await getData(`${REACT_APP_API_URL}/api/v1/outlet/getoutlets`, '', config)
         try {
             console.log(res)
             setLoad(false)
             if(res.data.status === 200){
               setBranches(res.data.data)
               
             }

         } catch (error) {
             
         }

     }

     const handleBranchSelection = (branch) => {
         localStorage.setItem("Active_Branch", JSON.stringify(branch))
         dispatch(setActiveBranch(JSON.stringify(branch)))
         dispatch(showBranchListModal(false))
         localStorage.removeItem("product_list")
         localStorage.removeItem("cart")
         window.location.reload()
     }


     const onClose = () => {
      dispatch(showBranchListModal(false))
     };


    return (
        <div>
        
         
            <Drawer
          title={<div className={props.locale == 'en' ? '': "text-left"}>{t('active-branches')}</div>}
          placement={props.locale == 'en' ? 'left': 'right'}
          closable={true}
          onClose={onClose}
          visible={visible}
          className="responsive-drawer"
          key="active branches"
   
        >

          <div className="Drawer-header">
            
          </div>
        {load ? <Spin ></Spin>: 
                <Scrollbars >
             <div style={{height:"100vh"}}>
                {Branches && Branches.map((branch, index) => {
                    return <>
                    {branch && branch.status === "2" ?  <Badge.Ribbon color="yellow" placement="start" text={branch.status === "2" ? "Busy" : ''}>
                    <div className={`branch-list-container ${branch.status === "2" ? "Busy" : ''}`}>
                    
                        <div className="branch-img-container">
                            {branch.image && branch.image.length > 0 ? <img className="branch-image" src={process.env.REACT_APP_BASE_URL + '/assets/uploads/photo/'+ branch.image[0]} alt={branch.image[0]}></img> : <img className="branch-image" src={process.env.REACT_APP_PUBLIC_URL+ "/images/NO_IMG.jpg"} alt="no img"></img>}
                            
                        </div>
                        <div className="branch-details-container">
                            <p className="branch-name">{branch.outlet_name[props.locale]} {branch.outlet_busy === "1" && <span class="badge bg-busy badge badge-outline">{t('busy')}</span> } {branch.outlet_full === "0" ? <span class="badge bg-dinein badge badge-outline">{t('dineIn')}</span> : <span class="badge bg-dinein badge badge-outline">{t('full')}</span>}</p>
                            <p className="branch-address">{branch.address[props.locale]}</p>
                            <p className="branch-timing"><img src={process.env.REACT_APP_PUBLIC_URL + '/images/time.svg'} alt="time"></img> {branch.opentime} -  {branch.closetime}</p>
                        </div>
                        <div className="branch-action-container">
                        <Rate disabled defaultValue={branch.rat.rating} />
                        <p className="branch-review">{branch.rat.review} Reviews</p>
                        <Button onClick={() => handleBranchSelection(branch)} type="dashed">Activate</Button>
                        </div>
                    </div>
                    </Badge.Ribbon> : <div className={`branch-list-container ${branch.status === "0" ? 'inActive' : branch.status === "2" ? "Busy" : ''}`}>
                    
                    <div className="branch-img-container">
                        {branch.image && branch.image.length > 0 ? <img className="branch-image" src={'https://wakameuat.upappfactory.app/wakame-admin/uploads/'+ branch.image[0]} alt={branch.image[0]}></img> : <img className="branch-image" src={process.env.REACT_APP_PUBLIC_URL+ "/images/NO_IMG.jpg"} alt="no img"></img>}
                        
                    </div>
                    <div className="branch-details-container">
                        <p className="branch-name">{branch.outlet_name[props.locale]} {branch.outlet_busy === "1" && <span class="badge bg-busy badge badge-outline">{t('busy')}</span> } {branch.outlet_full === "0" ? <span class="badge bg-dinein badge badge-outline">{t('dineIn')}</span> : <span class="badge bg-dinein badge badge-outline">{t('full')}</span>}</p>
                        <p className="branch-address">{branch.address[props.locale]}</p>
                        <p className="branch-timing"><img src={process.env.REACT_APP_PUBLIC_URL + '/images/time.svg'} alt="time"></img> {branch.opentime} -  {branch.closetime}</p>
                    </div>
                    <div className="branch-action-container">
                    <Rate disabled defaultValue={branch.rat.rating} />
                    <p className="branch-review">{branch.rat.review} {t('review')}</p>
                    <Button onClick={() => handleBranchSelection(branch)} type="dashed">{t('activate')}</Button>
                    </div>
                </div>}
                    
                    </>
                })}
          </div>
                </Scrollbars>
          }
        </Drawer>
          
        </div>
    );
};

BrnachManagmentList.propTypes = {
    locale: propTypes.string.isRequired,
};

const mapStateToProps = state => ({
    locale: state.i18n.locale
  });


  const mapDispatchToProps = { };


export default connect(mapStateToProps, mapDispatchToProps)(BrnachManagmentList);
