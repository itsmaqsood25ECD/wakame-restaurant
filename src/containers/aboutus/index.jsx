import React,{ useState, useEffect } from "react";
import {getData} from '../../services/api'
import propTypes from "prop-types";
import {connect} from 'react-redux'
import { useTranslation } from 'react-i18next';
const ReactDOMServer = require('react-dom/server');
const HtmlToReactParser = require('html-to-react').Parser;

function Index(props) {
    const {REACT_APP_API_URL} = process.env
    const htmlToReactParser = new HtmlToReactParser();
    const {t} = useTranslation()
    const [aboutUs, setaboutUs] = useState({
        about:'',
        about_ar:''
    })
    const [Load, setLoad] = useState(false);

    useEffect(() => {
      
        fetchAboutUs()
     
     }, []);

     const fetchAboutUs = async() => {
        setLoad(true)
        const res = await getData(`${REACT_APP_API_URL}/api/v1/outlet/aboutus`)
        try {
          
          console.log("about", res)
          if(res.data.msg === 'Operation Successful'){
            setaboutUs(res.data.data)
          }
          setLoad(false)
          
        } catch (error) {
          setLoad(false)
        }
     }

    return (
        <div>
            <p className="text-white">
            { aboutUs && props.locale == 'en' ? htmlToReactParser.parse(aboutUs.about) : htmlToReactParser.parse(aboutUs.about_ar)}
            {console.log("aboutUs.about", aboutUs.about)}
            </p>
        </div>
    )
}


Index.propTypes = {
    locale: propTypes.string.isRequired,
  };
  
  const mapStateToProps = state => ({
      locale: state.i18n.locale
    });
    
    const mapDispatchToProps = { };
    
  export default connect(mapStateToProps, mapDispatchToProps)(Index);
  