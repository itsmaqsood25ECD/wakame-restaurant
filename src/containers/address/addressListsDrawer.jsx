import React,{ useState, useEffect } from "react";
import {Drawer,Spin, Button, Radio } from 'antd'
import {useDispatch, useSelector} from 'react-redux'
import {showAddressDrawer, showAddressListDrawer} from '../../redux/actions/toggleAction'
import { setAddress } from "../../redux/actions/adress";
import propTypes from "prop-types";
import {connect} from 'react-redux'
import {getData} from '../../services/api'
import { useTranslation } from "react-i18next";

const areEqual = (prevProps, nextProps) => true;

const AddressManagmentList = (props) => {
  const {REACT_APP_API_URL} = process.env;  
  const {t} = useTranslation()
  var default_add = localStorage.getItem("default_address")
  var parsed_default_add = default_add ? JSON.parse(default_add) : ''

    const [visible, setVisible] = useState()
    const [address, setAddress] = useState([])
    const [defaultAddress, setDefaultAddress] = useState(parsed_default_add)
    const [load, setLoad] = useState(false)

    const dispatch = useDispatch();
    
    const state_visible = useSelector( state => state.toggleStack.showAddressListDrawer)

     useEffect(() => {
        setVisible(state_visible);
        setLoad(false)
        fetchAllAddress()
        console.log(state_visible)
     }, [state_visible]);

      

     async function fetchAllAddress () {
     const res = await getData(`${REACT_APP_API_URL}/api/v1/outlet/list_address`)
     try {
       
       console.log(res)
       if(res.data.status === 200){
        setAddress(res.data.data)
       }
       
     } catch (error) {
       
     }
     }


     const onClose = () => {
      dispatch(showAddressListDrawer(false))
     };

     const handleAddAddress = () => {
      dispatch(showAddressListDrawer(false))
      dispatch(showAddressDrawer(true))
     }

     const handleChangeDefaultAddress = add => {
       console.log(add)
       localStorage.setItem("default_address", JSON.stringify(add))
       dispatch(props.setAddress(add))
       window.location.reload()
     }


    return (
        <div>
            <Drawer
          title={<div className={props.locale == 'en' ? '': 'text-left'}> {t('myAddresses')}</div>}
          placement={props.locale == 'en' ? "left" : "right"}
          closable={true}
          onClose={onClose}
          visible={visible}
          key={props.locale === 'en' ? "left" : "right"}
          className="responsive-drawer"
        >
          <div className="Drawer-header">
            
          </div>
        <Button type="dashed" onClick={handleAddAddress} block > {t('address-addNewAddress')}</Button>
        {load ? <Spin ></Spin>:  <div>
          {console.log(defaultAddress.id)}
          <Radio.Group className="address-block-list" defaultValue={defaultAddress.id} buttonStyle="outline">
          {address && address.length > 0 && address.map(add => {
            return <Radio.Button onClick={() => handleChangeDefaultAddress(add)} className="address-block" value={add.id}>
              
                <p className="name-of-address">{add.name_of_address}</p>
                <p className="complete-address">{add.complete_address}</p>
              <img className={props.locale === 'en' ? "left-checked" : "right-checked"} src={process.env.REACT_APP_PUBLIC_URL + '/images/checkmark-filled.svg'} alt="checkmark-filled" />

            </Radio.Button>
          })}
          </Radio.Group>
          </div>}
        </Drawer>
        </div>
    );
};


AddressManagmentList.propTypes = {
  setAddress:propTypes.func.isRequired,
  DefaultAddress:propTypes.object.isRequired,
  locale: propTypes.string.isRequired,
};

const mapStateToProps = state => ({
  DefaultAddress:state.DefaultAddress.default_address,
  locale: state.i18n.locale
});


const mapDispatchToProps = { setAddress };


export default connect(mapStateToProps, mapDispatchToProps)(AddressManagmentList);



