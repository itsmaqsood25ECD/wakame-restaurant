import { connect } from "react-redux";
import React, { useEffect } from "react";
import { PropTypes } from "prop-types";
import AppBar from "./components/Layout/AppBar";
import AppFooter from "./components/Layout/AppFooter";
// import ConcertList from "./Concerts/ConcertList";
import { getDir } from "./redux/selectors/i18n";
import Home from './pages'
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import {ToastContainer} from 'react-toastify'

// Import Pages
import MyProfile from './pages/myprofile'
import MainMenu from './pages/MainMenu'
import Checkout from './pages/checkout'
import OrderPlaced from './pages/OrderPlaced'
import { FeedbackSuccess } from "./containers/feedback/sucess";
import CouponPage from './pages/CouponPage'
import DealsPage from './containers/deals/promotionPage'
import CateringPage from './containers/catering/index'
import PaymentStatus from './containers/Peyments/PaymentStatus'
import PaymentReceipt from './containers/Peyments/PaymentReceipt'
// Pages End

// mobile pages import
import Mmyprofile from './pages/mobile/Profile'
import Mmanageaddress from './pages/mobile/ManageAddress'
import Morderlist from './pages/mobile/OrderList'
import Mloyaltypoints from './pages/mobile/LoyaltyPoints'
import Mfeedback from './pages/mobile/Feedback'
import Maboutus from './pages/mobile/AboutUs'
import Mcontactus from './pages/mobile/ContactUs'
import Mlanguage from './pages/mobile/Language'

// end mobile pages

import { useTranslation } from "react-i18next";

// Importing the Bootstrap CSS
import './styles/css/bootstrap.min.css';

function App(props) {

  const { t, i18n } = useTranslation();

  useEffect(() => {
    document.dir = props.dir;
    i18n.changeLanguage(props.locale);
    localStorage.setItem("language", props.locale)
  }, [props.dir]);

  if(props.locale == 'ar'){
    document.body.classList.add('text-right');
    localStorage.setItem("language", props.locale)
  }else if(props.locale == 'en'){
    document.body.classList.remove('text-right');
    localStorage.setItem("language", props.locale)
  }

  return (
    <>
    <Router>
      <AppBar />
   
          <Switch>
              <Route path="/" exact>
            <Home />
          </Route>
          <Route path="/myprofile" exact>
            <MyProfile />
          </Route>
          <Route path="/menu" exact>
            <MainMenu />
          </Route>
          <Route path="/checkout" exact>
            <Checkout />
          </Route>
          <Route path="/checkout/success" exact>
            <OrderPlaced />
          </Route>
          <Route path="/feedback/success" exact>
            <FeedbackSuccess />
            </Route>
          <Route path="/coupon" exact>
            <CouponPage />
          </Route>
          <Route path="/deals" exact>
            <DealsPage />
          </Route>
          <Route path="/catering" exact>
          <CateringPage />
          </Route>
          <Route exact path="/paymentReceipt">
            <PaymentReceipt ></PaymentReceipt>
            </Route>
          <Route exact path="/paymentStatus">
            <PaymentStatus />
            </Route>
            {/* Mobile pages */}
            <Route exact path="/m.myprofile">
            <Mmyprofile />
            </Route>
            <Route exact path="/m.order-history">
            <Morderlist />
            </Route>
            <Route exact path="/m.manage-addresses">
            <Mmanageaddress />
            </Route>
            <Route exact path="/m.loyalty-points">
            <Mloyaltypoints />
            </Route>
            <Route exact path="/m.feedback">
            <Mfeedback />
            </Route>
            <Route exact path="/m.about-us">
            <Maboutus />
            </Route>
            <Route exact path="/m.contact-us">
            <Mcontactus />
            </Route>
            <Route exact path="/m.language">
            <Mlanguage />
            </Route>
          </Switch>
      <AppFooter />
      <ToastContainer />
    </Router>
    </>
  );
}

App.propTypes = {
  dir: PropTypes.string.isRequired,
  locale: PropTypes.string.isRequired,
};

const mapStateToProps = state => ({ 
  dir: getDir(state),
  locale: state.i18n.locale
 });

export default connect(mapStateToProps)(App);