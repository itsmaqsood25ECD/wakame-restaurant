import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import {Button} from 'antd'
import { CheckCircleTwoTone } from '@ant-design/icons';
import { Link } from 'react-router-dom';
import {useTranslation} from 'react-i18next'
import propTypes from 'prop-types'

export const FeedbackSuccess = (props) => {
    const {t} = useTranslation();
    return (
        <div className="mt-5 pt-5 mb-5 pb-5 ml-auto mr-auto text-center" style={{maxWidth:"300px", width:"100%"}}>
            <CheckCircleTwoTone className="text-xxl mt-5 pt-5" twoToneColor="#52c41a" />
            <p className="text-white text-xxl">{t('submittedSuccessful')}</p>
            <p>{t('thankyouForFeedback')}</p>
            <Link to="/">
            <Button type="primary">{t('goToHomepage')}</Button>
            </Link>
        </div>
    )
}

FeedbackSuccess.propTypes = {
    prop: PropTypes,
    locale: propTypes.string.isRequired,
}

const mapStateToProps = (state) => ({
    locale: state.i18n.locale

})

const mapDispatchToProps = {
    
}

export default connect(mapStateToProps, mapDispatchToProps)(FeedbackSuccess)
