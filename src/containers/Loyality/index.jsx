import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import './loyalitypoints.css'
import { getData, postData } from '../../services/api'
import propTypes from 'prop-types';
import {connect} from 'react-redux';
import { useTranslation } from "react-i18next";



function LoyalityManagement(props) {
    const {t} = useTranslation()
    const {REACT_APP_API_URL} = process.env
    const [loyalityPoints, setLoyalityPoints] = useState()
    const [userID, setUserID] = useState(localStorage.getItem('userID') ? localStorage.getItem('userID') : '')
    const [load, setLoad] = useState(false)
    const [loyalityToOmrRate, setLoyalityToOmrRate] = useState(0)
    useEffect(() => {
        getLoyalityPoints()
        getLoayaltyPointsInfo()
    }, [])

   const getLoyalityPoints = async() => {
    setLoad(true)
    const formData = new FormData()
    formData.append("emp_id", userID)
    const res = await postData(`${REACT_APP_API_URL}/api/v1/outlet/getloyalitybyemp`, formData)
    try {
        console.log(res)
        setLoad(false)
        if(res.data.status === 200){
            setLoyalityPoints(res.data.data[0].point)
        }
     } catch (error) {
        setLoad(true)
    }

   }
   
   const getLoayaltyPointsInfo = async() => {

    const res = await getData(`${REACT_APP_API_URL}/api/v1/outlet/getloyality`)
    try {
           if(res.data.msg == 'Operation Successful'){
            setLoyalityToOmrRate(res.data.data[0].redeem_point)
           }
    } catch (error) { 
    }
   }
    


    return (<>
        <div>
            <div className='d-flex flex-wrap d-md-block' style={{minHeight:300}}>
            <div className={`loyalty-container text-white ${props.locale == 'en' ?  "float-left" : 'float-right'}`}>
                <p className="text-white font-bold">{t('totalPointsEarned')}</p>
                <div className="loyality-points-inner">
                    <p className="m-0 text-md font-bold">{(+(loyalityPoints && loyalityPoints.loyality_point)).toFixed(3)}</p>
                    <p className="text-gray text-sm font-bold">{((loyalityPoints && +loyalityPoints.loyality_point) * +loyalityToOmrRate).toFixed(3)} SR</p>
                </div>
            </div>
            <div className={`loyalty-container text-white ${props.locale == 'en' ?  "float-left" : 'float-right'}`}>
                <p className="font-bold">{t('pointsEarnedThisMonth')}</p>
                <div className="loyality-points-inner">
                    <p className="m-0 text-md font-bold">{(+(loyalityPoints && loyalityPoints.loyality_used_this_month)).toFixed(3)}</p>
                    <p className="text-gray text-sm font-bold">{((loyalityPoints && +loyalityPoints.loyality_used_this_month) * +loyalityToOmrRate).toFixed(3)} SR</p>
                </div>
            </div>
            <div className={`loyalty-container text-white ${props.locale == 'en' ?  "float-left" : 'float-right'}`}>
                <p className="font-bold">{t('totalPointRedeemed')}</p>
                <div className="loyality-points-inner">
                    <p className="m-0 text-md font-bold">{(+(loyalityPoints && loyalityPoints.loyality_used)).toFixed(3)}</p>
                    <p className="text-gray text-sm font-bold">{((loyalityPoints && +loyalityPoints.loyality_used) * +loyalityToOmrRate).toFixed(3)} SR</p>
                </div>
            </div>
            <div className={`loyalty-container text-white ${props.locale == 'en' ?  "float-left" : 'float-right'}`}>
                <p className="font-bold">{t('totalPointAvialable')}</p>
                <div className="loyality-points-inner">
                    <p className="m-0 text-md font-bold">{(+(loyalityPoints && loyalityPoints.loyality_available_points)).toFixed(3)}</p>
                    <p className="text-gray text-sm font-bold">{((loyalityPoints && +loyalityPoints.loyality_available_points) * +loyalityToOmrRate).toFixed(3)} SR</p>
                </div>
            </div>
            <div className={`loyalty-container text-white ${props.locale == 'en' ?  "float-left" : 'float-right'}`}>
                <p className="font-bold">{t('totalPointThroughReferrals')}</p>
                <div className="loyality-points-inner">
                    <p className="m-0 text-md font-bold">3.050</p>
                    <p className="text-gray text-sm font-bold">0.305 SR</p>
                </div>
            </div>
         </div>
            
        </div>
        <p className="text-white text-uppercase section-subtitle mt-4">{t('history')}</p>
            <div className="loyality-points-history-list-container">
                <div className="points-used-container">
                    <div className="row">
                        <div className="col-md-6 col-sm-12">
                            <p className="text-gray text-sm font-weight-light m-0">{t('orderNumber')}</p>
                            <p className="text-white text-md font-bold mb-3">#51233423</p>
                        </div>
                        <div className="col-md-6 col-sm-12">
                        <p className="text-gray text-sm font-weight-light m-0">{t('orderDate')}</p>
                            <p className="text-md font-bold mb-3">Mon 17, 2021, 10:45 PM</p>
                        </div>
                        <div className="col-md-6 col-sm-12">
                            <p className="text-gray text-sm font-weight-light m-0">{t('loyalityEarned')}</p>
                            <p className="text-success text-md font-bold m-0">+ 3.450 pts (0.340 SR)</p>
                        </div>
                        <div className="col-md-6 col-sm-12">
                        <p className="text-gray text-sm font-weight-light m-0">{t('loyalityRedeemed')}</p>
                            <p className="text-danger text-md font-bold m-0">- 1.450 pts (0.140 SR)</p>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}


LoyalityManagement.propTypes = {
    locale: propTypes.string.isRequired,
  };
  
  const mapStateToProps = state => ({
      locale: state.i18n.locale
    });
    
    const mapDispatchToProps = { };
    
  export default connect(mapStateToProps, mapDispatchToProps)(LoyalityManagement);
  
