import React, {useState, useEffect} from "react";
import {useDispatch, useSelector} from 'react-redux'
import {showLoginModal, showSignupModal, showForgetPassModal} from '../redux/actions/toggleAction'
import {Form, Input, Button, Checkbox, Modal} from 'antd';
import SignUpForm from './signup'
import ForgetPass from './forgetPassword'
import './styles/login.css'
import {  toast } from 'react-toastify';
import {postData} from '../services/api'
import {useTranslation} from 'react-i18next'
import propTypes from 'prop-types';
import { connect } from 'react-redux';

// const {REACT_APP_BASE_URL} = process.env

const LoginManagement = React.memo((props) => {

    const {t} = useTranslation();

    const {REACT_APP_API_URL} = process.env
    const [visible,
        setVisible] = useState()
 
    const dispatch = useDispatch();

    const state_visible = useSelector(state => state.toggleStack.showLoginModal)

    useEffect(() => {
        setVisible(state_visible);
        console.log(state_visible);
    }, [state_visible]);

    const onClose = () => {
        dispatch(showLoginModal(false))
    };

    const handleShowSignUp = () => {
        dispatch(showLoginModal(false))
        dispatch(showSignupModal(true))
    }

    const HandleForgetPassword = () => {
        dispatch(showLoginModal(false))
        dispatch(showForgetPassModal(true))
    }

    const onFinish = async(values) => {
        // console.log('Received values of form: ', values);
    
        const body = {
          'username':values.mobile,
          'password':values.password,
          'mode':'mobile'
          }

          let config = {
            headers: {
              "Content-Type": "application/json; charset=utf-8",
              "Access-Control-Allow-Origin": "*",
              "Access-Control-Allow-Methods":"GET, POST, OPTIONS, PUT, DELETE"
            }
          }

      const res = await postData(`${REACT_APP_API_URL}/api/v1/user/login`, body)
        try {
            console.log("respo > ", res)
            if(res.data.msg == "Login Successfully"){
                localStorage.setItem('user', JSON.stringify(res.data.data))
                localStorage.setItem('userID', res.data.data.emp_id)
                localStorage.setItem('token', JSON.stringify(res.data.token))
                dispatch(showLoginModal(false))
                toast.success(`${t('loggedInSuccessfully')}`)
               window.location.reload()
               console.log(res)
            }else{
                toast.error(res.data.msg)
            }
           
        } catch (error) {
            // console.log(error)
            toast.success(`${t('usernameOrPasswordIsIncorrect')}`)
        }

    }

     

    return (
        <div>
          
            <Modal width={400} title="" footer={null} visible={visible} onCancel={onClose}>
                
                <div className="center">
                    <h2>{t('login')}</h2>
                    <p>{t('newToWakame')} {" "}
                    <Button type="link" onClick={handleShowSignUp} className="primary">{t('signUp')}</Button>
                    </p>
                </div>
                <br/>

                <Form // id="components-form-normal-login"
                    name="normal_login" className="login-form" initialValues={{
                    remember: false,
                    username:'',
                    password:''

                }} size="large" onFinish={onFinish}>
                    <Form.Item
                        name="mobile"
                        rules={[{
                            required: true,
                            message: `${t('err-mobileNumber')}`
                        }
                    ]}>
                        <Input
                            maxlength='9'
                            addonBefore="+966"
                            type="number"
                            min="0"
                            max="999999999"
                            varient="bottom-outline"
                            placeholder={t('mobileNumber')}
                            />
                    </Form.Item>
                    <Form.Item
                        name="password"
                        rules={[{
                            required: true,
                            message: `${t('err-password')}`
                        }
                    ]}>
                        <Input.Password
                            type="password"
                            placeholder={t('password')}
                            varient="bottom-outline"
                            />
                    </Form.Item>
                    <Form.Item>
                        <Form.Item name="remember" valuePropName="checked" noStyle>
                            <Checkbox>{t('rememberMe')}</Checkbox>
                        </Form.Item>

                        <Button type="link" className={`login-form-forgot ${props.locale == 'en' ? '' : 'float-left'}`} onClick={HandleForgetPassword}>
                            {t('forgetPassword')}
                        </Button>
                    </Form.Item>

                    <Form.Item>
                        <Button type="primary" htmlType="submit" className="login-form-button">
                           {t('login')}
                        </Button>
                    </Form.Item>
                </Form>
                <div className="center">
                    <p>{t('or')}</p>
                    <Button size="large" className="btn-block gmail-login-button">
                        {t('loginWithGmail')}
                    </Button>
                    <Button size="large" className="btn-block facebook-login-button">
                        {t('loginWithFacebook')}
                    </Button>
                </div>
            </Modal>
            <SignUpForm/>
            <ForgetPass/>
        </div>
    );
});


LoginManagement.propTypes = {
    locale: propTypes.string.isRequired,
};

const mapStateToProps = state => ({
    locale: state.i18n.locale
  });
  
  const mapDispatchToProps = { };
  
export default connect(mapStateToProps, mapDispatchToProps)(LoginManagement);
  
