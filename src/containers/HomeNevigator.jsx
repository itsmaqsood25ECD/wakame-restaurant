import React, { Component } from 'react';
import {Row, Col, Button} from 'antd'
import {withRouter} from 'react-router-dom'
import {showBranchListModal, showBranchListViewModal, showLoginModal, showAddressListDrawer} from '../redux/actions/toggleAction'
import propTypes from "prop-types";
import { connect } from "react-redux";
import { withTranslation } from 'react-i18next';


const {REACT_APP_PUBLIC_URL} = process.env
const isLogged = localStorage.getItem('token') ?  localStorage.getItem('token') : '';
const isBranchSelected = localStorage.getItem('Active_Branch') ? localStorage.getItem('Active_Branch') : '';
const isDefaultAddressSelected = localStorage.getItem('default_address') ? localStorage.getItem('default_address') : '';


class HomeNavigator extends Component {
    
    state = {
    isLogged:isLogged,
    isBranchSelected:isBranchSelected,
    isDefaultAddressSelected: isDefaultAddressSelected,

    };

    

    handleBranchListView = () => {
      
        this.props.showBranchListViewModal(true)
        
    }

    handleNavigate = (param) => {
        if(isBranchSelected == ''){
          this.props.showBranchListModal(true)
        }else{
            const { history } = this.props;
            if(history) history.push(`/${param}`);
        }
    }

    
    render() {
        const { t } = this.props;
        return (
         <Row gutter={16} className="">
             
             <Col xs={12} xl={6} sm={12} className="navigator-col" >
                 <div className="menu-navigator-container" onClick={() => this.handleNavigate('menu')}>
                     <img src={REACT_APP_PUBLIC_URL+'/images/homepage/MAIN_MENU.svg'} alt="Main-Menu"  />
                      <div className="title">
                      {t('home-navigator-mainmenu')}
                      </div>
                      <div className="desc d-none d-md-block">
                      {t('home-navigator-mainmenu-desc')}
                      </div>
                      <Button type="text" className="navigator-arrow d-none d-md-block" >
                          <img src={REACT_APP_PUBLIC_URL+'/images/homepage/arrow-right.svg'} alt="arrow-right" />
                      </Button>
                 </div>
             </Col>
             <Col xs={12} xl={6} sm={12} className="navigator-col" >
             <div className="menu-navigator-container">
                     <img src={REACT_APP_PUBLIC_URL+'/images/homepage/CATERING.svg'} alt="catering" />
                      <div className="title">
                      {t('home-navigator-catering')}
                      </div>
                      <div className="desc d-none d-md-block">
                      {t('home-navigator-catering-desc')}
                      </div>
                      <Button type="text" className="navigator-arrow d-none d-md-block"
                    //    onClick={() => this.handleNavigate('catering')}
                       >
                          <img src={REACT_APP_PUBLIC_URL+'/images/homepage/arrow-right.svg'} alt="arrow-right" />
                      </Button>
                 </div>
             </Col>
             <Col xs={12} xl={6} sm={12} className="navigator-col" >
             <div className="menu-navigator-container" onClick={this.handleBranchListView}>
                     <img src={REACT_APP_PUBLIC_URL+'/images/homepage/RESERVATION.svg'}  alt="reservation" />
                      <div className="title">
                      {t('home-navigator-reservation')}
                      </div>
                      <div className="desc d-none d-md-block">
                      {t('home-navigator-reservation-desc')}
                      </div>
                      <Button type="text" className="navigator-arrow d-none d-md-block" >
                          <img src={REACT_APP_PUBLIC_URL+'/images/homepage/arrow-right.svg'} alt="arrow-right" />
                      </Button>
                 </div>
             </Col>
             <Col xs={12} xl={6} sm={12} className="navigator-col" >
             <div className="menu-navigator-container" onClick={this.handleBranchListView}>
                     <img src={REACT_APP_PUBLIC_URL+'/images/homepage/BRANCHES.svg'} alt="branches"  />
                      <div className="title">
                      {t('home-navigator-branches')}
                      </div>
                      <div className="desc d-none d-md-block">
                      {t('home-navigator-branches-desc')}
                      </div>
                    <Button type="text" className="navigator-arrow d-none d-md-block" >
                            <img src={REACT_APP_PUBLIC_URL+'/images/homepage/arrow-right.svg'} alt="arrow-right" />
                    </Button>
                 </div>
             </Col>
         </Row>
        );
    }
}


HomeNavigator.propTypes = {
    showBranchListModal:propTypes.func.isRequired,
    showLoginModal:propTypes.func.isRequired,
    showAddressListDrawer:propTypes.func.isRequired,
    showBranchListViewModal:propTypes.func.isRequired

};

const mapStateToProps = state => ({
    showBranchLists: state.toggleStack.showBranchLists,
    showBranchListsView:state.toggleStack.showBranchListsView,
    showLoginModal: state.toggleStack.showLoginModal,
    showAddressListDrawer: state.toggleStack.showAddressListDrawer

 })

 const mapDispatchToProps = { showBranchListModal, showLoginModal, showAddressListDrawer, showBranchListViewModal }


 export default connect(mapStateToProps, mapDispatchToProps)(withTranslation()(withRouter(HomeNavigator)));


 